import styled from 'styled-components';

export const ButtonWrapper = styled.div`
    span {
        margin-right: 10px;
    }
`;
