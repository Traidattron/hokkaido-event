import { Tooltip } from 'antd';
import { DeleteOutlined, PlusCircleOutlined, EditOutlined } from '@ant-design/icons';
import colors from 'theme/colors';
import { ButtonWrapper } from './SetAdminButoon.style';
type SetAdminButtonProps = {
    handleAdd?: any;
    handleDelete?: any;
    handleUpdate?: any;
};
const SetAdminButton: React.FC<SetAdminButtonProps> = ({ handleAdd, handleDelete, handleUpdate }) => {
    return (
        <ButtonWrapper>
            <Tooltip title='Add'>
                {' '}
                <PlusCircleOutlined style={{ fontSize: '25px', color: colors.create }} onClick={handleAdd} />
            </Tooltip>
            <Tooltip title='Delete'>
                {' '}
                <DeleteOutlined style={{ fontSize: '25px', color: colors.delete }} onClick={handleDelete} />
            </Tooltip>

            <Tooltip title='Edit'>
                {' '}
                <EditOutlined style={{ fontSize: '25px', color: colors.update }} onClick={handleUpdate} />
            </Tooltip>
        </ButtonWrapper>
    );
};
export default SetAdminButton;
