export const AppIcon = ({ width = '40px', height = '40px' }) => {
    return (
        <svg
            id='Capa_1'
            enableBackground='new 0 0 512 512'
            height={height}
            viewBox='0 0 512 512'
            width={width}
            xmlns='http://www.w3.org/2000/svg'
            xmlnsXlink='http://www.w3.org/1999/xlink'
        >
            <linearGradient id='SVGID_1_' gradientUnits='userSpaceOnUse' x1='256' x2='256' y1='451' y2='61'>
                <stop offset='0' stopColor='#a93aff' />
                <stop offset='1' stopColor='#ff81ff' />
            </linearGradient>
            <linearGradient id='SVGID_2_' gradientUnits='userSpaceOnUse' x1='256' x2='256' y1='512' y2='0'>
                <stop offset='0' stopColor='#ffbef9' />
                <stop offset='1' stopColor='#fff1ff' />
            </linearGradient>
            <g id='Engineering_4_'>
                <g>
                    <g>
                        <path
                            d='m436 211h-53.203c-1.553-4.336-3.34-8.613-5.332-12.803l37.646-37.661c5.833-5.833 5.882-15.329 0-21.211l-42.451-42.422c-5.859-5.859-15.352-5.859-21.211 0l-37.646 37.646c-4.189-2.007-8.467-3.794-12.803-5.347v-53.202c0-8.291-6.709-15-15-15h-60c-8.291 0-15 6.709-15 15v53.203c-4.336 1.553-8.613 3.34-12.803 5.347l-37.646-37.646c-5.859-5.859-15.352-5.859-21.211 0l-42.451 42.422c-5.879 5.879-5.835 15.376 0 21.211l37.646 37.661c-1.992 4.189-3.779 8.467-5.332 12.803h-53.203c-8.291 0-15 6.709-15 15v60c0 8.291 6.709 15 15 15h53.203c1.553 4.336 3.34 8.613 5.332 12.803l-37.646 37.661c-5.833 5.833-5.882 15.329 0 21.211l42.451 42.422c5.859 5.859 15.352 5.859 21.211 0l37.646-37.646c4.189 2.007 8.467 3.794 12.803 5.347v53.201c0 8.291 6.709 15 15 15h60c8.291 0 15-6.709 15-15v-53.203c4.336-1.553 8.613-3.34 12.803-5.347l37.646 37.646c5.859 5.859 15.352 5.859 21.211 0l42.451-42.422c5.879-5.879 5.835-15.376 0-21.211l-37.646-37.661c1.992-4.189 3.779-8.467 5.332-12.803h53.203c8.291 0 15-6.709 15-15v-60c0-8.29-6.709-14.999-15-14.999z'
                            fill='url(#SVGID_1_)'
                        />
                    </g>
                </g>
                <g>
                    <g>
                        <path
                            d='m256 181c-41.367 0-75 33.647-75 75s33.633 75 75 75 75-33.647 75-75-33.633-75-75-75zm-1 301c-60.088 0-116.602-23.408-159.111-65.903-42.481-42.51-65.889-99.009-65.889-159.097 0-71.426 33.105-137.285 90.82-180.718 5.889-4.424 7.676-12.466 4.248-18.984-3.457-6.548-11.162-9.551-18.076-7.207l-68.877 22.983 9.492 28.447 7.061-2.344c-35.244 44.561-54.668 99.552-54.668 157.823 0 68.101 26.514 132.144 74.678 180.308 48.193 48.164 112.207 74.692 180.322 74.692 8.291 0 15-6.709 15-15s-6.709-15-15-15zm209.393-71.521c-2.344.776-4.717 1.567-7.119 2.358 35.273-44.634 54.726-99.741 54.726-157.837 0-68.101-26.514-132.144-74.678-180.308-48.193-48.164-112.207-74.692-180.322-74.692-8.291 0-15 6.709-15 15s6.709 15 15 15c60.088 0 116.602 23.408 159.111 65.903 42.481 42.51 65.889 99.009 65.889 159.097 0 69.565-31.67 134.341-86.895 177.7-6.568 5.139-7.673 14.675-2.432 21.196 2.959 3.662 4.775 5.933 10.049 5.933 8.496 0 25.898-5.801 71.162-20.903 7.852-2.607 12.1-11.104 9.492-18.97-2.636-7.866-11.132-12.07-18.983-9.477z'
                            fill='url(#SVGID_2_)'
                        />
                    </g>
                </g>
            </g>
        </svg>
    );
};
