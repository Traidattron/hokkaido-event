import React from 'react';
import { ContentLoading } from './LoadingSmall.style';
const LoadingSmall = () => {
    return (
        <ContentLoading>
            <div className='lds-facebook'>
                <div></div>
                <div></div>
                <div></div>
            </div>
        </ContentLoading>
    );
};
export default LoadingSmall;
