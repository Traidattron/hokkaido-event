import styled from 'styled-components';

export const ContentLoading = styled.div`
    @-webkit-keyframes ldio-qy0h229ctt {
        0% {
            transform: rotate(0deg);
        }
        100% {
            transform: rotate(360deg);
        }
    }
    @keyframes ldio-qy0h229ctt {
        0% {
            transform: rotate(0deg);
        }
        100% {
            transform: rotate(360deg);
        }
    }
    .ldio-qy0h229ctt > div > div {
        transform-origin: 98.5px 98.5px;
        -webkit-animation: ldio-qy0h229ctt 3.0303030303030303s linear infinite;
        animation: ldio-qy0h229ctt 3.0303030303030303s linear infinite;
        opacity: 0.8;
    }
    .ldio-qy0h229ctt > div > div > div {
        position: absolute;
        left: 0px;
        top: 0px;
        width: 98.5px;
        height: 98.5px;
        border-radius: 98.5px 0 0 0;
        transform-origin: 98.5px 98.5px;
    }
    .ldio-qy0h229ctt > div div:nth-child(1) {
        -webkit-animation-duration: 0.7575757575757576s;
        animation-duration: 0.7575757575757576s;
    }
    .ldio-qy0h229ctt > div div:nth-child(1) > div {
        background: #f150da;
        transform: rotate(0deg);
    }
    .ldio-qy0h229ctt > div div:nth-child(2) {
        -webkit-animation-duration: 1.0101010101010102s;
        animation-duration: 1.0101010101010102s;
    }
    .ldio-qy0h229ctt > div div:nth-child(2) > div {
        background: #f480e3;
        transform: rotate(0deg);
    }
    .ldio-qy0h229ctt > div div:nth-child(3) {
        -webkit-animation-duration: 1.5151515151515151s;
        animation-duration: 1.5151515151515151s;
    }
    .ldio-qy0h229ctt > div div:nth-child(3) > div {
        background: #fca3f0;
        transform: rotate(0deg);
    }
    .ldio-qy0h229ctt > div div:nth-child(4) {
        -webkit-animation-duration: 3.0303030303030303s;
        animation-duration: 3.0303030303030303s;
    }
    .ldio-qy0h229ctt > div div:nth-child(4) > div {
        background: #fccaf5;
        transform: rotate(0deg);
    }
    .loadingio-spinner-wedges-5ts5z3fjol {
        width: 197px;
        height: 197px;
        display: inline-block;
        overflow: hidden;
        background: transparent;
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
    }
    .ldio-qy0h229ctt {
        width: 100%;
        height: 100%;
        position: relative;
        transform: translateZ(0) scale(1);
        -webkit-backface-visibility: hidden;
        backface-visibility: hidden;
        transform-origin: 0 0; /* see note above */
    }
    .ldio-qy0h229ctt div {
        box-sizing: content-box;
    }
`;
