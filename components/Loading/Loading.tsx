import React from 'react';
import { ContentLoading } from './Loading.style';
const Loading = () => {
    return (
        <ContentLoading>
            <div className='loadingio-spinner-wedges-5ts5z3fjol'>
                <div className='ldio-qy0h229ctt'>
                    <div>
                        <div>
                            <div></div>
                        </div>
                        <div>
                            <div></div>
                        </div>
                        <div>
                            <div></div>
                        </div>
                        <div>
                            <div></div>
                        </div>
                    </div>
                </div>
            </div>
        </ContentLoading>
    );
};
export default Loading;
