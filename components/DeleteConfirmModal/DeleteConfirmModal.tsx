import { _word } from 'data/language';
import { Modal } from 'antd';

type props = {
    isModalVisible?: any;
    handleOk?: any;
    handleCancel?: any;
    isLoading?: boolean;
    contentList?: any;
    contentTitle?: string;
};

const DeleteConfirmModal: React.FC<props> = ({
    isModalVisible,
    handleOk,
    handleCancel,
    isLoading,
    contentList,
    contentTitle,
}) => {
    return (
        <Modal
            title='Warning'
            visible={isModalVisible}
            onOk={handleOk}
            onCancel={handleCancel}
            confirmLoading={isLoading}
        >
            {contentTitle}
            {contentList.map((item, index) => (
                <p key={index}>{item.name}</p>
            ))}
        </Modal>
    );
};

export default DeleteConfirmModal;
