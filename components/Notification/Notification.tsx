import { notification } from 'antd';

export enum typeNotification {
    success = 'success',
    info = 'info',
    warning = 'warning',
    error = 'error',
}

export const openNotification = (type: string, description: string) => {
    notification[type]({
        // message: type,
        description,
        placement: 'bottomLeft',
    });
};
