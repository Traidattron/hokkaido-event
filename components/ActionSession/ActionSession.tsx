import {
    EditOutlined,
    DeleteOutlined,
    UploadOutlined,
    MergeCellsOutlined,
    ContainerOutlined,
    PlusOutlined,
} from '@ant-design/icons';
import { _word } from 'data/language';
import { Button, Space, Tooltip } from 'antd';
import React from 'react';

type props = {
    onAdd?: any;
    onEdit?: any;
    onDelete?: any;
    onUpload?: any;
    onMerge?: any;
    disableEdit?: boolean;
    disableDelete?: boolean;
    isMail?: boolean;
    refetch?: any;
    hasEdit?: boolean;
    hasDelete?: boolean;
    hasUpload?: boolean;
    hasMerge?: boolean;
    loading?: boolean;
};

const ActionSession = ({
    onAdd,
    onEdit,
    onDelete,
    onUpload,
    onMerge,
    disableEdit = true,
    disableDelete = true,
    isMail = false,
    refetch,
    hasEdit = true,
    hasDelete = true,
    hasUpload = false,
    hasMerge = false,
    loading = false,
}: props) => {
    // const URL = `https://form.mirai-chi.com/`;
    const URL = process.env.NEXT_PUBLIC_FORM;
    // const URL = `http://127.0.0.1:8000/login/${decodeURI(cookie.get('user_name'))}/${cookie.get('user_email')}`;
    return (
        <Space>
            <Tooltip title={_word.add}>
                <Button
                    size='large'
                    icon={<PlusOutlined />}
                    title={_word.add}
                    type='primary'
                    style={{ borderRadius: '50%' }}
                    onClick={onAdd}
                >
                    {_word.add}
                </Button>
            </Tooltip>
            {!disableDelete && (
                <Tooltip title={disableDelete ? _word.mustSelectItemsToDelete : _word.delete}>
                    <Button
                        size='large'
                        style={{ borderRadius: '50%', display: disableDelete ? 'none' : 'block' }}
                        disabled={disableDelete}
                        icon={<DeleteOutlined />}
                        title={_word.delete}
                        type='primary'
                        danger
                        onClick={onDelete}
                    ></Button>
                </Tooltip>
            )}
            {!disableEdit && (
                <Tooltip title={disableEdit ? _word.canOnlyEditOne : _word.edit}>
                    <Button
                        size='large'
                        style={{
                            borderRadius: '50%',
                            background: '#8e8e8e',
                            color: '#fff',
                            display: disableEdit ? 'none' : 'block',
                        }}
                        disabled={disableEdit}
                        type='default'
                        icon={<EditOutlined />}
                        title={_word.edit}
                        onClick={onEdit}
                    ></Button>
                </Tooltip>
            )}
            {hasMerge && (
                <Tooltip title={_word.mergeGroupMails}>
                    <Button
                        size='large'
                        style={{ borderRadius: '50%' }}
                        icon={<MergeCellsOutlined />}
                        type='default'
                        onClick={onMerge}
                    >
                        {/* {_word.mergeTitle} */}
                    </Button>
                </Tooltip>
            )}
            {hasUpload && (
                <Tooltip title={_word.uploadTooltip}>
                    <Button
                        size='large'
                        style={{ borderRadius: '50%' }}
                        icon={<UploadOutlined />}
                        title={_word.uploadTitle}
                        type='default'
                        onClick={onUpload}
                    >
                        {/* {_word.uploadTitle} */}
                    </Button>
                </Tooltip>
            )}
            <Tooltip title={_word.questionaire}>
                <Button
                    size='large'
                    style={{ borderRadius: 10, background: '#8e8e8e', color: '#fff' }}
                    icon={<ContainerOutlined />}
                    title={_word.questionaire}
                    type='default'
                    onClick={() => window.open(URL)}
                >
                    {/* {_word.questionaire} */}
                </Button>
            </Tooltip>
        </Space>
    );
};

export default ActionSession;
