import React, { useEffect, useState } from 'react';
import { Form, Input, Select, DatePicker, TimePicker, Button } from 'antd';
import { _word } from 'data/language';
import { SendOutlined } from '@ant-design/icons';
import { useCreateMessageMutation } from 'graphql/gen-types';
import { openNotification, typeNotification } from 'components/Notification/Notification';

const { Option } = Select;
type props = {
    eventID: string;
};
const layout = {
    labelCol: {
        span: 4,
    },
    wrapperCol: {
        span: 20,
    },
};
const tailLayout = {
    wrapperCol: { offset: 8, span: 16 },
};
export default function PushNotificationForm({ eventID }: props) {
    const [createMessage, { loading: createLoading }] = useCreateMessageMutation({
        onCompleted: () => {
            openNotification(typeNotification.success, _word.createCompanySuccess);
        },
        onError: (error) => {
            openNotification(typeNotification.error, error.message);
        },
    });

    const onFinish = (values: any) => {
        createMessage({
            variables: {
                createMessageInput: {
                    eventId: eventID,
                    message: values.message,
                    type: values.type,
                },
            },
        });
    };

    return (
        <Form {...layout} name='basic' onFinish={onFinish} initialValues={{ type: 'ALL' }}>
            <Form.Item
                label='To'
                name='type'
                rules={[
                    {
                        required: true,
                        message: _word.ruleRequired,
                    },
                ]}
            >
                <Select defaultValue='ALL' style={{ width: '100%' }}>
                    <Option value='ALL'>{_word.all}</Option>
                    <Option value='ALL_REGISTERED'>{_word.allRegistered}</Option>
                    <Option value='ALL_NON_REGISTERED'>{_word.allNonRegistered}</Option>
                    <Option value='ATTENDED'>{_word.attended}</Option>
                    <Option value='ANSWER_FORM'>{_word.answerForm}</Option>
                    <Option value='ATTENDED_ANSWER_FORM'>{_word.attendedAnswerForm}</Option>
                </Select>
            </Form.Item>
            <Form.Item
                // label={_word.message}
                noStyle
                name='message'
                rules={[
                    {
                        required: true,
                        message: _word.ruleRequired,
                    },
                ]}
            >
                <Input.TextArea style={{ width: '100%' }} placeholder={_word.message} rows={3} />
            </Form.Item>
            <Form.Item style={{ marginTop: 16 }} {...tailLayout}>
                <Button loading={createLoading} icon={<SendOutlined />} type='primary' htmlType='submit'>
                    {_word.send}
                </Button>
            </Form.Item>
        </Form>
    );
}
