const colors = {
    update: '#0B4C5F',
    create: '#0B3B24',
    delete: '#8A0829',
    read: '#B45F04',
    primary: '#0069b1',
    primaryText: '#212f6f',
    greyBackground: 'rgba(0, 0, 0, 0.5)',
    primaryTitle: '#291720',
    greyBorder: '#efefef',
    grey: '#CED3DC',
    darkGrey: '#9899A6',
    save: '#6DA34D',
    upload: '#0B4C5F',
    darkPrimary: '#A30B37',
    color: '#5C8DF6',
    colorRight: '#041a49',
    colorLeft: '#010918',
    shadow: '#98a7c575',
};

export default colors;
