import { gql } from '@apollo/client';
import * as Apollo from '@apollo/client';
export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  /** A date-time string at UTC, such as 2019-12-03T09:54:33Z, compliant with the date-time format. */
  DateTime: any;
  /** The `Upload` scalar type represents a file upload. */
  Upload: any;
};

export type Area = {
  __typename?: 'Area';
  id: Scalars['Int'];
  name: Scalars['String'];
};

export type Category = {
  __typename?: 'Category';
  id: Scalars['Int'];
  name: Scalars['String'];
};

export type CompanyType = {
  __typename?: 'CompanyType';
  id: Scalars['Int'];
  type: Scalars['String'];
};

export type EventType = {
  __typename?: 'EventType';
  id: Scalars['Int'];
  type: Scalars['String'];
};

export type UserType = {
  __typename?: 'UserType';
  id: Scalars['Int'];
  type: Scalars['String'];
};

export type Company = {
  __typename?: 'Company';
  id: Scalars['Int'];
  name: Scalars['String'];
  postCode?: Maybe<Scalars['String']>;
  address1: Scalars['String'];
  address2?: Maybe<Scalars['String']>;
  tel: Scalars['String'];
  fax?: Maybe<Scalars['String']>;
  categoryId: Scalars['Float'];
  category?: Maybe<Category>;
  companyTypeId: Scalars['Float'];
  companyType?: Maybe<CompanyType>;
  areaId: Scalars['Float'];
  area?: Maybe<Area>;
};

export type CompanyPaginate = {
  __typename?: 'CompanyPaginate';
  count: Scalars['Int'];
  items: Array<Company>;
};

export type User = {
  __typename?: 'User';
  id: Scalars['Int'];
  email: Scalars['String'];
  role: Scalars['Float'];
  name: Scalars['String'];
  department: Scalars['String'];
  jobTitle: Scalars['String'];
  mobile: Scalars['String'];
  fax?: Maybe<Scalars['String']>;
  postCode?: Maybe<Scalars['String']>;
  address1: Scalars['String'];
  address2?: Maybe<Scalars['String']>;
  companyId: Scalars['Int'];
  company?: Maybe<Company>;
  userTypeId: Scalars['Int'];
  forwardFromId?: Maybe<Scalars['Int']>;
  userType?: Maybe<UserType>;
  access_token?: Maybe<Scalars['String']>;
  expiresTime?: Maybe<Scalars['Float']>;
};

export type UserPaginate = {
  __typename?: 'UserPaginate';
  count: Scalars['Int'];
  items: Array<User>;
};

export type UserEvent = {
  __typename?: 'UserEvent';
  id: Scalars['String'];
  email: Scalars['String'];
  role: Scalars['Float'];
  password?: Maybe<Scalars['String']>;
  nameKanji: Scalars['String'];
  jobTitle?: Maybe<Scalars['String']>;
  department?: Maybe<Scalars['String']>;
  companyName: Scalars['String'];
  tel: Scalars['String'];
  address?: Maybe<Scalars['String']>;
  access_token?: Maybe<Scalars['String']>;
  expiresTime?: Maybe<Scalars['Float']>;
  fcmToken?: Maybe<Scalars['String']>;
  companyAddress?: Maybe<Scalars['String']>;
};

export type UserEventRelation = {
  __typename?: 'UserEventRelation';
  user: UserEvent;
  checkinAt?: Maybe<Scalars['DateTime']>;
  isAttend: Scalars['Boolean'];
  isResponseForm: Scalars['Boolean'];
};


export type Event = {
  __typename?: 'Event';
  id: Scalars['String'];
  name: Scalars['String'];
  description?: Maybe<Scalars['String']>;
  /** format: YYYY/MM/DD */
  dateFrom?: Maybe<Scalars['DateTime']>;
  /** format: YYYY/MM/DD */
  dateTo?: Maybe<Scalars['DateTime']>;
  /** format: hh:mm */
  timeFrom?: Maybe<Scalars['String']>;
  /** format: hh:mm */
  timeTo?: Maybe<Scalars['String']>;
  placeName: Scalars['String'];
  placeAddress: Scalars['String'];
  staffId?: Maybe<Scalars['Float']>;
  staff?: Maybe<User>;
  formAddress?: Maybe<Scalars['String']>;
  thumbnail?: Maybe<Scalars['String']>;
  createdAt: Scalars['DateTime'];
  updatedAt: Scalars['DateTime'];
};

export type EventPaginate = {
  __typename?: 'EventPaginate';
  count: Scalars['Int'];
  items: Array<Event>;
};

export type UserEventPaginate = {
  __typename?: 'UserEventPaginate';
  count: Scalars['Int'];
  items: Array<UserEventRelation>;
};

export type StatisticEventUser = {
  __typename?: 'StatisticEventUser';
  attendCount: Scalars['Float'];
  submitFormCount: Scalars['Float'];
  total: Scalars['Float'];
};

export type MailGroup = {
  __typename?: 'MailGroup';
  id: Scalars['Int'];
  name: Scalars['String'];
  areaId?: Maybe<Scalars['Float']>;
  area?: Maybe<Area>;
  eventId?: Maybe<Scalars['String']>;
  event?: Maybe<Event>;
  companyId?: Maybe<Scalars['Float']>;
  company?: Maybe<Company>;
  companyTypeId?: Maybe<Scalars['Float']>;
  companyType?: Maybe<CompanyType>;
  categoryId?: Maybe<Scalars['Float']>;
  category?: Maybe<Category>;
  userTypeId?: Maybe<Scalars['Float']>;
  userType?: Maybe<UserType>;
  /** Danh sách id của những users sẽ nhận được mail mà không phụ thuộc vào đều kiện filter của group */
  whiteUsers?: Maybe<Array<User>>;
  /** Danh sách id của những users sẽ KHÔNG nhận được mail */
  blackUsers?: Maybe<Array<User>>;
  /** Danh sách id của những event cũ.(Mục đích: dùng để lấy danh sách những users đã nhận được mail từ những event cũ này sẽ nhận được mail ở group này) */
  subEvents?: Maybe<Array<Event>>;
  userCreateId: Scalars['Float'];
  userCreate?: Maybe<User>;
  userLatestId?: Maybe<Scalars['Float']>;
  userLatest?: Maybe<User>;
};

export type MailGroupPaginate = {
  __typename?: 'MailGroupPaginate';
  count: Scalars['Int'];
  items: Array<MailGroup>;
};

export type MailGroupUsersPaginate = {
  __typename?: 'MailGroupUsersPaginate';
  count: Scalars['Int'];
  items: Array<User>;
};

export type SubMail = {
  __typename?: 'SubMail';
  id: Scalars['Int'];
  title: Scalars['String'];
  body: Scalars['String'];
  sended: Scalars['Boolean'];
  createdAt: Scalars['DateTime'];
  updatedAt: Scalars['DateTime'];
  parentId: Scalars['Int'];
  userCreateId: Scalars['Float'];
  userCreate?: Maybe<User>;
  userSendId?: Maybe<Scalars['Float']>;
  userSend?: Maybe<User>;
};

export type Mail = {
  __typename?: 'Mail';
  id: Scalars['Int'];
  title: Scalars['String'];
  body?: Maybe<Scalars['String']>;
  sended?: Maybe<Scalars['Boolean']>;
  sendAt?: Maybe<Scalars['DateTime']>;
  createdAt: Scalars['DateTime'];
  updatedAt: Scalars['DateTime'];
  mailGroupId: Scalars['Float'];
  mailGroup?: Maybe<MailGroup>;
  subMails?: Maybe<Array<SubMail>>;
  userCreateId: Scalars['Float'];
  userCreate?: Maybe<User>;
  userSendId?: Maybe<Scalars['Float']>;
  userSend?: Maybe<User>;
};

export type MailPaginate = {
  __typename?: 'MailPaginate';
  count: Scalars['Int'];
  items: Array<Mail>;
};

export type StatisticEvent = {
  __typename?: 'StatisticEvent';
  mailCount: Scalars['Int'];
  viewedLink: Scalars['Int'];
  attend: Scalars['Int'];
  livestream: Scalars['Int'];
  postOffice: Scalars['Int'];
};

export type MailUser = {
  __typename?: 'MailUser';
  user: User;
  event?: Maybe<Event>;
  mail: Mail;
};

export type Message = {
  __typename?: 'Message';
  id: Scalars['String'];
  eventId: Scalars['String'];
  message: Scalars['String'];
  type: MessageType;
};

export enum MessageType {
  All = 'ALL',
  AllRegistered = 'ALL_REGISTERED',
  AllNonRegistered = 'ALL_NON_REGISTERED',
  Attended = 'ATTENDED',
  AnswerForm = 'ANSWER_FORM',
  AttendedAnswerForm = 'ATTENDED_ANSWER_FORM'
}

export type Query = {
  __typename?: 'Query';
  area: Area;
  areas: Array<Area>;
  category: Category;
  categories: Array<Category>;
  companyType: CompanyType;
  companyTypes: Array<CompanyType>;
  eventType: EventType;
  eventTypes: Array<EventType>;
  userType: UserType;
  userTypes: Array<UserType>;
  company: Company;
  companies: CompanyPaginate;
  user: User;
  users: UserPaginate;
  event: Event;
  events: EventPaginate;
  myEvents: EventPaginate;
  getUserJoinEvent: UserEventPaginate;
  isJoinEvent: Scalars['Boolean'];
  isResponseForm: Scalars['Boolean'];
  statisticEventUser: StatisticEventUser;
  myProfile: UserEvent;
  mailGroup: MailGroup;
  mailGroups: MailGroupPaginate;
  usersByMailGroup: MailGroupUsersPaginate;
  mail: Mail;
  statisticEvent: StatisticEvent;
  eventByMailUser: MailUser;
  mails: MailPaginate;
};


export type QueryAreaArgs = {
  id: Scalars['Int'];
};


export type QueryCategoryArgs = {
  id: Scalars['Int'];
};


export type QueryCompanyTypeArgs = {
  id: Scalars['Int'];
};


export type QueryEventTypeArgs = {
  id: Scalars['Int'];
};


export type QueryUserTypeArgs = {
  id: Scalars['Int'];
};


export type QueryCompanyArgs = {
  id: Scalars['Int'];
};


export type QueryCompaniesArgs = {
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  orderBy?: Maybe<SortOptions>;
  searchText?: Maybe<Scalars['String']>;
  companyTypeId?: Maybe<Scalars['Int']>;
  categoryId?: Maybe<Scalars['Int']>;
  areaId?: Maybe<Scalars['Int']>;
};


export type QueryUserArgs = {
  id: Scalars['Int'];
};


export type QueryUsersArgs = {
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  orderBy?: Maybe<SortOptions>;
  searchText?: Maybe<Scalars['String']>;
  companyId?: Maybe<Scalars['Int']>;
  userTypeId?: Maybe<Scalars['Int']>;
};


export type QueryEventArgs = {
  id: Scalars['String'];
};


export type QueryEventsArgs = {
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  orderBy?: Maybe<SortOptions>;
  searchText?: Maybe<Scalars['String']>;
  today?: Maybe<Scalars['DateTime']>;
  staffId?: Maybe<Scalars['Int']>;
};


export type QueryMyEventsArgs = {
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  orderBy?: Maybe<SortOptions>;
  searchText?: Maybe<Scalars['String']>;
  today?: Maybe<Scalars['DateTime']>;
  staffId?: Maybe<Scalars['Int']>;
};


export type QueryGetUserJoinEventArgs = {
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  orderBy?: Maybe<SortOptions>;
  searchText?: Maybe<Scalars['String']>;
  isCheck?: Maybe<Scalars['Boolean']>;
  eventId: Scalars['String'];
};


export type QueryIsJoinEventArgs = {
  eventId: Scalars['String'];
};


export type QueryIsResponseFormArgs = {
  eventId: Scalars['String'];
};


export type QueryStatisticEventUserArgs = {
  id: Scalars['String'];
};


export type QueryMailGroupArgs = {
  id: Scalars['Int'];
};


export type QueryMailGroupsArgs = {
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  orderBy?: Maybe<SortOptions>;
  searchText?: Maybe<Scalars['String']>;
  eventId?: Maybe<Scalars['Int']>;
  companyId?: Maybe<Scalars['Int']>;
  companyTypeId?: Maybe<Scalars['Int']>;
  areaId?: Maybe<Scalars['Int']>;
  categoryId?: Maybe<Scalars['Int']>;
  userTypeId?: Maybe<Scalars['Int']>;
};


export type QueryUsersByMailGroupArgs = {
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  orderBy?: Maybe<SortOptions>;
  searchText?: Maybe<Scalars['String']>;
  mailGroupId: Scalars['Int'];
};


export type QueryMailArgs = {
  id: Scalars['Int'];
};


export type QueryStatisticEventArgs = {
  mailId: Scalars['Int'];
};


export type QueryEventByMailUserArgs = {
  mailUserId: Scalars['String'];
};


export type QueryMailsArgs = {
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  orderBy?: Maybe<SortOptions>;
  searchText?: Maybe<Scalars['String']>;
  sended?: Maybe<Scalars['Boolean']>;
  mailGroupId?: Maybe<Scalars['Int']>;
};

export type SortOptions = {
  field?: Maybe<Scalars['String']>;
  direction?: Maybe<Direction>;
};

export enum Direction {
  Asc = 'ASC',
  Desc = 'DESC'
}

export type Mutation = {
  __typename?: 'Mutation';
  login: User;
  loginSmartphone: UserEvent;
  createArea: Area;
  updateArea: Area;
  deleteArea: Scalars['Boolean'];
  createCategory: Category;
  updateCategory: Category;
  deleteCategory: Scalars['Boolean'];
  createCompanyType: CompanyType;
  updateCompanyType: CompanyType;
  deleteCompanyType: Scalars['Boolean'];
  createEventType: EventType;
  updateEventType: EventType;
  deleteEventType: Scalars['Boolean'];
  createUserType: UserType;
  updateUserType: UserType;
  deleteUserType: Scalars['Boolean'];
  createCompany: Company;
  updateCompany: Company;
  deleteCompanies: Scalars['Boolean'];
  createUser: User;
  updateUser: User;
  deleteUsers: Scalars['Boolean'];
  createEvent: Event;
  updateEvent: Event;
  deleteEvents: Scalars['Boolean'];
  joinEvent: Scalars['Boolean'];
  attendEvent: Scalars['Boolean'];
  responseForm: Scalars['Boolean'];
  createUserEvent: UserEvent;
  updateProfile: UserEvent;
  deleteUserEvent: Scalars['Boolean'];
  createMailGroup: MailGroup;
  mergeMailGroup: MailGroup;
  updateMailGroup: MailGroup;
  deleteMailGroups: Scalars['Boolean'];
  addWhiteUserMailGroup: Scalars['Boolean'];
  removeWhiteUserMailGroup: Scalars['Boolean'];
  addBlackUserMailGroup: Scalars['Boolean'];
  removeBlackUserMailGroup: Scalars['Boolean'];
  createMail: Mail;
  updateMail: Mail;
  deleteMails: Scalars['Boolean'];
  trackingEventAttendees: Scalars['Boolean'];
  sendMailTest: Scalars['Boolean'];
  forwardMail: Scalars['Boolean'];
  createSubMail: SubMail;
  updateSubMail: SubMail;
  deleteSubMails: Scalars['Boolean'];
  uploadFile: Scalars['String'];
  createMessage: Message;
};


export type MutationLoginArgs = {
  email: Scalars['String'];
  password: Scalars['String'];
};


export type MutationLoginSmartphoneArgs = {
  email: Scalars['String'];
  password: Scalars['String'];
};


export type MutationCreateAreaArgs = {
  area: CreateAreaDto;
};


export type MutationUpdateAreaArgs = {
  area: UpdateAreaDto;
  id: Scalars['Int'];
};


export type MutationDeleteAreaArgs = {
  id: Scalars['Int'];
};


export type MutationCreateCategoryArgs = {
  category: CreateCategoryDto;
};


export type MutationUpdateCategoryArgs = {
  category: UpdateCategoryDto;
  id: Scalars['Int'];
};


export type MutationDeleteCategoryArgs = {
  id: Scalars['Int'];
};


export type MutationCreateCompanyTypeArgs = {
  companyType: CreateCompanyTypeDto;
};


export type MutationUpdateCompanyTypeArgs = {
  companyType: UpdateCompanyTypeDto;
  id: Scalars['Int'];
};


export type MutationDeleteCompanyTypeArgs = {
  id: Scalars['Int'];
};


export type MutationCreateEventTypeArgs = {
  eventType: CreateEventTypeDto;
};


export type MutationUpdateEventTypeArgs = {
  eventType: UpdateEventTypeDto;
  id: Scalars['Int'];
};


export type MutationDeleteEventTypeArgs = {
  id: Scalars['Int'];
};


export type MutationCreateUserTypeArgs = {
  userType: CreateUserTypeDto;
};


export type MutationUpdateUserTypeArgs = {
  userType: UpdateUserTypeDto;
  id: Scalars['Int'];
};


export type MutationDeleteUserTypeArgs = {
  id: Scalars['Int'];
};


export type MutationCreateCompanyArgs = {
  company: CreateCompanyDto;
};


export type MutationUpdateCompanyArgs = {
  company: UpdateCompanyDto;
  id: Scalars['Int'];
};


export type MutationDeleteCompaniesArgs = {
  ids: Array<Scalars['Int']>;
};


export type MutationCreateUserArgs = {
  user: CreateUserDto;
};


export type MutationUpdateUserArgs = {
  user: UpdateUserDto;
  id: Scalars['Int'];
};


export type MutationDeleteUsersArgs = {
  ids: Array<Scalars['Int']>;
};


export type MutationCreateEventArgs = {
  event: CreateEventDto;
};


export type MutationUpdateEventArgs = {
  event: UpdateEventDto;
  id: Scalars['String'];
};


export type MutationDeleteEventsArgs = {
  ids: Array<Scalars['String']>;
};


export type MutationJoinEventArgs = {
  eventId: Scalars['String'];
};


export type MutationAttendEventArgs = {
  eventId: Scalars['String'];
  userId: Scalars['String'];
};


export type MutationResponseFormArgs = {
  userId: Scalars['String'];
  eventId: Scalars['String'];
};


export type MutationCreateUserEventArgs = {
  user: CreateUserEventDto;
};


export type MutationUpdateProfileArgs = {
  user: UpdateUserEventDto;
};


export type MutationDeleteUserEventArgs = {
  id: Scalars['String'];
};


export type MutationCreateMailGroupArgs = {
  mailGroup: CreateMailGroupDto;
};


export type MutationMergeMailGroupArgs = {
  mailGroup: MergeMailGroupDto;
};


export type MutationUpdateMailGroupArgs = {
  mailGroup: UpdateMailGroupDto;
  id: Scalars['Int'];
};


export type MutationDeleteMailGroupsArgs = {
  ids: Array<Scalars['Int']>;
};


export type MutationAddWhiteUserMailGroupArgs = {
  mailGroupId: Scalars['Int'];
  userId: Scalars['Int'];
};


export type MutationRemoveWhiteUserMailGroupArgs = {
  mailGroupId: Scalars['Int'];
  userId: Scalars['Int'];
};


export type MutationAddBlackUserMailGroupArgs = {
  mailGroupId: Scalars['Int'];
  userId: Scalars['Int'];
};


export type MutationRemoveBlackUserMailGroupArgs = {
  mailGroupId: Scalars['Int'];
  userId: Scalars['Int'];
};


export type MutationCreateMailArgs = {
  mail: CreateMailDto;
};


export type MutationUpdateMailArgs = {
  mail: UpdateMailDto;
  id: Scalars['Int'];
};


export type MutationDeleteMailsArgs = {
  ids: Array<Scalars['Int']>;
};


export type MutationTrackingEventAttendeesArgs = {
  forwardFromId?: Maybe<Scalars['Float']>;
  forwardUser?: Maybe<CreateUserDto>;
  tracking: EventAttendeesDto;
  mailUserId: Scalars['String'];
};


export type MutationSendMailTestArgs = {
  mail: CreateMailTestDto;
  mails: Scalars['String'];
};


export type MutationForwardMailArgs = {
  mailTo: Scalars['String'];
  mailUserId: Scalars['String'];
};


export type MutationCreateSubMailArgs = {
  subMail: CreateSubMailDto;
};


export type MutationUpdateSubMailArgs = {
  subMail: UpdateSubMailDto;
  id: Scalars['Int'];
};


export type MutationDeleteSubMailsArgs = {
  ids: Array<Scalars['Int']>;
};


export type MutationUploadFileArgs = {
  file: Scalars['Upload'];
};


export type MutationCreateMessageArgs = {
  createMessageInput: CreateMessageDto;
};

export type CreateAreaDto = {
  name: Scalars['String'];
};

export type UpdateAreaDto = {
  name: Scalars['String'];
};

export type CreateCategoryDto = {
  name: Scalars['String'];
};

export type UpdateCategoryDto = {
  name: Scalars['String'];
};

export type CreateCompanyTypeDto = {
  type: Scalars['String'];
};

export type UpdateCompanyTypeDto = {
  type: Scalars['String'];
};

export type CreateEventTypeDto = {
  type: Scalars['String'];
};

export type UpdateEventTypeDto = {
  type: Scalars['String'];
};

export type CreateUserTypeDto = {
  type: Scalars['String'];
};

export type UpdateUserTypeDto = {
  type: Scalars['String'];
};

export type CreateCompanyDto = {
  name: Scalars['String'];
  postCode: Scalars['String'];
  address1: Scalars['String'];
  address2?: Maybe<Scalars['String']>;
  tel: Scalars['String'];
  fax?: Maybe<Scalars['String']>;
  categoryId: Scalars['Int'];
  companyTypeId: Scalars['Int'];
  areaId: Scalars['Int'];
};

export type UpdateCompanyDto = {
  name?: Maybe<Scalars['String']>;
  postCode?: Maybe<Scalars['String']>;
  address1?: Maybe<Scalars['String']>;
  address2?: Maybe<Scalars['String']>;
  tel?: Maybe<Scalars['String']>;
  fax?: Maybe<Scalars['String']>;
  categoryId?: Maybe<Scalars['Float']>;
  companyTypeId?: Maybe<Scalars['Float']>;
  areaId?: Maybe<Scalars['Float']>;
};

export type CreateUserDto = {
  email: Scalars['String'];
  name: Scalars['String'];
  department: Scalars['String'];
  jobTitle: Scalars['String'];
  mobile: Scalars['String'];
  fax?: Maybe<Scalars['String']>;
  postCode?: Maybe<Scalars['String']>;
  address1: Scalars['String'];
  address2?: Maybe<Scalars['String']>;
  companyId: Scalars['Int'];
  userTypeId: Scalars['Int'];
  forwardFromId?: Maybe<Scalars['Float']>;
  access_token?: Maybe<Scalars['String']>;
  expiresTime?: Maybe<Scalars['Float']>;
  password?: Maybe<Scalars['String']>;
};

export type UpdateUserDto = {
  email?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  department?: Maybe<Scalars['String']>;
  jobTitle?: Maybe<Scalars['String']>;
  mobile?: Maybe<Scalars['String']>;
  fax?: Maybe<Scalars['String']>;
  postCode?: Maybe<Scalars['String']>;
  address1?: Maybe<Scalars['String']>;
  address2?: Maybe<Scalars['String']>;
  companyId?: Maybe<Scalars['Int']>;
  userTypeId?: Maybe<Scalars['Int']>;
  forwardFromId?: Maybe<Scalars['Int']>;
  access_token?: Maybe<Scalars['String']>;
  expiresTime?: Maybe<Scalars['Float']>;
  password?: Maybe<Scalars['String']>;
};

export type CreateEventDto = {
  name: Scalars['String'];
  description?: Maybe<Scalars['String']>;
  /** format: YYYY/MM/DD */
  dateFrom: Scalars['DateTime'];
  /** format: YYYY/MM/DD */
  dateTo: Scalars['DateTime'];
  /** format: hh:mm */
  timeFrom: Scalars['String'];
  /** format: hh:mm */
  timeTo: Scalars['String'];
  placeName: Scalars['String'];
  placeAddress: Scalars['String'];
  formAddress?: Maybe<Scalars['String']>;
  thumbnail?: Maybe<Scalars['String']>;
};

export type UpdateEventDto = {
  name?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  /** format: YYYY/MM/DD */
  dateFrom?: Maybe<Scalars['DateTime']>;
  /** format: YYYY/MM/DD */
  dateTo?: Maybe<Scalars['DateTime']>;
  /** format: hh:mm */
  timeFrom?: Maybe<Scalars['String']>;
  /** format: hh:mm */
  timeTo?: Maybe<Scalars['String']>;
  placeName?: Maybe<Scalars['String']>;
  placeAddress?: Maybe<Scalars['String']>;
  formAddress?: Maybe<Scalars['String']>;
  thumbnail?: Maybe<Scalars['String']>;
};

export type CreateUserEventDto = {
  email: Scalars['String'];
  role: Scalars['Float'];
  password?: Maybe<Scalars['String']>;
  nameKanji: Scalars['String'];
  jobTitle?: Maybe<Scalars['String']>;
  department?: Maybe<Scalars['String']>;
  companyName: Scalars['String'];
  tel: Scalars['String'];
  address?: Maybe<Scalars['String']>;
  fcmToken?: Maybe<Scalars['String']>;
  companyAddress?: Maybe<Scalars['String']>;
};

export type UpdateUserEventDto = {
  role?: Maybe<Scalars['Float']>;
  password?: Maybe<Scalars['String']>;
  nameKanji?: Maybe<Scalars['String']>;
  jobTitle?: Maybe<Scalars['String']>;
  department?: Maybe<Scalars['String']>;
  companyName?: Maybe<Scalars['String']>;
  tel?: Maybe<Scalars['String']>;
  address?: Maybe<Scalars['String']>;
  fcmToken?: Maybe<Scalars['String']>;
  companyAddress?: Maybe<Scalars['String']>;
};

export type CreateMailGroupDto = {
  name: Scalars['String'];
  areaId?: Maybe<Scalars['Float']>;
  eventId?: Maybe<Scalars['String']>;
  companyId?: Maybe<Scalars['Float']>;
  companyTypeId?: Maybe<Scalars['Float']>;
  categoryId?: Maybe<Scalars['Float']>;
  userTypeId?: Maybe<Scalars['Float']>;
  whiteUserIds?: Maybe<Array<Scalars['Int']>>;
  blackUserIds?: Maybe<Array<Scalars['Int']>>;
  subEventIds?: Maybe<Array<Scalars['Int']>>;
};

export type MergeMailGroupDto = {
  name: Scalars['String'];
  areaId?: Maybe<Scalars['Float']>;
  eventId?: Maybe<Scalars['String']>;
  companyId?: Maybe<Scalars['Float']>;
  companyTypeId?: Maybe<Scalars['Float']>;
  categoryId?: Maybe<Scalars['Float']>;
  userTypeId?: Maybe<Scalars['Float']>;
  mailGroupIds?: Maybe<Array<Scalars['Int']>>;
};

export type UpdateMailGroupDto = {
  name?: Maybe<Scalars['String']>;
  areaId?: Maybe<Scalars['Float']>;
  eventId?: Maybe<Scalars['String']>;
  companyId?: Maybe<Scalars['Float']>;
  companyTypeId?: Maybe<Scalars['Float']>;
  categoryId?: Maybe<Scalars['Float']>;
  userTypeId?: Maybe<Scalars['Float']>;
  whiteUserIds?: Maybe<Array<Scalars['Int']>>;
  blackUserIds?: Maybe<Array<Scalars['Int']>>;
  subEventIds?: Maybe<Array<Scalars['Int']>>;
};

export type CreateMailDto = {
  title: Scalars['String'];
  body: Scalars['String'];
  sended?: Maybe<Scalars['Boolean']>;
  sendAt?: Maybe<Scalars['DateTime']>;
  mailGroupId: Scalars['Float'];
};

export type UpdateMailDto = {
  title?: Maybe<Scalars['String']>;
  body?: Maybe<Scalars['String']>;
  sended?: Maybe<Scalars['Boolean']>;
  sendAt?: Maybe<Scalars['DateTime']>;
  mailGroupId?: Maybe<Scalars['Float']>;
};

export type EventAttendeesDto = {
  viewedLink?: Maybe<Scalars['Boolean']>;
  attendFlag?: Maybe<Scalars['Boolean']>;
  livestreamFlag?: Maybe<Scalars['Boolean']>;
  postOfficeFlag?: Maybe<Scalars['Boolean']>;
};

export type CreateMailTestDto = {
  title: Scalars['String'];
  body: Scalars['String'];
  mailGroupId: Scalars['Float'];
};

export type CreateSubMailDto = {
  title: Scalars['String'];
  body: Scalars['String'];
  sended?: Maybe<Scalars['Boolean']>;
  parentId: Scalars['Int'];
  viewedLink?: Maybe<Scalars['Boolean']>;
  attendFlag?: Maybe<Scalars['Boolean']>;
  livestreamFlag?: Maybe<Scalars['Boolean']>;
  postOfficeFlag?: Maybe<Scalars['Boolean']>;
};

export type UpdateSubMailDto = {
  title?: Maybe<Scalars['String']>;
  body?: Maybe<Scalars['String']>;
  sended?: Maybe<Scalars['Boolean']>;
  parentId?: Maybe<Scalars['Int']>;
  viewedLink?: Maybe<Scalars['Boolean']>;
  attendFlag?: Maybe<Scalars['Boolean']>;
  livestreamFlag?: Maybe<Scalars['Boolean']>;
  postOfficeFlag?: Maybe<Scalars['Boolean']>;
};


export type CreateMessageDto = {
  eventId: Scalars['String'];
  message?: Maybe<Scalars['String']>;
  type?: Maybe<MessageType>;
};

export type DeleteEventsMutationVariables = Exact<{
  ids: Array<Scalars['String']> | Scalars['String'];
}>;


export type DeleteEventsMutation = (
  { __typename?: 'Mutation' }
  & Pick<Mutation, 'deleteEvents'>
);

export type CreateEventMutationVariables = Exact<{
  event: CreateEventDto;
}>;


export type CreateEventMutation = (
  { __typename?: 'Mutation' }
  & { createEvent: (
    { __typename?: 'Event' }
    & Pick<Event, 'id' | 'name'>
  ) }
);

export type ForwardMailMutationVariables = Exact<{
  mailTo: Scalars['String'];
  mailUserId: Scalars['String'];
}>;


export type ForwardMailMutation = (
  { __typename?: 'Mutation' }
  & Pick<Mutation, 'forwardMail'>
);

export type UpdateEventMutationVariables = Exact<{
  event: UpdateEventDto;
  id: Scalars['String'];
}>;


export type UpdateEventMutation = (
  { __typename?: 'Mutation' }
  & { updateEvent: (
    { __typename?: 'Event' }
    & Pick<Event, 'id' | 'name' | 'description' | 'placeName' | 'placeAddress' | 'dateFrom' | 'dateTo' | 'timeFrom' | 'timeTo' | 'formAddress' | 'staffId'>
    & { staff?: Maybe<(
      { __typename?: 'User' }
      & Pick<User, 'id' | 'name'>
    )> }
  ) }
);

export type CreateMessageMutationVariables = Exact<{
  createMessageInput: CreateMessageDto;
}>;


export type CreateMessageMutation = (
  { __typename?: 'Mutation' }
  & { createMessage: (
    { __typename?: 'Message' }
    & Pick<Message, 'id' | 'eventId' | 'message' | 'type'>
  ) }
);

export type UploadFileMutationVariables = Exact<{
  file: Scalars['Upload'];
}>;


export type UploadFileMutation = (
  { __typename?: 'Mutation' }
  & Pick<Mutation, 'uploadFile'>
);

export type LoginMutationVariables = Exact<{
  email: Scalars['String'];
  password: Scalars['String'];
}>;


export type LoginMutation = (
  { __typename?: 'Mutation' }
  & { login: (
    { __typename?: 'User' }
    & Pick<User, 'access_token' | 'name' | 'email'>
  ) }
);

export type LoginSmartphoneMutationVariables = Exact<{
  email: Scalars['String'];
  password: Scalars['String'];
}>;


export type LoginSmartphoneMutation = (
  { __typename?: 'Mutation' }
  & { loginSmartphone: (
    { __typename?: 'UserEvent' }
    & Pick<UserEvent, 'access_token' | 'email' | 'nameKanji' | 'id'>
  ) }
);

export type TrackingEventAttendeesMutationVariables = Exact<{
  forwardFromId?: Maybe<Scalars['Float']>;
  forwardUser?: Maybe<CreateUserDto>;
  tracking: EventAttendeesDto;
  mailUserId: Scalars['String'];
}>;


export type TrackingEventAttendeesMutation = (
  { __typename?: 'Mutation' }
  & Pick<Mutation, 'trackingEventAttendees'>
);

export type DeleteUsersMutationVariables = Exact<{
  ids: Array<Scalars['Int']> | Scalars['Int'];
}>;


export type DeleteUsersMutation = (
  { __typename?: 'Mutation' }
  & Pick<Mutation, 'deleteUsers'>
);

export type CreateUserMutationVariables = Exact<{
  user: CreateUserDto;
}>;


export type CreateUserMutation = (
  { __typename?: 'Mutation' }
  & { createUser: (
    { __typename?: 'User' }
    & Pick<User, 'id' | 'email'>
  ) }
);

export type UpdateUserMutationVariables = Exact<{
  user: UpdateUserDto;
  id: Scalars['Int'];
}>;


export type UpdateUserMutation = (
  { __typename?: 'Mutation' }
  & { updateUser: (
    { __typename?: 'User' }
    & Pick<User, 'id' | 'companyId' | 'name' | 'userTypeId' | 'department' | 'jobTitle' | 'mobile' | 'email' | 'postCode' | 'address1' | 'address2' | 'fax' | 'role'>
  ) }
);

export type EventsQueryVariables = Exact<{
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  searchText?: Maybe<Scalars['String']>;
  field?: Maybe<Scalars['String']>;
  direction?: Maybe<Direction>;
}>;


export type EventsQuery = (
  { __typename?: 'Query' }
  & { events: (
    { __typename?: 'EventPaginate' }
    & Pick<EventPaginate, 'count'>
    & { items: Array<(
      { __typename?: 'Event' }
      & Pick<Event, 'id' | 'name' | 'description' | 'placeName' | 'placeAddress' | 'dateFrom' | 'dateTo' | 'timeFrom' | 'timeTo' | 'formAddress' | 'createdAt' | 'updatedAt'>
    )> }
  ) }
);

export type EventByMailUserQueryVariables = Exact<{
  mailUserId: Scalars['String'];
}>;


export type EventByMailUserQuery = (
  { __typename?: 'Query' }
  & { eventByMailUser: (
    { __typename?: 'MailUser' }
    & { user: (
      { __typename?: 'User' }
      & Pick<User, 'id' | 'email' | 'name'>
    ), event?: Maybe<(
      { __typename?: 'Event' }
      & Pick<Event, 'name' | 'id'>
    )> }
  ) }
);

export type EventQueryVariables = Exact<{
  id: Scalars['String'];
}>;


export type EventQuery = (
  { __typename?: 'Query' }
  & { event: (
    { __typename?: 'Event' }
    & Pick<Event, 'id' | 'name' | 'description' | 'dateFrom' | 'dateTo' | 'timeFrom' | 'timeTo' | 'placeName' | 'placeAddress' | 'formAddress' | 'createdAt' | 'updatedAt'>
  ) }
);

export type StatisticEventUserQueryVariables = Exact<{
  id: Scalars['String'];
}>;


export type StatisticEventUserQuery = (
  { __typename?: 'Query' }
  & { statisticEventUser: (
    { __typename?: 'StatisticEventUser' }
    & Pick<StatisticEventUser, 'attendCount' | 'submitFormCount' | 'total'>
  ) }
);

export type UsersQueryVariables = Exact<{
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  searchText?: Maybe<Scalars['String']>;
}>;


export type UsersQuery = (
  { __typename?: 'Query' }
  & { users: (
    { __typename?: 'UserPaginate' }
    & Pick<UserPaginate, 'count'>
    & { items: Array<(
      { __typename?: 'User' }
      & Pick<User, 'id' | 'companyId' | 'name' | 'userTypeId' | 'department' | 'jobTitle' | 'mobile' | 'email' | 'postCode' | 'address1' | 'address2' | 'fax' | 'role'>
      & { company?: Maybe<(
        { __typename?: 'Company' }
        & Pick<Company, 'id' | 'name'>
      )> }
    )> }
  ) }
);

export type UserQueryVariables = Exact<{
  id: Scalars['Int'];
}>;


export type UserQuery = (
  { __typename?: 'Query' }
  & { user: (
    { __typename?: 'User' }
    & Pick<User, 'id' | 'companyId' | 'name' | 'userTypeId' | 'department' | 'jobTitle' | 'mobile' | 'email' | 'postCode' | 'address1' | 'address2' | 'fax' | 'role'>
    & { company?: Maybe<(
      { __typename?: 'Company' }
      & Pick<Company, 'id' | 'name'>
    )> }
  ) }
);

export type Users_ByMailGroupQueryVariables = Exact<{
  mailGroupId: Scalars['Int'];
}>;


export type Users_ByMailGroupQuery = (
  { __typename?: 'Query' }
  & { usersByMailGroup: (
    { __typename?: 'MailGroupUsersPaginate' }
    & Pick<MailGroupUsersPaginate, 'count'>
    & { items: Array<(
      { __typename?: 'User' }
      & Pick<User, 'id' | 'companyId' | 'name' | 'userTypeId' | 'department' | 'jobTitle' | 'mobile' | 'email' | 'postCode' | 'address1' | 'address2' | 'fax' | 'role'>
      & { company?: Maybe<(
        { __typename?: 'Company' }
        & Pick<Company, 'id' | 'name'>
      )> }
    )> }
  ) }
);

export type Users_By_MailGroupQueryVariables = Exact<{
  mailGroupId: Scalars['Int'];
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  searchText?: Maybe<Scalars['String']>;
}>;


export type Users_By_MailGroupQuery = (
  { __typename?: 'Query' }
  & { usersByMailGroup: (
    { __typename?: 'MailGroupUsersPaginate' }
    & Pick<MailGroupUsersPaginate, 'count'>
    & { items: Array<(
      { __typename?: 'User' }
      & Pick<User, 'id' | 'companyId' | 'name' | 'userTypeId' | 'department' | 'jobTitle' | 'mobile' | 'email' | 'postCode' | 'address1' | 'address2' | 'fax' | 'role'>
      & { company?: Maybe<(
        { __typename?: 'Company' }
        & Pick<Company, 'id' | 'name'>
      )> }
    )> }
  ) }
);


export const DeleteEventsDocument = gql`
    mutation deleteEvents($ids: [String!]!) {
  deleteEvents(ids: $ids)
}
    `;
export type DeleteEventsMutationFn = Apollo.MutationFunction<DeleteEventsMutation, DeleteEventsMutationVariables>;

/**
 * __useDeleteEventsMutation__
 *
 * To run a mutation, you first call `useDeleteEventsMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteEventsMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteEventsMutation, { data, loading, error }] = useDeleteEventsMutation({
 *   variables: {
 *      ids: // value for 'ids'
 *   },
 * });
 */
export function useDeleteEventsMutation(baseOptions?: Apollo.MutationHookOptions<DeleteEventsMutation, DeleteEventsMutationVariables>) {
        return Apollo.useMutation<DeleteEventsMutation, DeleteEventsMutationVariables>(DeleteEventsDocument, baseOptions);
      }
export type DeleteEventsMutationHookResult = ReturnType<typeof useDeleteEventsMutation>;
export type DeleteEventsMutationResult = Apollo.MutationResult<DeleteEventsMutation>;
export type DeleteEventsMutationOptions = Apollo.BaseMutationOptions<DeleteEventsMutation, DeleteEventsMutationVariables>;
export const CreateEventDocument = gql`
    mutation createEvent($event: CreateEventDTO!) {
  createEvent(event: $event) {
    id
    name
  }
}
    `;
export type CreateEventMutationFn = Apollo.MutationFunction<CreateEventMutation, CreateEventMutationVariables>;

/**
 * __useCreateEventMutation__
 *
 * To run a mutation, you first call `useCreateEventMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateEventMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createEventMutation, { data, loading, error }] = useCreateEventMutation({
 *   variables: {
 *      event: // value for 'event'
 *   },
 * });
 */
export function useCreateEventMutation(baseOptions?: Apollo.MutationHookOptions<CreateEventMutation, CreateEventMutationVariables>) {
        return Apollo.useMutation<CreateEventMutation, CreateEventMutationVariables>(CreateEventDocument, baseOptions);
      }
export type CreateEventMutationHookResult = ReturnType<typeof useCreateEventMutation>;
export type CreateEventMutationResult = Apollo.MutationResult<CreateEventMutation>;
export type CreateEventMutationOptions = Apollo.BaseMutationOptions<CreateEventMutation, CreateEventMutationVariables>;
export const ForwardMailDocument = gql`
    mutation forwardMail($mailTo: String!, $mailUserId: String!) {
  forwardMail(mailTo: $mailTo, mailUserId: $mailUserId)
}
    `;
export type ForwardMailMutationFn = Apollo.MutationFunction<ForwardMailMutation, ForwardMailMutationVariables>;

/**
 * __useForwardMailMutation__
 *
 * To run a mutation, you first call `useForwardMailMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useForwardMailMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [forwardMailMutation, { data, loading, error }] = useForwardMailMutation({
 *   variables: {
 *      mailTo: // value for 'mailTo'
 *      mailUserId: // value for 'mailUserId'
 *   },
 * });
 */
export function useForwardMailMutation(baseOptions?: Apollo.MutationHookOptions<ForwardMailMutation, ForwardMailMutationVariables>) {
        return Apollo.useMutation<ForwardMailMutation, ForwardMailMutationVariables>(ForwardMailDocument, baseOptions);
      }
export type ForwardMailMutationHookResult = ReturnType<typeof useForwardMailMutation>;
export type ForwardMailMutationResult = Apollo.MutationResult<ForwardMailMutation>;
export type ForwardMailMutationOptions = Apollo.BaseMutationOptions<ForwardMailMutation, ForwardMailMutationVariables>;
export const UpdateEventDocument = gql`
    mutation updateEvent($event: UpdateEventDTO!, $id: String!) {
  updateEvent(event: $event, id: $id) {
    id
    name
    description
    placeName
    placeAddress
    dateFrom
    dateTo
    timeFrom
    timeTo
    formAddress
    staffId
    staff {
      id
      name
    }
  }
}
    `;
export type UpdateEventMutationFn = Apollo.MutationFunction<UpdateEventMutation, UpdateEventMutationVariables>;

/**
 * __useUpdateEventMutation__
 *
 * To run a mutation, you first call `useUpdateEventMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateEventMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateEventMutation, { data, loading, error }] = useUpdateEventMutation({
 *   variables: {
 *      event: // value for 'event'
 *      id: // value for 'id'
 *   },
 * });
 */
export function useUpdateEventMutation(baseOptions?: Apollo.MutationHookOptions<UpdateEventMutation, UpdateEventMutationVariables>) {
        return Apollo.useMutation<UpdateEventMutation, UpdateEventMutationVariables>(UpdateEventDocument, baseOptions);
      }
export type UpdateEventMutationHookResult = ReturnType<typeof useUpdateEventMutation>;
export type UpdateEventMutationResult = Apollo.MutationResult<UpdateEventMutation>;
export type UpdateEventMutationOptions = Apollo.BaseMutationOptions<UpdateEventMutation, UpdateEventMutationVariables>;
export const CreateMessageDocument = gql`
    mutation createMessage($createMessageInput: CreateMessageDTO!) {
  createMessage(createMessageInput: $createMessageInput) {
    id
    eventId
    message
    type
  }
}
    `;
export type CreateMessageMutationFn = Apollo.MutationFunction<CreateMessageMutation, CreateMessageMutationVariables>;

/**
 * __useCreateMessageMutation__
 *
 * To run a mutation, you first call `useCreateMessageMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateMessageMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createMessageMutation, { data, loading, error }] = useCreateMessageMutation({
 *   variables: {
 *      createMessageInput: // value for 'createMessageInput'
 *   },
 * });
 */
export function useCreateMessageMutation(baseOptions?: Apollo.MutationHookOptions<CreateMessageMutation, CreateMessageMutationVariables>) {
        return Apollo.useMutation<CreateMessageMutation, CreateMessageMutationVariables>(CreateMessageDocument, baseOptions);
      }
export type CreateMessageMutationHookResult = ReturnType<typeof useCreateMessageMutation>;
export type CreateMessageMutationResult = Apollo.MutationResult<CreateMessageMutation>;
export type CreateMessageMutationOptions = Apollo.BaseMutationOptions<CreateMessageMutation, CreateMessageMutationVariables>;
export const UploadFileDocument = gql`
    mutation uploadFile($file: Upload!) {
  uploadFile(file: $file)
}
    `;
export type UploadFileMutationFn = Apollo.MutationFunction<UploadFileMutation, UploadFileMutationVariables>;

/**
 * __useUploadFileMutation__
 *
 * To run a mutation, you first call `useUploadFileMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUploadFileMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [uploadFileMutation, { data, loading, error }] = useUploadFileMutation({
 *   variables: {
 *      file: // value for 'file'
 *   },
 * });
 */
export function useUploadFileMutation(baseOptions?: Apollo.MutationHookOptions<UploadFileMutation, UploadFileMutationVariables>) {
        return Apollo.useMutation<UploadFileMutation, UploadFileMutationVariables>(UploadFileDocument, baseOptions);
      }
export type UploadFileMutationHookResult = ReturnType<typeof useUploadFileMutation>;
export type UploadFileMutationResult = Apollo.MutationResult<UploadFileMutation>;
export type UploadFileMutationOptions = Apollo.BaseMutationOptions<UploadFileMutation, UploadFileMutationVariables>;
export const LoginDocument = gql`
    mutation login($email: String!, $password: String!) {
  login(email: $email, password: $password) {
    access_token
    name
    email
  }
}
    `;
export type LoginMutationFn = Apollo.MutationFunction<LoginMutation, LoginMutationVariables>;

/**
 * __useLoginMutation__
 *
 * To run a mutation, you first call `useLoginMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useLoginMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [loginMutation, { data, loading, error }] = useLoginMutation({
 *   variables: {
 *      email: // value for 'email'
 *      password: // value for 'password'
 *   },
 * });
 */
export function useLoginMutation(baseOptions?: Apollo.MutationHookOptions<LoginMutation, LoginMutationVariables>) {
        return Apollo.useMutation<LoginMutation, LoginMutationVariables>(LoginDocument, baseOptions);
      }
export type LoginMutationHookResult = ReturnType<typeof useLoginMutation>;
export type LoginMutationResult = Apollo.MutationResult<LoginMutation>;
export type LoginMutationOptions = Apollo.BaseMutationOptions<LoginMutation, LoginMutationVariables>;
export const LoginSmartphoneDocument = gql`
    mutation loginSmartphone($email: String!, $password: String!) {
  loginSmartphone(email: $email, password: $password) {
    access_token
    email
    nameKanji
    id
  }
}
    `;
export type LoginSmartphoneMutationFn = Apollo.MutationFunction<LoginSmartphoneMutation, LoginSmartphoneMutationVariables>;

/**
 * __useLoginSmartphoneMutation__
 *
 * To run a mutation, you first call `useLoginSmartphoneMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useLoginSmartphoneMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [loginSmartphoneMutation, { data, loading, error }] = useLoginSmartphoneMutation({
 *   variables: {
 *      email: // value for 'email'
 *      password: // value for 'password'
 *   },
 * });
 */
export function useLoginSmartphoneMutation(baseOptions?: Apollo.MutationHookOptions<LoginSmartphoneMutation, LoginSmartphoneMutationVariables>) {
        return Apollo.useMutation<LoginSmartphoneMutation, LoginSmartphoneMutationVariables>(LoginSmartphoneDocument, baseOptions);
      }
export type LoginSmartphoneMutationHookResult = ReturnType<typeof useLoginSmartphoneMutation>;
export type LoginSmartphoneMutationResult = Apollo.MutationResult<LoginSmartphoneMutation>;
export type LoginSmartphoneMutationOptions = Apollo.BaseMutationOptions<LoginSmartphoneMutation, LoginSmartphoneMutationVariables>;
export const TrackingEventAttendeesDocument = gql`
    mutation trackingEventAttendees($forwardFromId: Float, $forwardUser: CreateUserDTO, $tracking: EventAttendeesDTO!, $mailUserId: String!) {
  trackingEventAttendees(
    forwardFromId: $forwardFromId
    forwardUser: $forwardUser
    tracking: $tracking
    mailUserId: $mailUserId
  )
}
    `;
export type TrackingEventAttendeesMutationFn = Apollo.MutationFunction<TrackingEventAttendeesMutation, TrackingEventAttendeesMutationVariables>;

/**
 * __useTrackingEventAttendeesMutation__
 *
 * To run a mutation, you first call `useTrackingEventAttendeesMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useTrackingEventAttendeesMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [trackingEventAttendeesMutation, { data, loading, error }] = useTrackingEventAttendeesMutation({
 *   variables: {
 *      forwardFromId: // value for 'forwardFromId'
 *      forwardUser: // value for 'forwardUser'
 *      tracking: // value for 'tracking'
 *      mailUserId: // value for 'mailUserId'
 *   },
 * });
 */
export function useTrackingEventAttendeesMutation(baseOptions?: Apollo.MutationHookOptions<TrackingEventAttendeesMutation, TrackingEventAttendeesMutationVariables>) {
        return Apollo.useMutation<TrackingEventAttendeesMutation, TrackingEventAttendeesMutationVariables>(TrackingEventAttendeesDocument, baseOptions);
      }
export type TrackingEventAttendeesMutationHookResult = ReturnType<typeof useTrackingEventAttendeesMutation>;
export type TrackingEventAttendeesMutationResult = Apollo.MutationResult<TrackingEventAttendeesMutation>;
export type TrackingEventAttendeesMutationOptions = Apollo.BaseMutationOptions<TrackingEventAttendeesMutation, TrackingEventAttendeesMutationVariables>;
export const DeleteUsersDocument = gql`
    mutation deleteUsers($ids: [Int!]!) {
  deleteUsers(ids: $ids)
}
    `;
export type DeleteUsersMutationFn = Apollo.MutationFunction<DeleteUsersMutation, DeleteUsersMutationVariables>;

/**
 * __useDeleteUsersMutation__
 *
 * To run a mutation, you first call `useDeleteUsersMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteUsersMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteUsersMutation, { data, loading, error }] = useDeleteUsersMutation({
 *   variables: {
 *      ids: // value for 'ids'
 *   },
 * });
 */
export function useDeleteUsersMutation(baseOptions?: Apollo.MutationHookOptions<DeleteUsersMutation, DeleteUsersMutationVariables>) {
        return Apollo.useMutation<DeleteUsersMutation, DeleteUsersMutationVariables>(DeleteUsersDocument, baseOptions);
      }
export type DeleteUsersMutationHookResult = ReturnType<typeof useDeleteUsersMutation>;
export type DeleteUsersMutationResult = Apollo.MutationResult<DeleteUsersMutation>;
export type DeleteUsersMutationOptions = Apollo.BaseMutationOptions<DeleteUsersMutation, DeleteUsersMutationVariables>;
export const CreateUserDocument = gql`
    mutation createUser($user: CreateUserDTO!) {
  createUser(user: $user) {
    id
    email
  }
}
    `;
export type CreateUserMutationFn = Apollo.MutationFunction<CreateUserMutation, CreateUserMutationVariables>;

/**
 * __useCreateUserMutation__
 *
 * To run a mutation, you first call `useCreateUserMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateUserMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createUserMutation, { data, loading, error }] = useCreateUserMutation({
 *   variables: {
 *      user: // value for 'user'
 *   },
 * });
 */
export function useCreateUserMutation(baseOptions?: Apollo.MutationHookOptions<CreateUserMutation, CreateUserMutationVariables>) {
        return Apollo.useMutation<CreateUserMutation, CreateUserMutationVariables>(CreateUserDocument, baseOptions);
      }
export type CreateUserMutationHookResult = ReturnType<typeof useCreateUserMutation>;
export type CreateUserMutationResult = Apollo.MutationResult<CreateUserMutation>;
export type CreateUserMutationOptions = Apollo.BaseMutationOptions<CreateUserMutation, CreateUserMutationVariables>;
export const UpdateUserDocument = gql`
    mutation updateUser($user: UpdateUserDTO!, $id: Int!) {
  updateUser(user: $user, id: $id) {
    id
    companyId
    name
    userTypeId
    department
    jobTitle
    mobile
    email
    postCode
    address1
    address2
    fax
    role
  }
}
    `;
export type UpdateUserMutationFn = Apollo.MutationFunction<UpdateUserMutation, UpdateUserMutationVariables>;

/**
 * __useUpdateUserMutation__
 *
 * To run a mutation, you first call `useUpdateUserMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateUserMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateUserMutation, { data, loading, error }] = useUpdateUserMutation({
 *   variables: {
 *      user: // value for 'user'
 *      id: // value for 'id'
 *   },
 * });
 */
export function useUpdateUserMutation(baseOptions?: Apollo.MutationHookOptions<UpdateUserMutation, UpdateUserMutationVariables>) {
        return Apollo.useMutation<UpdateUserMutation, UpdateUserMutationVariables>(UpdateUserDocument, baseOptions);
      }
export type UpdateUserMutationHookResult = ReturnType<typeof useUpdateUserMutation>;
export type UpdateUserMutationResult = Apollo.MutationResult<UpdateUserMutation>;
export type UpdateUserMutationOptions = Apollo.BaseMutationOptions<UpdateUserMutation, UpdateUserMutationVariables>;
export const EventsDocument = gql`
    query events($limit: Int = 10, $offset: Int = 0, $searchText: String = "", $field: String = "createdAt", $direction: Direction = DESC) {
  events(
    limit: $limit
    offset: $offset
    searchText: $searchText
    orderBy: {field: $field, direction: $direction}
  ) {
    count
    items {
      id
      name
      description
      placeName
      placeAddress
      dateFrom
      dateTo
      timeFrom
      timeTo
      formAddress
      createdAt
      updatedAt
    }
  }
}
    `;

/**
 * __useEventsQuery__
 *
 * To run a query within a React component, call `useEventsQuery` and pass it any options that fit your needs.
 * When your component renders, `useEventsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useEventsQuery({
 *   variables: {
 *      limit: // value for 'limit'
 *      offset: // value for 'offset'
 *      searchText: // value for 'searchText'
 *      field: // value for 'field'
 *      direction: // value for 'direction'
 *   },
 * });
 */
export function useEventsQuery(baseOptions?: Apollo.QueryHookOptions<EventsQuery, EventsQueryVariables>) {
        return Apollo.useQuery<EventsQuery, EventsQueryVariables>(EventsDocument, baseOptions);
      }
export function useEventsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<EventsQuery, EventsQueryVariables>) {
          return Apollo.useLazyQuery<EventsQuery, EventsQueryVariables>(EventsDocument, baseOptions);
        }
export type EventsQueryHookResult = ReturnType<typeof useEventsQuery>;
export type EventsLazyQueryHookResult = ReturnType<typeof useEventsLazyQuery>;
export type EventsQueryResult = Apollo.QueryResult<EventsQuery, EventsQueryVariables>;
export const EventByMailUserDocument = gql`
    query eventByMailUser($mailUserId: String!) {
  eventByMailUser(mailUserId: $mailUserId) {
    user {
      id
      email
      name
    }
    event {
      name
      id
    }
  }
}
    `;

/**
 * __useEventByMailUserQuery__
 *
 * To run a query within a React component, call `useEventByMailUserQuery` and pass it any options that fit your needs.
 * When your component renders, `useEventByMailUserQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useEventByMailUserQuery({
 *   variables: {
 *      mailUserId: // value for 'mailUserId'
 *   },
 * });
 */
export function useEventByMailUserQuery(baseOptions: Apollo.QueryHookOptions<EventByMailUserQuery, EventByMailUserQueryVariables>) {
        return Apollo.useQuery<EventByMailUserQuery, EventByMailUserQueryVariables>(EventByMailUserDocument, baseOptions);
      }
export function useEventByMailUserLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<EventByMailUserQuery, EventByMailUserQueryVariables>) {
          return Apollo.useLazyQuery<EventByMailUserQuery, EventByMailUserQueryVariables>(EventByMailUserDocument, baseOptions);
        }
export type EventByMailUserQueryHookResult = ReturnType<typeof useEventByMailUserQuery>;
export type EventByMailUserLazyQueryHookResult = ReturnType<typeof useEventByMailUserLazyQuery>;
export type EventByMailUserQueryResult = Apollo.QueryResult<EventByMailUserQuery, EventByMailUserQueryVariables>;
export const EventDocument = gql`
    query event($id: String!) {
  event(id: $id) {
    id
    name
    description
    dateFrom
    dateTo
    timeFrom
    timeTo
    placeName
    placeAddress
    formAddress
    createdAt
    updatedAt
  }
}
    `;

/**
 * __useEventQuery__
 *
 * To run a query within a React component, call `useEventQuery` and pass it any options that fit your needs.
 * When your component renders, `useEventQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useEventQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useEventQuery(baseOptions: Apollo.QueryHookOptions<EventQuery, EventQueryVariables>) {
        return Apollo.useQuery<EventQuery, EventQueryVariables>(EventDocument, baseOptions);
      }
export function useEventLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<EventQuery, EventQueryVariables>) {
          return Apollo.useLazyQuery<EventQuery, EventQueryVariables>(EventDocument, baseOptions);
        }
export type EventQueryHookResult = ReturnType<typeof useEventQuery>;
export type EventLazyQueryHookResult = ReturnType<typeof useEventLazyQuery>;
export type EventQueryResult = Apollo.QueryResult<EventQuery, EventQueryVariables>;
export const StatisticEventUserDocument = gql`
    query statisticEventUser($id: String!) {
  statisticEventUser(id: $id) {
    attendCount
    submitFormCount
    total
  }
}
    `;

/**
 * __useStatisticEventUserQuery__
 *
 * To run a query within a React component, call `useStatisticEventUserQuery` and pass it any options that fit your needs.
 * When your component renders, `useStatisticEventUserQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useStatisticEventUserQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useStatisticEventUserQuery(baseOptions: Apollo.QueryHookOptions<StatisticEventUserQuery, StatisticEventUserQueryVariables>) {
        return Apollo.useQuery<StatisticEventUserQuery, StatisticEventUserQueryVariables>(StatisticEventUserDocument, baseOptions);
      }
export function useStatisticEventUserLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<StatisticEventUserQuery, StatisticEventUserQueryVariables>) {
          return Apollo.useLazyQuery<StatisticEventUserQuery, StatisticEventUserQueryVariables>(StatisticEventUserDocument, baseOptions);
        }
export type StatisticEventUserQueryHookResult = ReturnType<typeof useStatisticEventUserQuery>;
export type StatisticEventUserLazyQueryHookResult = ReturnType<typeof useStatisticEventUserLazyQuery>;
export type StatisticEventUserQueryResult = Apollo.QueryResult<StatisticEventUserQuery, StatisticEventUserQueryVariables>;
export const UsersDocument = gql`
    query users($limit: Int = 10, $offset: Int = 0, $searchText: String = "") {
  users(limit: $limit, offset: $offset, searchText: $searchText) {
    count
    items {
      id
      companyId
      name
      userTypeId
      department
      jobTitle
      mobile
      email
      postCode
      address1
      address2
      fax
      role
      company {
        id
        name
      }
    }
  }
}
    `;

/**
 * __useUsersQuery__
 *
 * To run a query within a React component, call `useUsersQuery` and pass it any options that fit your needs.
 * When your component renders, `useUsersQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useUsersQuery({
 *   variables: {
 *      limit: // value for 'limit'
 *      offset: // value for 'offset'
 *      searchText: // value for 'searchText'
 *   },
 * });
 */
export function useUsersQuery(baseOptions?: Apollo.QueryHookOptions<UsersQuery, UsersQueryVariables>) {
        return Apollo.useQuery<UsersQuery, UsersQueryVariables>(UsersDocument, baseOptions);
      }
export function useUsersLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<UsersQuery, UsersQueryVariables>) {
          return Apollo.useLazyQuery<UsersQuery, UsersQueryVariables>(UsersDocument, baseOptions);
        }
export type UsersQueryHookResult = ReturnType<typeof useUsersQuery>;
export type UsersLazyQueryHookResult = ReturnType<typeof useUsersLazyQuery>;
export type UsersQueryResult = Apollo.QueryResult<UsersQuery, UsersQueryVariables>;
export const UserDocument = gql`
    query user($id: Int!) {
  user(id: $id) {
    id
    companyId
    name
    userTypeId
    department
    jobTitle
    mobile
    email
    postCode
    address1
    address2
    fax
    role
    company {
      id
      name
    }
  }
}
    `;

/**
 * __useUserQuery__
 *
 * To run a query within a React component, call `useUserQuery` and pass it any options that fit your needs.
 * When your component renders, `useUserQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useUserQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useUserQuery(baseOptions: Apollo.QueryHookOptions<UserQuery, UserQueryVariables>) {
        return Apollo.useQuery<UserQuery, UserQueryVariables>(UserDocument, baseOptions);
      }
export function useUserLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<UserQuery, UserQueryVariables>) {
          return Apollo.useLazyQuery<UserQuery, UserQueryVariables>(UserDocument, baseOptions);
        }
export type UserQueryHookResult = ReturnType<typeof useUserQuery>;
export type UserLazyQueryHookResult = ReturnType<typeof useUserLazyQuery>;
export type UserQueryResult = Apollo.QueryResult<UserQuery, UserQueryVariables>;
export const Users_ByMailGroupDocument = gql`
    query users_ByMailGroup($mailGroupId: Int!) {
  usersByMailGroup(mailGroupId: $mailGroupId) {
    count
    items {
      id
      companyId
      name
      userTypeId
      department
      jobTitle
      mobile
      email
      postCode
      address1
      address2
      company {
        id
        name
      }
      fax
      role
    }
  }
}
    `;

/**
 * __useUsers_ByMailGroupQuery__
 *
 * To run a query within a React component, call `useUsers_ByMailGroupQuery` and pass it any options that fit your needs.
 * When your component renders, `useUsers_ByMailGroupQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useUsers_ByMailGroupQuery({
 *   variables: {
 *      mailGroupId: // value for 'mailGroupId'
 *   },
 * });
 */
export function useUsers_ByMailGroupQuery(baseOptions: Apollo.QueryHookOptions<Users_ByMailGroupQuery, Users_ByMailGroupQueryVariables>) {
        return Apollo.useQuery<Users_ByMailGroupQuery, Users_ByMailGroupQueryVariables>(Users_ByMailGroupDocument, baseOptions);
      }
export function useUsers_ByMailGroupLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<Users_ByMailGroupQuery, Users_ByMailGroupQueryVariables>) {
          return Apollo.useLazyQuery<Users_ByMailGroupQuery, Users_ByMailGroupQueryVariables>(Users_ByMailGroupDocument, baseOptions);
        }
export type Users_ByMailGroupQueryHookResult = ReturnType<typeof useUsers_ByMailGroupQuery>;
export type Users_ByMailGroupLazyQueryHookResult = ReturnType<typeof useUsers_ByMailGroupLazyQuery>;
export type Users_ByMailGroupQueryResult = Apollo.QueryResult<Users_ByMailGroupQuery, Users_ByMailGroupQueryVariables>;
export const Users_By_MailGroupDocument = gql`
    query users_By_MailGroup($mailGroupId: Int!, $limit: Int = 10, $offset: Int = 0, $searchText: String = "") {
  usersByMailGroup(
    mailGroupId: $mailGroupId
    limit: $limit
    offset: $offset
    searchText: $searchText
  ) {
    count
    items {
      id
      companyId
      name
      userTypeId
      department
      jobTitle
      mobile
      email
      postCode
      address1
      address2
      fax
      role
      company {
        id
        name
      }
    }
  }
}
    `;

/**
 * __useUsers_By_MailGroupQuery__
 *
 * To run a query within a React component, call `useUsers_By_MailGroupQuery` and pass it any options that fit your needs.
 * When your component renders, `useUsers_By_MailGroupQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useUsers_By_MailGroupQuery({
 *   variables: {
 *      mailGroupId: // value for 'mailGroupId'
 *      limit: // value for 'limit'
 *      offset: // value for 'offset'
 *      searchText: // value for 'searchText'
 *   },
 * });
 */
export function useUsers_By_MailGroupQuery(baseOptions: Apollo.QueryHookOptions<Users_By_MailGroupQuery, Users_By_MailGroupQueryVariables>) {
        return Apollo.useQuery<Users_By_MailGroupQuery, Users_By_MailGroupQueryVariables>(Users_By_MailGroupDocument, baseOptions);
      }
export function useUsers_By_MailGroupLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<Users_By_MailGroupQuery, Users_By_MailGroupQueryVariables>) {
          return Apollo.useLazyQuery<Users_By_MailGroupQuery, Users_By_MailGroupQueryVariables>(Users_By_MailGroupDocument, baseOptions);
        }
export type Users_By_MailGroupQueryHookResult = ReturnType<typeof useUsers_By_MailGroupQuery>;
export type Users_By_MailGroupLazyQueryHookResult = ReturnType<typeof useUsers_By_MailGroupLazyQuery>;
export type Users_By_MailGroupQueryResult = Apollo.QueryResult<Users_By_MailGroupQuery, Users_By_MailGroupQueryVariables>;