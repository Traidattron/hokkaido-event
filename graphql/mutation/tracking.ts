import { gql } from '@apollo/client';
//TRACKING_EVENT, TRACKING_EVENT_ATTENDEES

gql`
    mutation trackingEventAttendees($forwardFromId: Float, $forwardUser: CreateUserDTO, $tracking: EventAttendeesDTO!, $mailUserId: String!) {
        trackingEventAttendees(
            forwardFromId: $forwardFromId
            forwardUser: $forwardUser
            tracking: $tracking
            mailUserId: $mailUserId
        )
    }
`;
