import { gql } from '@apollo/client';
//DELETE_USERS, CREATE_USER, UPDATE_USER
gql`
    mutation deleteUsers($ids: [Int!]!) {
        deleteUsers(ids: $ids)
    }
`;
gql`
    mutation createUser($user: CreateUserDTO!) {
        createUser(user: $user) {
            id
            email
        }
    }
`;
gql`
    mutation updateUser($user: UpdateUserDTO!, $id: Int!) {
        updateUser(user: $user, id: $id) {
            id
            companyId
            name
            userTypeId
            department
            jobTitle
            mobile
            email
            postCode
            address1
            address2
            fax
            role
        }
    }
`;
