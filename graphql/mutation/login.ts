import { gql } from '@apollo/client';
//LOGIN_USER
gql`
    mutation login($email: String!, $password: String!) {
        login(email: $email, password: $password) {
            access_token
            name
            email
        }
    }
`;
gql`
    mutation loginSmartphone($email: String!, $password: String!) {
        loginSmartphone(email: $email, password: $password) {
            access_token
            email
            nameKanji
            id
        }
    }
`;
