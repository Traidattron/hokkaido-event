import { gql } from '@apollo/client';
//DELETE_EVENTS, CREATE_EVENT, FORWARD_EVENT, UPDATE_EVENT
gql`
    mutation deleteEvents($ids: [String!]!) {
        deleteEvents(ids: $ids)
    }
`;
gql`
    mutation createEvent($event: CreateEventDTO!) {
        createEvent(event: $event) {
            id
            name
        }
    }
`;
gql`
    mutation forwardMail($mailTo: String!, $mailUserId: String!) {
        forwardMail(mailTo: $mailTo, mailUserId: $mailUserId)
    }
`;
gql`
    mutation updateEvent($event: UpdateEventDTO!, $id: String!) {
        updateEvent(event: $event, id: $id) {
            id
            name
            description
            placeName
            placeAddress
            dateFrom
            dateTo
            timeFrom
            timeTo
            formAddress
            staffId
            staff {
                id
                name
            }
        }
    }
`;
gql`
    mutation createMessage($createMessageInput: CreateMessageDTO!) {
        createMessage(createMessageInput: $createMessageInput) {
            id
            eventId
            message
            type
        }
    }
`;
export const UPLOAD_FILE = gql`
    mutation uploadFile($file: Upload!) {
        uploadFile(file: $file)
    }
`;
