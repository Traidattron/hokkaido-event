import { gql } from '@apollo/client';

gql`
    query events(
        $limit: Int = 10
        $offset: Int = 0
        $searchText: String = ""
        $field: String = "createdAt"
        $direction: Direction = DESC
    ) {
        events(
            limit: $limit
            offset: $offset
            searchText: $searchText
            orderBy: { field: $field, direction: $direction }
        ) {
            count
            items {
                id
                name
                description
                placeName
                placeAddress
                dateFrom
                dateTo
                timeFrom
                timeTo
                formAddress
                createdAt
                updatedAt
            }
        }
    }
`;
gql`
    query eventByMailUser($mailUserId: String!) {
        eventByMailUser(mailUserId: $mailUserId) {
            user {
                id
                email
                name
            }
            event {
                name
                id
            }
        }
    }
`;
gql`
    query event($id: String!) {
        event(id: $id) {
            id
            name
            description
            dateFrom
            dateTo
            timeFrom
            timeTo
            placeName
            placeAddress
            formAddress
            createdAt
            updatedAt
        }
    }
`;
// gql`
//     query statisticEventUser($id: Int!) {
//         statisticEventUser(id: $id) {
//             attendCount
//             total
//         }
//     }
// `;
gql`
    query statisticEventUser($id: String!) {
        statisticEventUser(id: $id) {
            attendCount
            submitFormCount
            total
        }
    }
`;
