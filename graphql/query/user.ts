import { gql } from '@apollo/client';

gql`
    query users($limit: Int = 10, $offset: Int = 0, $searchText: String = "") {
        users(limit: $limit, offset: $offset, searchText: $searchText) {
            count
            items {
                id
                companyId
                name
                userTypeId
                department
                jobTitle
                mobile
                email
                postCode
                address1
                address2
                fax
                role
                company {
                    id
                    name
                }
            }
        }
    }
`;
gql`
    query user($id: Int!) {
        user(id: $id) {
            id
            companyId
            name
            userTypeId
            department
            jobTitle
            mobile
            email
            postCode
            address1
            address2
            fax
            role
            company {
                id
                name
            }
        }
    }
`;
gql`
    query users_ByMailGroup($mailGroupId: Int!) {
        usersByMailGroup(mailGroupId: $mailGroupId) {
            count
            items {
                id
                companyId
                name
                userTypeId
                department
                jobTitle
                mobile
                email
                postCode
                address1
                address2
                company {
                    id
                    name
                }
                fax
                role
            }
        }
    }
`;
gql`
    query users_By_MailGroup($mailGroupId: Int!, $limit: Int = 10, $offset: Int = 0, $searchText: String = "") {
        usersByMailGroup(mailGroupId: $mailGroupId, limit: $limit, offset: $offset, searchText: $searchText) {
            count
            items {
                id
                companyId
                name
                userTypeId
                department
                jobTitle
                mobile
                email
                postCode
                address1
                address2
                fax
                role
                company {
                    id
                    name
                }
            }
        }
    }
`;
// gql`
//     query usersByMail($id: Float!){
//         usersByMail(id: $id){
//         id
//         email
//         }
//     }
// `;
