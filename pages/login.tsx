import React, { useRef, useEffect } from 'react';
import { _word } from 'data/language';
import { LoginWrapper, FormWrapper, ContentLogin, Logo, NameProject, DivImage } from 'style/pages.style';
import { Form, Input, Button } from 'antd';
import { useLoginSmartphoneMutation } from 'graphql/gen-types';
import { useRouter } from 'next/router';
import { useAuth } from 'hooks/useAuth';
import { LoginOutlined } from '@ant-design/icons';
import { openNotification } from 'components/Notification/Notification';
import Image from 'next/image';
import Head from 'next/head';
import axios from 'axios';
import Cookies from 'universal-cookie';

const layout = {
    labelCol: {
        span: 8,
    },
    wrapperCol: {
        span: 16,
    },
};

const Login = () => {
    const cookie = new Cookies();
    const router = useRouter();
    // const handleSubmit = (event) => {
    //     event.preventDefault();
    //     firebase
    //         .auth()
    //         .createUserWithEmailAndPassword('vu261194@gmail.com', 'Thanhvu@94')
    //         .then((user) => {})
    //         .catch((error) => {});
    // };
    const { SIGNIN_SUCCESS } = useAuth();
    const [loginMutation, { loading }] = useLoginSmartphoneMutation();
    const [form] = Form.useForm();
    const inputRef = useRef<any>(null);
    const loginSuccess = async (value: any) => {
        // const result = await axios.get('http://127.0.0.1:8000/login');
        // console.log('result', result);

        // console.log('result post: ', result);
        const { email, password } = value;
        loginMutation({
            variables: {
                email,
                password,
            },
        })
            .then(({ data }) => {
                SIGNIN_SUCCESS(
                    data?.loginSmartphone.access_token,
                    data?.loginSmartphone.nameKanji,
                    data?.loginSmartphone.email,
                );
                router.push('/events');
                openNotification('success', _word.signinSucess);
                console.log('test id', data?.loginSmartphone.id);
                axios({
                    method: 'post',
                    url: process.env.NEXT_PUBLIC_URL,
                    data: {
                        username: data?.loginSmartphone.nameKanji,
                        email: data?.loginSmartphone.email,
                        id: data?.loginSmartphone.id,
                    },
                }).then((response) => {
                    console.log('after login', response);
                    cookie.set('sessionid', response.data, {
                        path: process.env.NEXT_PUBLIC_PATH,
                        domain: process.env.NEXT_PUBLIC_SUBDOMAIN,
                    });
                });
            })
            .catch((errors) => {
                openNotification('error', errors.message);
            });
    };
    useEffect(() => {
        inputRef.current.focus({
            cursor: 'end',
        });
    }, []);

    return (
        <>
            <Head>
                <title>{_word.logoLogin}</title>
            </Head>
            {/* {!loading && <BirdsPage />} */}
            <LoginWrapper>
                <Logo>{_word.logo}</Logo>
                <ContentLogin>
                    <NameProject>
                        <DivImage>
                            <Image src='/logo-hokkaido.png' alt='Email' width={100} height={100}></Image>
                        </DivImage>
                    </NameProject>
                    <FormWrapper>
                        <Form
                            className='form-login'
                            layout={'vertical'}
                            form={form}
                            initialValues={{
                                layout: layout,
                            }}
                            onFinish={loginSuccess}
                        >
                            <Form.Item
                                label='ID'
                                name='email'
                                rules={[
                                    {
                                        required: true,
                                        message: _word.ruleRequired,
                                    },
                                ]}
                            >
                                <Input placeholder={_word.placeholderID} ref={inputRef} />
                            </Form.Item>
                            <Form.Item
                                className='input-password'
                                label={_word.password}
                                name='password'
                                rules={[
                                    {
                                        required: true,
                                        message: _word.ruleRequired,
                                    },
                                ]}
                            >
                                <Input.Password placeholder={_word.placeholderPassword} />
                            </Form.Item>
                            <Form.Item noStyle>
                                <Button
                                    icon={<LoginOutlined />}
                                    style={{ borderRadius: 20, fontWeight: 700 }}
                                    type='primary'
                                    htmlType='submit'
                                    // onClick={handleSubmit}
                                    loading={loading}
                                >
                                    {_word.signIn}
                                </Button>
                            </Form.Item>
                        </Form>
                    </FormWrapper>
                </ContentLogin>
            </LoginWrapper>
        </>
    );
};
export default Login;
