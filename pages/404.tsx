import { FrownFilled } from "@ant-design/icons";
import { useRouter } from "next/router";
import React from "react";
import { PageNotFound } from "style/pages.style";

// custom 404 Page, any not found route will be redirect here
const Custom404: React.FC = () => {
  const router = useRouter();
  return (
    <PageNotFound>
      <section>
        <h1>404</h1>
        <p>ページが見つかりません。<FrownFilled onClick={() => router.push("/")} /></p>
      </section>
    </PageNotFound>
  );
};
export default Custom404;
