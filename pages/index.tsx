import React from 'react';
import Head from 'next/head';
import { useRouter } from 'next/router';
import Cookies from 'universal-cookie';

export default function Home() {
    const router = useRouter();
    const cookie = new Cookies();
    const token = cookie.get('access_token');
    React.useEffect(() => {
        if (token) {
            router.replace('/events');
        } else {
            router.replace('/login');
        }
    });
    return (
        <Head>
            <meta name='robots' content='noindex, nofollow' />
        </Head>
    );
}
