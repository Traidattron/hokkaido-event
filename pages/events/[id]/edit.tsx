import { useRouter } from 'next/router';

export default function EditEventPage() {
    const router = useRouter();
    return <div>event edit page: {JSON.stringify(router.query)}</div>;
}
