import React, { useEffect, useState } from 'react';
import { _word } from 'data/language';
import { EventWrapper, NameEvent, TextEvent, SubmitEvent, ULEvent, LinkText } from 'style/pages.style';
import { Button, Checkbox, Row, Select } from 'antd';
import { useRouter } from 'next/router';
import { openNotification } from 'components/Notification/Notification';
import { fullFormatJP } from 'constants/format';
import { SendOutlined } from '@ant-design/icons';
import moment from 'moment';
import {
    useEventByMailUserQuery,
    useEventQuery,
    useForwardMailMutation,
    useTrackingEventAttendeesMutation,
} from 'graphql/gen-types';
import Head from 'next/head';
const options = [
    { label: _word.livestreamFlag, value: 'livestreamFlag' },
    { label: _word.attendFlag, value: 'attendFlag' },
    { label: _word.postOfficeFlag, value: 'postOfficeFlag' },
];
const EventPage = () => {
    const router = useRouter();
    const [inputEmails, setInputEmails] = useState(false);
    const [listCheck, setListCheck] = useState<any>([]);
    const [listEmailForward, setListEmailForward] = useState<any>([]);
    const id = router.query.id;
    const mailUserId = router.query.mail_user_id?.toString();
    function onChange(checkedValues) {
        setListCheck(checkedValues);
    }
    const [trackingEvent] = useTrackingEventAttendeesMutation();
    const [forwardEvent, { loading: loadingForward }] = useForwardMailMutation();
    const { data } = useEventQuery({
        variables: {
            id: id as string,
        },
        onCompleted: (data) => {
            // countdown(data.event.registerTo);
        },
    });
    const { data: dataEventByMailUserId } = useEventByMailUserQuery({
        variables: {
            mailUserId: router.query.mail_user_id as string,
        },
    });
    const [days, setDays] = useState(Number);
    const [hours, setHours] = useState(Number);
    const [minutes, setMinutes] = useState(Number);
    const [seconds, setSeconds] = useState(Number);
    function countdown(date) {
        const second = 1000,
            minute = second * 60,
            hour = minute * 60,
            day = hour * 24;
        let registerTo = date,
            countDown = new Date(registerTo).getTime(),
            x = setInterval(function () {
                let now = new Date().getTime(),
                    distance = countDown - now;
                setDays(Math.floor(distance / day)),
                    setHours(Math.floor((distance % day) / hour)),
                    setMinutes(Math.floor((distance % hour) / minute)),
                    setSeconds(Math.floor((distance % minute) / second));
                if (distance < 0) {
                    clearInterval(x);
                }
            }, 0);
    }
    useEffect(() => {
        if (mailUserId) {
            trackingEvent({
                variables: {
                    mailUserId: mailUserId,
                    tracking: {
                        viewedLink: true,
                    },
                },
            });
        }
    }, [mailUserId]);
    const forwardMailAPI = () => {
        if (listEmailForward.length === 0) {
            openNotification('warning', _word.ruleCheckbox);
            return;
        }
        forwardEvent({
            variables: {
                mailUserId,
                mailTo: listEmailForward.join(','),
            },
        })
            .then(() => {
                openNotification('success', _word.send);
            })
            .catch((err) => {
                openNotification('error', err.message);
            });
    };
    const submit = () => {
        if (listCheck.length === 0) {
            openNotification('warning', _word.ruleCheckbox);
            return;
        }
        trackingEvent({
            variables: {
                mailUserId,
                tracking: {
                    viewedLink: true,
                    attendFlag: listCheck.indexOf('attendFlag') !== -1 ? true : false,
                    livestreamFlag: listCheck.indexOf('livestreamFlag') !== -1 ? true : false,
                    postOfficeFlag: listCheck.indexOf('postOfficeFlag') !== -1 ? true : false,
                },
            },
        })
            .then(() => {
                openNotification('success', _word.joinSuccess);
            })
            .catch((err) => {
                openNotification('error', err.message);
            });
    };
    console.log(dataEventByMailUserId);

    if (!data) return <></>;
    return (
        <>
            <Head>
                <title>{data.event.name}</title>
            </Head>
            <EventWrapper style={{ flexDirection: 'column' }}>
                <div className='event-info-content'>
                    {days < 0 && hours < 0 && minutes < 0 && seconds < 0 ? (
                        <NameEvent>{_word.eventOver}</NameEvent>
                    ) : (
                        <ULEvent>
                            <li>
                                <span>{days}</span>
                                <span className='text-time'>{_word.days}</span>
                            </li>
                            <li>
                                <span>{hours}</span>
                                <span className='text-time'>{_word.hours}</span>
                            </li>
                            <li>
                                <span>{minutes}</span>
                                <span className='text-time'>{_word.minutes}</span>
                            </li>
                            <li>
                                <span>{seconds}</span>
                                <span className='text-time'>{_word.seconds}</span>
                            </li>
                        </ULEvent>
                    )}
                    <NameEvent>{data?.event.name}</NameEvent>
                    <TextEvent>
                        {/* {_word.organizationDate}: {moment(data?.event?.registerTo).format(fullFormatJP)} */}
                    </TextEvent>
                    <TextEvent>{data?.event.description}</TextEvent>
                    {days >= 0 && hours >= 0 && minutes >= 0 && seconds >= 0 && (
                        <>
                            <TextEvent>
                                {dataEventByMailUserId?.eventByMailUser.user.name}様,{_word.inviteEvent}
                            </TextEvent>
                            <Checkbox.Group options={options} onChange={onChange} style={{ marginBottom: 20 }} />
                            {!inputEmails && (
                                <LinkText onClick={() => setInputEmails(true)}>{_word.textIntroduce}</LinkText>
                            )}
                            {inputEmails && (
                                <Row style={{ width: '100%', justifyContent: 'center' }}>
                                    <Select
                                        autoFocus
                                        onChange={(values) => setListEmailForward(values)}
                                        className='input-mails'
                                        placeholder={_word.placeholderIntroduce}
                                        mode='tags'
                                        tokenSeparators={[',', ';']}
                                        maxTagCount='responsive'
                                    />
                                    <Button
                                        type='default'
                                        icon={<SendOutlined />}
                                        onClick={forwardMailAPI}
                                        loading={loadingForward}
                                    />
                                </Row>
                            )}
                            <SubmitEvent onClick={submit}>
                                <span>{_word.send}</span>
                            </SubmitEvent>
                        </>
                    )}
                </div>
            </EventWrapper>
        </>
    );
};
export default EventPage;
