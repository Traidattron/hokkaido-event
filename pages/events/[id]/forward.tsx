import React, { useEffect, useState } from 'react';
import { _word } from 'data/language';
import { EventWrapper, NameEvent, TextEvent, SubmitEvent, ULEvent, EventInfo, EventForm } from 'style/pages.style';
import { useRouter } from 'next/router';
import { fullFormatJP } from 'constants/format';
import moment from 'moment';
import ForwardForm from 'containers/UserForm/ForwardForm';
import { useEventQuery, useUserQuery } from 'graphql/gen-types';
import Head from 'next/head';

const EventPage = () => {
    const router = useRouter();
    const [initialUser, setInitialUser] = useState<any>({});
    const id = router.query.id;
    const idUser = router.query.forwardFromId;
    const mailUserId = router.query.mail_user_id;

    const { data: dataUser } = useUserQuery({
        variables: {
            id: +idUser,
        },
        onCompleted: (data) => {
            setInitialUser(data.user);
        },
    });
    const { data } = useEventQuery({
        variables: {
            id: id as string,
        },
        // onCompleted: (data) => {
        //     countdown(data.event.registerTo);
        // },
    });
    const [days, setDays] = useState(Number);
    const [hours, setHours] = useState(Number);
    const [minutes, setMinutes] = useState(Number);
    const [seconds, setSeconds] = useState(Number);
    //can use to Countdown/Statistic ANTD
    function countdown(date) {
        const second = 1000,
            minute = second * 60,
            hour = minute * 60,
            day = hour * 24;
        let registerTo = date,
            countDown = new Date(registerTo).getTime(),
            x = setInterval(function () {
                let now = new Date().getTime(),
                    distance = countDown - now;
                setDays(Math.floor(distance / day)),
                    setHours(Math.floor((distance % day) / hour)),
                    setMinutes(Math.floor((distance % hour) / minute)),
                    setSeconds(Math.floor((distance % minute) / second));
                if (distance < 0) {
                    clearInterval(x);
                }
            }, 0);
    }

    if (!data) return <></>;
    return (
        <>
            <Head>
                <title>{data.event.name}</title>
            </Head>
            <EventWrapper style={{ flexDirection: 'row' }}>
                <EventInfo>
                    <div className='event-info-content'>
                        {days < 0 && hours < 0 && minutes < 0 && seconds < 0 ? (
                            <NameEvent>{_word.eventOver}</NameEvent>
                        ) : (
                            <ULEvent>
                                <li>
                                    <span>{days}</span>
                                    <span className='text-time'>{_word.days}</span>
                                </li>
                                <li>
                                    <span>{hours}</span>
                                    <span className='text-time'>{_word.hours}</span>
                                </li>
                                <li>
                                    <span>{minutes}</span>
                                    <span className='text-time'>{_word.minutes}</span>
                                </li>
                                <li>
                                    <span>{seconds}</span>
                                    <span className='text-time'>{_word.seconds}</span>
                                </li>
                            </ULEvent>
                        )}
                        <NameEvent>{data.event.name}</NameEvent>
                        <TextEvent>
                            {/* {_word.organizationDate}: {moment(data?.event?.registerTo).format(fullFormatJP)} */}
                        </TextEvent>
                        <TextEvent>{data.event.description}</TextEvent>
                        {days > 0 && hours > 0 && minutes > 0 && seconds >= 0 && (
                            <>
                                <TextEvent>
                                    {dataUser?.user.name}
                                    {_word.textForwardEvent}
                                </TextEvent>
                                <TextEvent>{_word.placeholderFormForward}</TextEvent>
                            </>
                        )}
                    </div>
                </EventInfo>
                {/* <EventForm>{dataUser && <ForwardForm initialValues={initialUser}></ForwardForm>}</EventForm> */}
                <EventForm>
                    <ForwardForm initialValues={dataUser && initialUser}></ForwardForm>
                </EventForm>
            </EventWrapper>
        </>
    );
};
export default EventPage;
