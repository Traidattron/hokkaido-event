import { Table, Pagination, Select, Space } from 'antd';
import { useHasMounted } from 'hooks/useHasMounted';
import { openNotification, typeNotification } from 'components/Notification/Notification';
import { columnsEvent } from 'data/columns';
import { _word } from 'data/language';
import React, { useState, useEffect } from 'react';
import useApp from 'hooks/useApp';
import EventForm from 'containers/EventForm/EventForm';
import moment from 'moment';
import { timeFormat } from 'constants/format';
import { SortByWrapper, TitleTableWrapper } from 'containers/AppLayout/AppLayout.style';
import { usePagination } from 'hooks/usePagination';
import ActionSession from 'components/ActionSession/ActionSession';
import { show_delete_modal } from 'hooks/useDeleteModal';
import { useDebounce } from 'use-debounce';
import { EventsDocument, useEventsQuery, useDeleteEventsMutation } from 'graphql/gen-types';
import { useRouter } from 'next/router';
import Head from 'next/head';
import Image from 'next/image';
const { Option } = Select;

export default function EventsPage() {
    const hasMounted = useHasMounted();
    const router = useRouter();
    const { query } = router;
    const { searchText, SHOW_PRIMARY_DRAWER } = useApp();
    const [text] = useDebounce(searchText, 1000);
    const [selectedRows, setSelectedRows] = useState<any[]>([]);
    const { data, loading: queryLoading, refetch, error } = useEventsQuery({
        variables: {
            limit: 1,
            offset: 0,
            searchText: undefined,
            field: 'createdAt',
            direction: 'DESC' as any,
        },
    });
    const { limit, page, handlePagination } = usePagination(refetch);

    const [deleteEvents, { loading: deleteLoading }] = useDeleteEventsMutation({
        onCompleted: () => {
            setSelectedRows([]);
            openNotification(typeNotification.success, _word.deleteCompanySuccess);
        },
        refetchQueries: [
            {
                query: EventsDocument,
                variables: {
                    limit: limit,
                    offset: (page - 1) * limit,
                },
            },
        ],
        onError: (error) => {
            console.log(error);
            openNotification(typeNotification.error, error.message);
        },
    });

    const rowSelection = {
        onChange: (selectedRowKeys, selectedRows) => {
            console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
            setSelectedRows(selectedRows);
        },
        selectedRowKeys: selectedRows.map((selectedRow) => selectedRow.id),
    };
    function formatdate(date: Date) {
        return moment(date);
    }
    const onMutationSuccess = (mutationType) => {
        if (mutationType === 'create') {
            setSelectedRows([]);
            handlePagination(1, limit);
        } else if (mutationType === 'update') {
            setSelectedRows([]);
        }
    };
    const showCreateEventForm = () => {
        SHOW_PRIMARY_DRAWER({
            title: _word.addEvent,
            child: <EventForm initialValues={{}} mutationType='create' onMutationSuccess={onMutationSuccess} />,
        });
    };

    const showUpdateEventForm = () => {
        let selectedRow = selectedRows[0];
        SHOW_PRIMARY_DRAWER({
            title: _word.editEvent,
            child: (
                <EventForm
                    mutationType='update'
                    onMutationSuccess={onMutationSuccess}
                    initialValues={{
                        id: selectedRow.id,
                        name: selectedRow.name,
                        description: selectedRow.description,
                        date: [formatdate(selectedRow.dateFrom), formatdate(selectedRow.dateTo)],
                        time: [moment(selectedRow.timeFrom, timeFormat), moment(selectedRow.timeTo, timeFormat)],
                        placeName: selectedRow.placeName,
                        placeAddress: selectedRow.placeAddress,
                        formAddress: selectedRow.formAddress,
                    }}
                />
            ),
        });
    };
    function handleChangeField(value) {
        console.log(`selected ${value}`);
        refetch({
            field: value,
        });
    }
    function handleChangeDirection(value) {
        console.log(`selected ${value}`);
        refetch({
            direction: value,
        });
    }
    const showDeleteEventForm = () => {
        show_delete_modal({
            selectedRows: selectedRows,
            onOk: () => {
                deleteEvents({
                    variables: {
                        ids: selectedRows.map((selectedRow) => selectedRow.id),
                    },
                });
            },
        });
    };
    useEffect(() => {
        if (text !== '') {
            refetch({
                searchText: text,
            });
        }
    }, [text]);
    if (!hasMounted) {
        return null;
    }
    return (
        <>
            <Head>
                <title>{_word.logo}</title>
            </Head>
            <Table
                className='table-event'
                rowKey='id'
                title={() => (
                    <TitleTableWrapper>
                        <div style={{ display: 'flex', alignItems: 'center' }}>
                            <ActionSession
                                disableEdit={selectedRows.length !== 1}
                                disableDelete={selectedRows.length === 0}
                                onAdd={showCreateEventForm}
                                onEdit={showUpdateEventForm}
                                onDelete={showDeleteEventForm}
                            />
                            <SortByWrapper>
                                <p>{_word.sortBy}: </p>
                                <Select
                                    defaultValue='createdAt'
                                    style={{ width: 200, marginRight: 10 }}
                                    onChange={handleChangeField}
                                >
                                    <Option value='name'>{_word.name}</Option>
                                    <Option value='dateFrom'>{_word.dateFrom}</Option>
                                    <Option value='dateTo'>{_word.dateTo}</Option>
                                    <Option value='placeName'>{_word.placeName}</Option>
                                    <Option value='createdAt'>{_word.createdAt}</Option>
                                    <Option value='updatedAt'>{_word.updatedAt}</Option>
                                </Select>
                                <Select
                                    defaultValue='DESC'
                                    style={{ width: 200, marginRight: 10 }}
                                    onChange={handleChangeDirection}
                                >
                                    <Option value='ASC'>
                                        <Space>
                                            <Image src='/sort-a-to-z.svg' alt='Logo' width={15} height={15} />
                                            {_word.ascending}
                                        </Space>
                                    </Option>
                                    <Option value='DESC'>
                                        <Space>
                                            <Image src='/sort-z-to-a.svg' alt='Logo' width={15} height={15} />
                                            {_word.descending}
                                        </Space>
                                    </Option>
                                </Select>
                            </SortByWrapper>
                        </div>
                        <Pagination
                            current={page}
                            size='small'
                            pageSizeOptions={['15', '20', '30', '50']}
                            pageSize={limit}
                            onChange={(page, limit) => {
                                setSelectedRows([]);
                                handlePagination(page, limit);
                            }}
                            showLessItems={true}
                            total={data?.events.count}
                        />
                    </TitleTableWrapper>
                )}
                rowSelection={{ ...rowSelection }}
                // expandable={{
                //     expandedRowRender: (record, index, indent, expanded) => {
                //         return <p style={{ margin: 0 }}>{record.description}</p>
                //     },
                //     expandIcon: ({ expanded, onExpand, record }) =>
                //         expanded ? (
                //             <MinusCircleOutlined onClick={e => onExpand(record, e)} />
                //         ) : (
                //                 <PlusCircleOutlined onClick={e => onExpand(record, e)} />
                //             ),
                //     expandRowByClick: true
                // }}
                onRow={(record, _rowIndex) => {
                    return {
                        onClick: (_event) => {
                            router.replace({ query: { ...query, eventID: record.id } });
                        },
                    };
                }}
                columns={columnsEvent as any}
                dataSource={data?.events.items}
                loading={!data && !error}
                scroll={{
                    x: 2200,
                    y: 'calc(100vh - 211px)',
                }}
                pagination={false}
            />
        </>
    );
}
