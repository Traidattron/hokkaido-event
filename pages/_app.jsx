import AppLayout from 'containers/AppLayout/AppLayout';
import { ApolloClient, InMemoryCache, ApolloProvider, createHttpLink } from '@apollo/client';
import { onError } from '@apollo/client/link/error';
import { setContext } from '@apollo/client/link/context';
import { isBrowser } from 'helper/isBrowser';
import { AuthProvider } from 'contexts/AuthContext';
import { AppProvider } from 'contexts/AppContext';
import Router, { useRouter } from 'next/router';
import { GlobalStyle } from 'style/global.style';
import 'assets/antd-custom.less'; // includes antd style and our customization
import { ConfigProvider } from 'antd';
import ja_JP from 'antd/lib/locale/ja_JP';
import { ROUTER } from 'constants/navigation';
import Cookies from 'universal-cookie';
import Head from 'next/head';
import { _word } from 'data/language';
import NProgress from 'nprogress';
import { createUploadLink } from 'apollo-upload-client';

const cookie = new Cookies();
const httpLink = createHttpLink({
    uri: process.env.NEXT_PUBLIC_API_URI,
});
const linkUpload = createUploadLink({
    uri: process.env.NEXT_PUBLIC_API_URI,
});
Router.events.on('routeChangeStart', () => NProgress.start());
Router.events.on('routeChangeComplete', () => NProgress.done());
Router.events.on('routeChangeError', () => NProgress.done());

const errorLink = onError(({ graphQLErrors, networkError }) => {
    if (graphQLErrors)
        graphQLErrors.map(({ message, locations, path, extensions }) => {
            console.log(`[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`);
            if (extensions?.exception?.response?.statusCode === 401) {
                // Router.replace(ROUTER.LOGIN);
            }
        });

    if (networkError) console.log(`[Network error]: ${networkError}`);
});
const authLink = setContext((_, { headers }) => {
    const token = isBrowser && cookie.get('access_token');
    return {
        headers: {
            ...headers,
            authorization: token ? `Bearer ${token}` : '',
        },
    };
});

const client = new ApolloClient({
    link: authLink.concat(errorLink).concat(linkUpload),
    // link: authLink.concat(errorLink).concat(httpLink),
    cache: new InMemoryCache(),
    defaultOptions: {
        watchQuery: {
            fetchPolicy: 'cache-and-network',
        },
    },
});
const withoutLayout = [
    ROUTER.LOGIN,
    ROUTER.EVENT_REGISTRATION,
    ROUTER.EVENT_FORWARD,
    ROUTER.ROOT,
    ROUTER.PAGE_NOT_FOUND,
    ROUTER.HOKKAIDO_EVENT,
];
function App({ Component, pageProps }) {
    const router = useRouter();
    const { pathname } = router;
    return (
        <>
            <ApolloProvider client={client}>
                <AuthProvider>
                    <AppProvider>
                        <ConfigProvider locale={process.env.NEXT_PUBLIC_LANGUAGE === 'en' ? undefined : ja_JP}>
                            {withoutLayout.includes(pathname) ? (
                                <Component {...pageProps} />
                            ) : (
                                <>
                                    <Head>
                                        <title>{_word.logoFullName}</title>
                                        {/* Import CSS for nprogress */}
                                        <link rel='stylesheet' type='text/css' href='/nprogress.css' />
                                    </Head>
                                    <AppLayout>
                                        <Component {...pageProps} />
                                    </AppLayout>
                                    <GlobalStyle />
                                </>
                            )}
                        </ConfigProvider>
                    </AppProvider>
                </AuthProvider>
            </ApolloProvider>
        </>
    );
}
export default App;
