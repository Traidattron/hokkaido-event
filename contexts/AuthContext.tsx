import React, { createContext } from 'react';
import Cookies from 'universal-cookie';

const AuthContext = createContext([{}, () => {}]);

const AuthProvider = (props: any) => {
    const cookie = new Cookies();
    const [state, setState] = React.useState({
        isAuthenticated: !!cookie.get('access_token'),
    });

    return <AuthContext.Provider value={[state, setState]}>{props.children}</AuthContext.Provider>;
};

export { AuthContext, AuthProvider };
