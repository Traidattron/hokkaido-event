import React, { createContext, ReactElement } from 'react';

type contextType = {
    primaryDrawer: {
        display: boolean;
        child: ReactElement;
        title: string;
    };
    secondaryDrawer: {
        display: boolean;
        child: ReactElement;
        title: string;
    };
    mailBox: {
        type: 'editMail' | 'createSubMail' | 'editSubMail' | 'createMail' | 'none';
        id: string;
        mainMailTitle: string;
    };
    searchText: string | undefined;
};
const AppContext = createContext<any>([{}, () => {}]);

const AppProvider = (props) => {
    const [state, setState] = React.useState<contextType>({
        primaryDrawer: {
            display: false,
            child: <div />,
            title: '',
        },
        secondaryDrawer: {
            display: false,
            child: <div />,
            title: '',
        },
        mailBox: {
            type: 'none',
            id: '',
            mainMailTitle: '',
        },
        searchText: '',
    });
    return <AppContext.Provider value={[state, setState]}>{props.children}</AppContext.Provider>;
};

export { AppContext, AppProvider };
