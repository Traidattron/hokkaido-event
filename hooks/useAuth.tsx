import { useContext } from 'react';
import { AuthContext } from 'contexts/AuthContext';
import Cookies from 'universal-cookie';

const useAuth = () => {
    const [state, setState] = useContext<any>(AuthContext);
    const cookie = new Cookies();
    const ecodePass = (pass) => {
        return pass;
    };
    function SIGNIN_SUCCESS(access_token, name, email) {
        setState((state) => ({ ...state, isAuthenticated: true }));
        // localStorage.setItem('access_token', access_token);
        cookie.set('access_token', access_token);
        cookie.set('user_name', name, {
            path: process.env.NEXT_PUBLIC_PATH,
            domain: process.env.NEXT_PUBLIC_SUBDOMAIN,
        });
        cookie.set('user_email', email);
    }

    function SIGNOUT() {
        setState((state) => ({ ...state, isAuthenticated: false }));
        // localStorage.removeItem('access_token');
        cookie.remove('access_token');
        cookie.remove('user_name', { path: process.env.NEXT_PUBLIC_PATH, domain: process.env.NEXT_PUBLIC_SUBDOMAIN });
        cookie.remove('user_email');
        cookie.remove('sessionid', { path: process.env.NEXT_PUBLIC_PATH, domain: process.env.NEXT_PUBLIC_SUBDOMAIN });
    }
    return {
        SIGNIN_SUCCESS,
        SIGNOUT,
        isAuthenticated: state.isAuthenticated,
    };
};

export { useAuth };
