import { useUsers_By_MailGroupQuery } from 'graphql/gen-types';
import React from 'react';

export async function useGetUsersByMailGroup(currentGroupMailId) {
    useUsers_By_MailGroupQuery({
        variables: {
            mailGroupId: currentGroupMailId,
        },
        onCompleted: (dataUserList) => {
            // console.log('2');
            console.log('oncomplete', dataUserList);
            // getUserByGroupMail({ variables: { mailGroupId: 502 } });
        },
        onError: () => console.log('err'),
    });
}
