import React, { useContext } from 'react';
import { AppContext } from 'contexts/AppContext';

export default function useApp() {
    const [state, setState] = useContext(AppContext);
    function SHOW_COMPOSEMAIL() {
        setState((state) => ({ ...state, stateOpen: 1 }));
    }
    function CLOSE_COMPOSEMAIL() {
        setState((state) => ({ ...state, stateOpen: 0 }));
    }
    function SHOW_PRIMARY_DRAWER({ child, title }) {
        setState((state) => ({ ...state, primaryDrawer: { display: true, child, title } }));
    }
    function CLOSE_PRIMARY_DRAWER() {
        setState((state) => ({ ...state, primaryDrawer: { display: false, child: <></>, title: '' } }));
    }

    function SHOW_SECONDARY_DRAWER({ child, title }) {
        setState((state) => ({ ...state, secondaryDrawer: { display: true, child, title } }));
    }

    function CLOSE_SECONDARY_DRAWER() {
        setState((state) => ({ ...state, secondaryDrawer: { display: false, child: <></>, title: '' } }));
    }
    function SEARCH_TEXT({ text }) {
        setState((state) => ({ ...state, searchText: text }));
    }
    const CLOSE_DRAWER = () => {
        if (state.secondaryDrawer.display) {
            CLOSE_SECONDARY_DRAWER();
        } else {
            state.primaryDrawer.display && CLOSE_PRIMARY_DRAWER();
        }
    };

    // declare the type for arguments pass in SHOW_MAILBOX

    type createMail = {
        type: 'createMail';
    };
    type editMail = {
        type: 'editMail';
        id: string;
    };
    type createSubMail = {
        type: 'createSubMail';
        id: string;
        mainMailTitle: string;
    };
    type editSubMail = {
        type: 'editSubMail';
        id: string;
        mainMailTitle: string;
    };

    // declare overloaded functions

    function SHOW_MAILBOX(mailBox: createMail): void;
    function SHOW_MAILBOX(mailBox: editMail): void;
    function SHOW_MAILBOX(mailBox: createSubMail): void;
    function SHOW_MAILBOX(mailBox: editSubMail): void;

    function SHOW_MAILBOX(mailBox) {
        let tmp;
        switch (mailBox.type) {
            case 'createMail':
                tmp = { ...mailBox, id: '', mainMailTitle: '' };
                break;
            case 'editMail':
                tmp = { ...mailBox, mainMailTitle: '' };
                break;
            case 'createSubMail':
                tmp = mailBox;
                break;
            case 'editSubMail':
                tmp = mailBox;
                break;
            default:
                throw new Error('show mail box context failure!');
        }
        setState((state) => ({ ...state, mailBox: tmp }));
    }

    function CLOSE_MAILBOX() {
        setState((state) => ({
            ...state,
            mailBox: {
                type: 'none',
                id: '',
                mainMailTitle: '',
            },
        }));
    }
    return {
        SHOW_COMPOSEMAIL,
        CLOSE_COMPOSEMAIL,
        SHOW_PRIMARY_DRAWER,
        CLOSE_PRIMARY_DRAWER,
        SHOW_SECONDARY_DRAWER,
        CLOSE_SECONDARY_DRAWER,
        stateOpen: state.stateOpen,
        primaryDrawer: state.primaryDrawer,
        secondaryDrawer: state.secondaryDrawer,
        searchText: state.searchText,
        CLOSE_DRAWER,
        mailBox: state.mailBox,
        SHOW_MAILBOX,
        CLOSE_MAILBOX,
        SEARCH_TEXT,
    };
}
