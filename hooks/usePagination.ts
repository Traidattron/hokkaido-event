import { useRouter } from 'next/router';
import { useEffect } from 'react';

export function usePagination(refetch) {
    const router = useRouter();
    const { query } = router;
    const { page, limit } = query;
    const numPage = Number(page) || 1;
    const numLimit = Number(limit) || 20;

    useEffect(() => {
        refetch({ limit: numLimit, offset: (numPage - 1) * numLimit });
        document.getElementsByClassName('ant-table-body')[0]?.scrollTo({ top: 0, behavior: 'smooth' });
    }, [numPage, numLimit]);

    const handlePagination = (current, pageSize) => {
        router.push({ query: { ...query, page: current, limit: pageSize } }, undefined, { shallow: true });
        // refetch({ limit: numLimit, offset: (numPage - 1) * numLimit });
    };

    return {
        handlePagination,
        page: numPage,
        limit: numLimit,
    };
}
