import { Modal } from 'antd';
import { _word } from 'data/language';
import React from 'react';

export function show_delete_modal({ selectedRows, onOk }) {
    Modal.confirm({
        title: (
            <>
                {_word.warning}
                <br />
                <h4>{selectedRows.length === 1 ? _word.sureToDeleteSingleItem : _word.sureToDeleteMultipleItem}</h4>
            </>
        ),
        content: (
            <>
                <ol>
                    {selectedRows.map((item, index) => (
                        <li key={index}>{item.name ?? item.title}</li>
                    ))}
                </ol>
            </>
        ),
        cancelText: _word.cancel,
        maskClosable: true,
        bodyStyle: { overflowY: 'auto', maxHeight: '70vh' },
        onOk: onOk,
    });
}
