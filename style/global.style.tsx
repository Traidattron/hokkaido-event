import { createGlobalStyle } from 'styled-components';
import colors from 'theme/colors';
export const GlobalStyle = createGlobalStyle`
    .row-class-name {
        cursor: pointer;
    }
    .ant-table-container{
        border: 1px solid ${colors.greyBorder};
    }
    .drawer-notification{
        .ant-drawer-title{
            font-size: 1.5rem;
        }
    }
    .ant-form-item-label > label{
            font-weight: 500;
    }
    .lable-notrule{
        .ant-form-item-label > label{
            margin-left: 10px;
        }
    }
    .input-datetimepicker{
        width: 100%;
    }
    .ant-modal{
        .ant-btn{
            width: 100px
        }
    }
    .popover-userbygroupmail{
        width: auto;
        max-width: 400px;
        .ant-popover-title{
            font-size: 1rem;
            text-align: left;
        }
        .ant-popover-inner-content{
            height: auto;
            overflow: auto;
            max-height: 400px;
        }
    }
    .popover-statistic-event{
        .ant-popover-title{
            font-size: 1rem;
            text-align: center;
            font-weight: 600;
        }
    }
    .ckeditor-content{
        .ant-col-22,.ant-col-20{
            width: 500px;
            .ck-content{
                height: 30vh;
            }
        }
    }
    .table-mail{
        .ant-table-row{
            cursor: pointer;
            &:hover{
                z-index: 1;
                box-shadow: rgba(50, 50, 93, 0.25) 0px 2px 5px -1px, rgba(0, 0, 0, 0.3) 0px 1px 3px -1px;
            }
        }
    }
    .table-event{
        .ant-table{
            background-color: transparent;
            .ant-table-title{
                padding: 16px 0;
            }
            .ant-table-container{
                background-color: #fff;
                border: 1px solid lightgray;
                box-shadow: rgba(0, 0, 0, 0.24) 0px 3px 8px;
            }
            .ant-btn-lg{
                border-color: transparent;
                border-radius: 10px !important;
                box-shadow: rgba(0, 0, 0, 0.24) 0px 3px 8px !important;
            }
            .ant-table-header{
                background: #0069b1;
                .ant-table-thead > tr > th{
                    background: #0069b1;
                    color: #fff;
                }
            }
            .ant-pagination-item-active{
                background-color: #0069b1;
                a{
                    color: #fff;
                }
            }
        }
        .ant-table-row{
            cursor: pointer;
        }
    }
    input:-internal-autofill-selected{
        background-color: transparent !important;
    }
    .ant-table-thead > tr > th{
        background: #ececec;
    }
    .site-tag-plus {
        background: #fff;
        border-style: dashed;
        cursor: pointer;
        padding: 5px;
        font-size: 0.8rem;
    }
    .edit-tag {
        padding: 5px;
        font-size: 0.8rem;
        user-select: none;
    }
    .tag-input {
        padding: 4px;
        width: 78px;
        margin-right: 8px;
        vertical-align: top;
    }
    .ant-drawer-title{
        font-weight: 600;
        font-size: 20px;
    }
    .d-none{
        display: none;
    }
    .ant-tooltip-inner{
        padding: 8px 10px;
        font-weight: 500;
        background-color: #545454;
        border-radius: 5px;
        font-size: 0.7rem;
    }
    .btn-icon{
        border-radius: 50%;
        box-shadow: rgb(0 0 0 / 24%) 0px 3px 8px;
        border-color: transparent;
        background: rgb(142, 142, 142);
        color: rgb(255, 255, 255);
        &:hover, &:focus{
            opacity: 0.7;
            border-color: transparent;
            background: rgb(142, 142, 142);
            color: rgb(255, 255, 255);
        }
    }
    .popover-pushnoti{
        .ant-popover-content{
            width: 400px;
        }
    }
    ///////////////////////////////////////////////
    .fade {
        animation: fadeIn 1s ease-in-out 1;
        @keyframes fadeIn {
            from {
                opacity: 0;
            }
            to {
                opacity: 1;
            }
        }
    }
    ///////////Custom scrollbar//////////
    ::-webkit-scrollbar-track
    {
        box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
        background-color: #F5F5F5;
    }
    ::-webkit-scrollbar
    {
        width: 8px;
        height: 8px;
        background-color: #F5F5F5;
    }
    ::-webkit-scrollbar-thumb
    {
        background-color: #1e2963;
        &:hover{
            background-color: #111738;
        }
    }
    /////////////////////////////////////
`;
