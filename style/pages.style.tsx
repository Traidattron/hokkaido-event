import colors from 'theme/colors';
import styled from 'styled-components';
// import background from 'image/background.jpg';
export const PageNotFound = styled.div`
    height: 100vh;
    width: 100vw;
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
    background-image: url('/bg-404.jpg');
    position: relative;
    section {
        position: absolute;
        top: 48%;
        left: 55%;
        margin: -103px 0 0 -120px;
        text-align: center;
        h1 {
            color: #1890ff;
            font-weight: 500;
            font-size: 120px;
            margin-bottom: 0;
        }
        p {
            color: #314659;
            font-size: 18px;
            a {
                color: #1890ff;
            }
        }
    }
`;
export const EventWrapper = styled.div`
    background-image: url('/IMG_2718.jpg');
    overflow: auto;
    height: 100vh;
    width: 100vw;
    font-size: 12px;
    /* Center and scale the image nicely */
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
    display: flex;
    flex-wrap: wrap;
    align-items: center;
    justify-content: center;
    .ant-result-title {
        color: #fff !important;
    }
    .ant-checkbox-group {
        margin-top: 20px;
        text-align: center;
    }
    .ant-checkbox-wrapper,
    .ant-checkbox-disabled + span {
        color: #fff;
        font-weight: 500;
    }
    .ant-select {
        color: #fff;
    }
    .ant-select-multiple .ant-select-selection-item-content {
        color: #000;
    }
    .ant-select:not(.ant-select-customize-input) .ant-select-selector {
        background-color: rgb(0 0 0 / 65%);
    }
    .input-mails {
        width: 25%;
    }
    .event-info-content {
        width: 70%;
        display: flex;
        align-items: center;
        justify-content: center;
        flex-direction: column;
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        padding: 20px 20px 60px 20px;
        background-color: rgb(0 0 0 / 63%);
        border-radius: 10px;
    }
    @media (max-width: 1024px) {
        & {
            flex-direction: column !important;
            padding: 20px 0;
            overflow: unset;
            height: auto;
            min-height: 100vh;
        }
        .input-mails {
            width: 80%;
        }
    }
    @media (max-width: 420px) {
        & {
            padding: 20px 0;
            overflow: unset;
            /* flex-direction: column !important; */
            height: auto;
            min-height: 100vh;
        }
        .input-mails {
            width: 80%;
        }
    }
`;
export const EventInfo = styled.div`
    height: 100vh;
    flex-basis: 60%;
    flex-grow: 1;
    font-size: 12px;
    position: relative;
    /* Center and scale the image nicely */
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
    .event-info-content {
        width: 70%;
        display: flex;
        align-items: center;
        justify-content: center;
        flex-direction: column;
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        padding: 20px 20px 60px 20px;
        background-color: rgb(0 0 0 / 63%);
        border-radius: 10px;
    }
    @media (max-width: 1024px) {
        & {
            height: auto;
            /* width: 100vw; */
            margin: 50px 0;
            width: 80%;
        }
        .event-info-content {
            width: 90%;
            position: unset;
            transform: unset;
            /* padding: 0;
            background-color: transparent; */
        }
    }
    @media (max-width: 420px) {
        & {
            height: auto;
            /* width: 100vw; */
            margin: 50px 0;
        }
        .event-info-content {
            /* position: unset;
            transform: unset; */
            padding: 0;
            background-color: transparent;
        }
    }
`;
export const EventForm = styled.div`
    height: 100vh;
    flex-basis: 40%;
    flex-grow: 1;
    font-size: 12px;
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
    .ant-checkbox-wrapper,
    .ant-checkbox-disabled + span {
        /* color: #000; */
        font-weight: 500;
        text-align: center;
    }
    @media (max-width: 1024px) {
        & {
            /* margin-top: 20px; */
            height: auto;
            /* width: 100vw; */
        }
    }
`;
export const ForwardForm = styled.div`
    padding: 30px 20px;
    height: auto;
    width: 55%;
    /* background-color: rgba(138, 75, 8, 0.4); */
    background-color: rgb(0 0 0 / 63%);
    border-radius: 10px;
    box-shadow: rgba(0, 0, 0, 0.35) 0px 5px 15px;
    h1 {
        color: #fff;
        text-align: center;
    }
    .form-forward .ant-form-item-label > label,
    .ant-col-24.ant-form-item-label > label,
    .ant-col-xl-24.ant-form-item-label > label {
        color: #fff;
    }
    @media (max-width: 1024px) {
        & {
            width: 70%;
        }
    }
    @media (max-width: 420px) {
        & {
            padding: 15px 10px;
            width: 80%;
        }
    }
`;
export const NameEvent = styled.div`
    margin: 0 auto;
    font-size: 3rem;
    font-weight: bold;
    text-align: center;
    text-transform: uppercase;
    color: white;
    padding: 15px;
    background-color: transparent;
    @media (max-width: 768px) {
        font-size: 2rem;
        width: 70vw;
    }
    @media (max-width: 420px) {
        width: 85vw;
        font-size: 1.5rem;
    }
`;
export const ULEvent = styled.ul`
    display: inline;
    text-align: center;
    padding: 0;
    li {
        display: inline-block;
        font-size: 1.5em;
        list-style-type: none;
        padding: 1em;
        text-transform: uppercase;
        .text-time {
            font-size: 1.5rem;
        }
    }
    li span {
        color: #fff;
        font-size: 3.5rem;
    }
    @media (max-width: 420px) {
        & {
            display: none;
        }
    }
`;
export const TextEvent = styled.span`
    width: 50vw;
    text-align: center;
    font-weight: bold;
    color: #fff;
    font-size: 1.2em;
    @media (max-width: 420px) {
        width: 85vw;
    }
`;
export const LinkText = styled.a`
    width: 50vw;
    text-align: center;
    font-weight: 500;
    color: #fff;
    font-size: 1.2em;
`;
export const LoginWrapper = styled.div`
    /* background-image: url('/background3.jpg'); */
    /* background-image: url('/bg.gif'); */
    background-image: url('/blur.jpg');
    height: 100vh;
    padding: 0 12vw;
    /* Center and scale the image nicely */
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
    display: flex;
    align-items: center;
    justify-content: space-between;
    .ant-form-item {
        font-weight: 500;
    }
`;
export const Logo = styled.h1`
    font-weight: 700;
    font-size: 5rem;
    color: #005b9a;
    text-shadow: #fafafa 2px 2px;
    animation-duration: 1s;
    animation-name: toRight;
    @keyframes toRight {
        0% {
            opacity: 0;
            transform: translateX(300px);
        }
        100% {
            opacity: 1;
            transform: translateX(0px);
        }
    }
`;
export const ContentLogin = styled.div`
    border-radius: 10px;
    width: 330px;
    animation-duration: 1s;
    animation-name: toLeft;
    background-color: #0069b1;
    @keyframes toLeft {
        0% {
            opacity: 0;
            transform: translateX(-300px);
            /* transform: scale(0.3); */
        }
        100% {
            opacity: 1;
            transform: translateX(0px);
            /* transform: scale(1); */
        }
    }
`;

export const FormWrapper = styled.div`
    padding: 40px 30px 50px 30px;
    text-align: center;
    width: 100%;
    height: 270px;
    background: #fff;
    position: relative;
    border-radius: 0px 0px 10px 10px;
    input:-webkit-autofill,
    input:-webkit-autofill:hover,
    input:-webkit-autofill:focus,
    input:-webkit-autofill:active {
        -webkit-box-shadow: 0 0 0 30px white inset !important;
    }
    .form-login {
        box-shadow: rgb(0 0 0 / 35%) 0px 5px 15px;
        border-radius: 10px;
        border: 1px solid #cecece;
        background: #fafafa;
        padding: 25px 20px;
        width: 265px;
        position: absolute;
        top: -40px;
        left: 50%;
        transform: translateX(-50%);
    }
    .ant-form-item-label > label {
        font-size: 16px;
        color: #000;
    }
    .ant-input {
        color: #000;
        background: #fff !important;
        border-radius: 3px;
        height: 100%;
    }
    .ant-form-item-control-input-content {
        height: 40px;
    }
    .ant-input-affix-wrapper {
        height: 100%;
    }
    .ant-btn {
        width: 70%;
        height: 40px;
    }
    .ant-btn-primary:focus {
        // color: #fff;
        // background: unset;
        // border-color: unset;
    }
    .ant-form-item-has-error .ant-input,
    .ant-form-item-has-error .ant-input-affix-wrapper,
    .ant-form-item-has-error .ant-input:hover,
    .ant-form-item-has-error .ant-input-affix-wrapper:hover {
        background: #fff !important;
    }
    .ant-form-item-has-error .ant-input:focus,
    .ant-form-item-has-error .ant-input-affix-wrapper:focus,
    .ant-form-item-has-error .ant-input-focused,
    .ant-form-item-has-error .ant-input-affix-wrapper-focused {
        box-shadow: none;
    }
    .ant-input-password-icon {
        color: #000;
        &:hover {
            color: #000;
        }
    }
    .input-password {
        .ant-form-item-control-input-content {
            background: #fff;
            border-radius: 3px;
        }
    }
`;
export const FormVertical = styled.div`
    display: flex;
    flex-direction: column;
`;
export const Title = styled.div`
    font-size: 25px;
    font-weight: bold;
    text-align: center;
    color: white;
    padding-bottom: 15px;
`;
export const DivImage = styled.div`
    position: absolute;
    top: 25px;
    left: 50%;
    transform: translateX(-50%);
`;
export const NameProject = styled.div`
    position: relative;
    display: flex;
    justify-content: flex-end;
    flex-direction: column;
    font-size: 2rem;
    height: 190px;
    width: 100%;
    font-weight: bold;
    text-align: center;
    color: white;
`;
export const ErrorText = styled.span`
    color: #8e8e8e;
    font-weight: 500;
`;
export const ColMasterPage = styled.div`
    flex: 0 0 50%;
    width: 50%;
    border: 1px solid #fafafa;
    .editable-row .ant-form-item-explain {
        position: absolute;
        top: 100%;
        font-size: 12px;
    }
    a {
        font-weight: 500;
    }
    @media (max-height: 900px) {
        .ant-table-title {
            padding: 10px;
        }
    }
`;
export const TableTitle = styled.span`
    font-weight: bold;
    color: ${colors.primaryTitle};
    font-size: 1.2em;
`;

export const ColumnTitle = styled.div`
    text-align: ${(props: { align?: 'left' | 'right' | 'center' | 'justify' }) => props.align ?? 'left'};
    font-weight: bold;
`;
export const ColumnBody = styled.div`
    cursor: pointer;
    display: flex;
    justify-content: space-between;
    align-items: center;
    .content-mail {
        color: ${colors.darkGrey};
        margin-left: 3px;
    }
    h5 {
        font-weight: 600;
    }
    /* * {
        white-space: nowrap;
        text-overflow: ellipsis;
        overflow: hidden;
    } */
`;
export const PaginationWrapper = styled.div`
    text-align: center;
    padding: 10px;
`;

export const SubmitEvent = styled.button`
    margin: 30px 0;
    letter-spacing: 0.1em;
    cursor: pointer;
    font-size: 14px;
    font-weight: 400;
    line-height: 45px;
    max-width: 200px;
    position: relative;
    text-decoration: none;
    text-align: center;
    text-transform: uppercase;
    width: 100%;
    &:hover {
        text-decoration: none;
    }
    color: #fff0f1;
    border: 2px solid #fff0f1;
    box-shadow: 0px 0px 0px 1px #000 inset;
    background-color: transparent;
    overflow: hidden;
    position: relative;
    transition: all 0.3s ease-in-out;

    &:hover {
        border: 2px solid #666;
        background-color: #fff;
        box-shadow: 0px 0px 0px 4px #eee inset;
    }

    /*btn_text*/
    & span {
        transition: all 0.2s ease-out;
        z-index: 2;
    }
    &:hover span {
        letter-spacing: 0.13em;
        color: #333;
    }

    /*highlight*/
    &:after {
        background: #fff;
        border: 0px solid #000;
        content: '';
        height: 155px;
        left: -75px;
        opacity: 0.8;
        position: absolute;
        top: -50px;
        -webkit-transform: rotate(35deg);
        transform: rotate(35deg);
        width: 50px;
        transition: all 1s cubic-bezier(0.075, 0.82, 0.165, 1); /*easeOutCirc*/
        z-index: 1;
    }
    &:hover:after {
        background: #fff;
        border: 20px solid #000;
        opacity: 0;
        left: 120%;
        -webkit-transform: rotate(40deg);
        transform: rotate(40deg);
    }
`;
// Detail Mail
export const DetailMailWrapper = styled.div`
    display: flex;
    padding: 16px;
    flex-direction: column;
`;
export const DetailMailButton = styled.div`
    display: flex;
    justify-content: space-between;
    span {
        font-weight: 500;
    }
`;
export const DetailMailContent = styled.div`
    width: 90%;
    margin: 20px auto;
    .header-panel {
        display: flex;
        align-items: center;
        font-weight: 700;
        color: ${colors.primary};
    }
    .ant-collapse-borderless {
        background-color: #ececec;
        border-radius: 5px;
    }
`;
export const GroupButtonMail = styled.div`
    margin-top: 10px;
    display: flex;
    justify-content: flex-end;
    .ant-btn {
        border-radius: 50%;
        text-align: center;
    }
    .btn-send-mail {
        border-radius: 10px;
    }
`;
export const IconCC = styled.span`
    font-weight: 600;
    background: #5e5e5e;
    color: white;
    border-radius: 10px;
    padding: 10px;
`;
export const PopoverStatisticEventContent = styled.div`
    display: flex;
    flex-direction: column;
    span {
        font-weight: 500;
    }
    .statistic-item {
        display: flex;
        justify-content: space-between;
    }
    .anticon-user {
        font-size: 0.7rem;
    }
`;
// DETAIL EVENT //
export const DetailEventWrapper = styled.div`
    display: flex;
    flex-direction: column;
    padding: 10px 20px;
    height: 100vh;
    overflow: auto;
`;
export const QRDetailEventContent = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    position: relative;
    &:hover .overlayQRCode {
        opacity: 1;
    }
    cursor: pointer;
    .overlayQRCode {
        background: rgba(0, 0, 0, 0.7);
        position: absolute;
        height: 100%;
        width: 180px;
        left: 50%;
        top: 0;
        bottom: 0;
        right: 0;
        transform: translateX(-50%);
        opacity: 0;
        transition: all 0.4s ease-in-out 0s;
        display: flex;
        justify-content: center;
        align-items: center;
        h1 {
            color: #fff;
        }
    }
`;

export const NotificationDataEvent = styled.div`
    display: flex;
    text-align: center;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    position: absolute;
    top: 50%;
    transform: translateY(-50%);
    color: darkgray;
    span {
        margin-top: 100px;
        font-size: 1rem;
    }
    .boxes {
        --size: 32px;
        --duration: 800ms;
        height: calc(var(--size) * 2);
        width: calc(var(--size) * 3);
        position: relative;
        transform-style: preserve-3d;
        transform-origin: 50% 50%;
        margin-top: calc(var(--size) * 1.5 * -1);
        transform: rotateX(60deg) rotateZ(45deg) rotateY(0deg) translateZ(0px);
        .box {
            width: var(--size);
            height: var(--size);
            top: 0;
            left: 0;
            position: absolute;
            transform-style: preserve-3d;
            &:nth-child(1) {
                transform: translate(100%, 0);
                animation: box1 var(--duration) linear infinite;
            }
            &:nth-child(2) {
                transform: translate(0, 100%);
                animation: box2 var(--duration) linear infinite;
            }
            &:nth-child(3) {
                transform: translate(100%, 100%);
                animation: box3 var(--duration) linear infinite;
            }
            &:nth-child(4) {
                transform: translate(200%, 0);
                animation: box4 var(--duration) linear infinite;
            }
            & > div {
                --background: ${colors.color};
                --top: auto;
                --right: auto;
                --bottom: auto;
                --left: auto;
                --translateZ: calc(var(--size) / 2);
                --rotateY: 0deg;
                --rotateX: 0deg;
                position: absolute;
                width: 100%;
                height: 100%;
                background: var(--background);
                top: var(--top);
                right: var(--right);
                bottom: var(--bottom);
                left: var(--left);
                transform: rotateY(var(--rotateY)) rotateX(var(--rotateX)) translateZ(var(--translateZ));
                &:nth-child(1) {
                    --top: 0;
                    --left: 0;
                }
                &:nth-child(2) {
                    --background: ${colors.colorRight};
                    --right: 0;
                    --rotateY: 90deg;
                }
                &:nth-child(3) {
                    --background: ${colors.colorLeft};
                    --rotateX: -90deg;
                }
                &:nth-child(4) {
                    --background: ${colors.shadow};
                    --top: 0;
                    --left: 0;
                    --translateZ: calc(var(--size) * 3 * -1);
                }
            }
        }
    }
    @keyframes box1 {
        0%,
        50% {
            transform: translate(100%, 0);
        }
        100% {
            transform: translate(200%, 0);
        }
    }

    @keyframes box2 {
        0% {
            transform: translate(0, 100%);
        }
        50% {
            transform: translate(0, 0);
        }
        100% {
            transform: translate(100%, 0);
        }
    }

    @keyframes box3 {
        0%,
        50% {
            transform: translate(100%, 100%);
        }
        100% {
            transform: translate(0, 100%);
        }
    }

    @keyframes box4 {
        0% {
            transform: translate(200%, 0);
        }
        50% {
            transform: translate(200%, 100%);
        }
        100% {
            transform: translate(100%, 100%);
        }
    }
`;
export const ImageEventContent = styled.div`
    width: 100%;
    height: 30%;
    text-align: center;
`;
export const DetailEventContent = styled.div`
    height: auto;
    width: 100%;
    margin-top: 10px;
    display: flex;
    flex-direction: column;
    .ant-btn {
        border-color: transparent;
        border-radius: 10px !important;
        box-shadow: rgba(0, 0, 0, 0.24) 0px 3px 8px !important;
    }
`;
export const DetailEvent = styled.div`
    padding: 10px;
    display: flex;
    flex-direction: column;
    a:hover {
        color: #0069b1 !important;
    }
`;
export const DetailEventName = styled.h1`
    font-size: 1.5rem;
    color: #000;
    font-weight: 600;
    text-align: center;
    word-break: break-all;
`;
export const DetailEventInfo = styled.p`
    font-size: 0.9rem;
    color: #000;
    font-weight: 500;
`;
export const HeaderDetailEvent = styled.div`
    width: 100%;
    position: relative;
    display: flex;
    justify-content: center;
    button {
        border-color: transparent;
        box-shadow: rgba(0, 0, 0, 0.24) 0px 3px 8px !important;
        &:hover {
            border-color: transparent;
            opacity: 0.8;
        }
    }
`;
export const CreateDate = styled.span`
    font-size: 0.7rem;
    color: #8e8e8e;
    font-weight: 500;
`;
