const langEN = {
    pushNoti: 'Push notification settings',
    detailEvent: 'Detail Event',
    sortBy: 'Sort By',
    ascending: 'Ascending (Oldest)',
    descending: 'Descending (Latest)',
    totalUsers: 'Total users',
    responseForm: 'Response Questionaire',
    filterUserByMailGroup: 'Filter Users by MailGroup:',
    addUserType: 'Add user type',
    action: 'Action',
    add: 'Add',
    addArea: 'Add Area',
    addCompany: 'Add Company',
    addCategory: 'Add Category',
    addEvent: 'Add Event',
    addMailGroup: 'Add Mail Group',
    address1: 'Address 1',
    address2: 'Address 2',
    addUser: 'Add User',
    area: 'Area',
    areaId: 'Area Id',
    areaManagement: 'Area Management',
    areaName: 'Area Name',
    attendFlag: 'AttendFlag',
    back: 'Back',
    body: 'Body',
    cancel: 'Cancel',
    canOnlyEditOne: 'You can only edit one item at a time',
    category: 'Category',
    categoryTypeManagement: 'Category Type Management',
    companies: 'Companies',
    companyId: 'Company Id',
    companyName: 'Company Name',
    companyType: 'Company Type',
    companyTypeManagement: 'Company Type Management',
    compose: 'Compose',
    composeMail: 'Compose Mail',
    composeSubMail: 'Compose SubMail',
    createAreaError: 'There was an error when create area',
    createAreaSuccess: 'Create area successfully!',
    createCompanyError: 'There was an error when create company',
    createCompanySuccess: 'Create company successfully!',
    createCategorySuccess: 'Create category successfully!',
    createdAt: 'CreatedAt',
    createdDate: 'Created date',
    createdOrUpdatedBy: 'Created By',
    createMailGroupError: 'There was an error when create mail group',
    createMailGroupSuccess: 'Create mail group successfully!',
    createMailSuccess: 'Create mail successfully!',
    saveMailSuccess: 'Save mail successfully!',
    testMailSuccess: 'Test mail successfully!',
    createSuccessfully: 'Create Successfully',
    dateFrom: 'Date From',
    dateTo: 'Date To',
    days: 'Days',
    delete: 'Delete',
    deleteSure: 'Do you want to delete?',
    deleteCompanyError: 'There was an error when delete this company',
    deleteCompanySuccess: 'Delete company successfully!',
    deleteMailGroupError: 'There was an error when delete this mail group',
    deleteMailGroupSuccess: 'Delete mail group successfully!',
    deleteMailSuccess: 'Delete mail successfully!',
    deleteSuccess: 'Delete Successfully!',
    department: 'Department',
    description: 'Description',
    edit: 'Edit',
    editCompany: 'Edit Company',
    editEvent: 'Edit Event',
    editMail: 'Edit Mail',
    editMailGroup: 'Edit Mail Group',
    editUser: 'Edit User',
    email: 'Email',
    eventManagement: 'Event Management',
    eventName: 'Event Name',
    eventOver: 'The event has expired!!!',
    eventType: 'Event Type',
    eventTypeId: 'Event Type Id',
    excludeUsers: 'Exclude Users',
    extendedTo: 'Extended To',
    fax: 'Fax',
    forward: 'Forward',
    groupName: 'Group Name',
    hours: 'Hours',
    includeUsers: 'Include Users',
    info: 'Info & Notication',
    invalidDate: 'Invalid data',
    invitationsReplied: 'Invitations Replied',
    invitationsSent: 'Invitations Sent',
    inviteEvent: 'Do you want to join this event ?',
    jobTitle: 'Job Title',
    join: 'Join',
    joinSuccess: 'Join Success',
    listCompanies: 'List Companies',
    livestreamFlag: 'LivestreamFlag',
    logo: 'HokkaidoEM',
    logoFullName: 'Hokkaido Exhibitor Management',
    logoLogin: 'Login',
    logOut: 'Log Out',
    mailContent: 'Mail Content',
    mailGroup: 'Mail Group',
    mailGroups: 'Mail Groups',
    mails: 'Mails',
    mailSendThankToJoin: 'Thank you email to the participants',
    mailSendToJoin: 'Mail sent to the participants',
    mailSendToNotJoin: 'Mail sent to those who are not participating',
    mailSent: 'Mail Sent',
    mailStatus: 'Mail Status',
    mailTitle: 'Mail Title',
    masterManagement: 'Master Management',
    minutes: 'Minutes',
    miraiExhibitorManagement: 'Hokkaido Exhibitor Management',
    mobile: 'Mobile',
    mustSelectItemsToDelete: 'You must select at least one item to delete',
    name: 'Name',
    notification: 'Notification',
    ok: 'OK',
    order: 'ID',
    password: 'Password',
    place: 'Place',
    placeAddress: 'Place Address',
    placeholderEmail: 'Example@mail.com',
    placeholderID: 'ID',
    placeholderPassword: 'Password',
    placeholderSearch: 'Input search text...',
    placeName: 'Place Name',
    postcode: 'Postcode',
    postOfficeFlag: 'PostOfficeFlag',
    refresh: 'Refresh',
    registerFrom: 'Register From',
    registerTo: 'Register To',
    replyRate: 'Reply Rate',
    role: 'Role',
    ruleCheckbox: 'Please choose Form Of Participation!',
    ruleInputAddress1: 'Please input Primary Address!',
    ruleInputArea: 'Please select an Area!',
    ruleInputCategory: 'Please select a Category!',
    ruleInputCompanyName: 'Please input Company Name!',
    ruleInputCompanyType: 'Please select a Company Type!',
    ruleInputEmail: 'Please input your email!',
    ruleInputPassword: 'Please input your password!',
    ruleInputPostcode: 'Please input Postcode!',
    ruleInputTel: 'Please input your phone number',
    ruleNotEmail: 'The input is not valid E-mail!',
    ruleRequired: 'This field is required',
    saveDate: 'Saved Date',
    savedMail: 'Saved Mail',
    saveMail: 'Save Mail',
    saveAndSentLaterMail: 'Save Or Sent Later',
    secondaryMail: 'Secondary Mails',
    seconds: 'Seconds',
    send: 'Send',
    sendThankToJoin: 'Send a thank you letter to the participants',
    sendTo: 'Send To',
    sendToJoin: ' Sent to AttendFlag',
    sendToJoinOnline: ' Sent to LivestreamFlag',
    sendToNotJoin: 'Send to ViewedLink',
    sendToPostOffice: ' Sent to PostOfficeFlag',
    sentDate: 'Sent Date',
    sentMail: 'Sent Mail',
    signIn: 'Sign In',
    signinSucess: 'Logged in successfully!',
    signoutSucess: 'Successfully logged out!',
    startDate: 'Start Date',
    subEvents: 'Include Sub Events',
    sureToCancel: 'Sure to cancel?',
    sureToDeleteMultipleItem: 'Are you sure to delete these items?',
    sureToDeleteSingleItem: 'Are you sure to delete this item?',
    tel: 'Tel',
    time: 'Time',
    timeFrom: 'Time From',
    timeTo: 'Time To',
    typeSubmail: 'Type',
    updateAreaError: 'There was an error when update this area',
    updateAreaSuccess: 'update area successfully!',
    updateCategory: 'update category successfully!',
    updateCompanyError: 'There was an error when update this company',
    updateCompanySuccess: 'Update company successfully!',
    updateCompanyType: 'update company type successfully!',
    updatedAt: 'UpdatedAt',
    updateMailGroupError: 'There was an error when update this mail group',
    updateMailGroupSuccess: 'Update mail group successfully!',
    updateMailSuccess: 'Update mail successfully!',
    updateUserSuccess: 'Update user successfully!',
    updateUserTypeSuccess: 'Update usertype successfully!',
    updateUserTypeError: 'There was an error when update this user type',
    users: 'Users',
    userType: 'User Type',
    userTypeId: 'User Type Id',
    userTypeManagement: 'User Type Management',
    view: 'View',
    viewLink: 'Viewed Link',
    warning: 'Warning!',
    createUserTypeSuccess: 'Create user type successfully!',
    createUserTypeError: 'There was an error when create user type',
    updatedBy: 'Updated By',
    sentBy: 'Sent By',
    mailTest: 'Mail Tests',
    textMailTests: 'Do you want to test mail?',
    test: 'Test',
    sendAt: 'Send At',
    placeholderSendAt: 'Choose a time if you want to send this mail later',
    organizationDate: 'Expiration date for registration',
    textIntroduce: 'Would you like to introduce to anyone ?',
    placeholderIntroduce: 'Enter the email you want to recommend',
    textForwardEvent: 'You are recommended by',
    placeholderFormForward: 'If you join, please register in the Registration Form',
    registrationForm: 'Registration Form',
    lableTypeJoin: 'Form Of Participation',
    createdBy: 'CreatedBy',
    usedLastestBy: 'UsedLastestBy',
    testnhi: 'test',
    uploadTooltip: 'Upload xlxs file',
    uploadTitle: 'Upload',
    mergeTitle: 'Merge',
    uploadGuide: 'Click or drag file to this area to upload',
    uploadHint:
        'Support for a single or bulk upload. Strictly prohibit from uploading company data or other band files',
    uploadSuccess: 'file uploaded successfully',
    uploadFailed: 'file upload failed',
    uploadFile: 'Uploaded file',
    scheduleSend: 'Schedule send',
    notificationLogout: 'You will be returned to the login screen!',
    notificationChangeForm: 'Changes you made may not be saved!',
    mailCount: 'Mail Count',
    eventStatistic: 'Event Statistic',
    mail: 'Mails',
    sending: 'Sending',
    primaryMail: 'Primary Mail',
    warningUserbyMailGroup: 'Mailgroup does not exist or has not been deleted',
    addUserEvent: 'Add User Event',
    mergeGroupMails: 'Merge GroupMails',
    warningMergeGroups: 'There are no group mails!',
    backToOutBox: 'Back To Outbox',
    warningDontHaveUser: 'There are no users!',
    questionaire: 'Questionaire',
    thumbnail: 'Thumbnail',
    selectEvent: 'Select an event to display details.',
    dontHave: `Don't Have`,
    clickToSee: 'Click here to see',
    loading: 'Loading',
    createQuestion: 'Click here to create question',
    downloadQR: 'Download QR Code',
    downloadUsers: 'Download Users',
    message: 'Message',
    all: 'All',
    allRegistered: 'All Registered',
    allNonRegistered: 'All Non-Registered',
    attended: 'Attended',
    answerForm: 'Answer Form',
    attendedAnswerForm: 'Attended & Answer Form',
};
const langJP = {
    all: '全てのユーザー',
    allRegistered: '登録済みユーザー',
    allNonRegistered: '未登録ユーザー',
    attended: 'チェックインしたユーザー',
    answerForm: 'アンケートに入力したユーザー',
    attendedAnswerForm: 'チェックインし、アンケートに入力したユーザー',
    message: 'メッセージ',
    pushNoti: 'プッシュ通知の設定',
    detailEvent: 'イベントの詳細',
    sortBy: '並び替え',
    ascending: '昇順で並び替え',
    descending: '降順で並び替え',
    placeholderID: 'ID',
    totalUsers: 'ユーザーの総数',
    responseForm: 'クリックしてアンケートの回答ページに移動します',
    downloadUsers: '参加者リストをダウンロードする',
    downloadQR: 'QRコードをダウンロード',
    createQuestion: '質問を作成するには、ここをクリックしてください',
    loading: '読み込み中。',
    clickToSee: 'ここをクリックしてご覧ください',
    dontHave: 'ありません',
    selectEvent: '詳細を表示するイベントを選択します。',
    thumbnail: 'イベント画像',
    questionaire: 'アンケート',
    saveMailSuccess: '保存しました！',
    testMailSuccess: '送信しました！',
    warningDontHaveUser: 'このメールグループにユーザーは存在していません',
    backToOutBox: '送信済みメールへ',
    mergeGroupMails: 'メールマージ',
    warningMergeGroups: 'このイベントにメールグループはありません',
    warningUserbyMailGroup: 'メールグループは存在していない、または既に削除されました',
    primaryMail: '初期メールアドレス',
    logoFullName: 'みらい出展管理',
    deleteSure: '選択されたアイテムを削除します。よろしいですか。',
    sending: '送信',
    mail: 'メールアドレス',
    eventStatistic: 'イベント統計',
    mailCount: 'メール件数',
    viewLink: 'リンク閲覧数',
    placeholderIntroduce: '紹介したいメールアドレスを入力してください',
    lableTypeJoin: 'イベント参加申込みフォーム',
    placeholderFormForward: '参加を希望する方は、情報の入力をお願いします。',
    textForwardEvent: '様から紹介されました',
    textIntroduce: '他の人に紹介しますか?',
    organizationDate: '登録の有効期限',
    registrationForm: '登録情報',
    filterUserByMailGroup: 'メールグループで絞り込み',
    action: '操作',
    add: '追加',
    addCompany: '企業の追加',
    addEvent: 'イベントの追加',
    addMailGroup: 'メールグループの追加',
    address1: '住所 1',
    address2: '住所 2',
    addUser: 'ユーザーの追加',
    area: '場所名',
    areaId: '場所ID',
    areaManagement: '場所管理',
    areaName: '場所名',
    attendFlag: 'オフラインで参加する',
    back: '戻る',
    body: '内容',
    cancel: 'キャンセル',
    canOnlyEditOne: '一度に１つのみ編集できます',
    category: 'カテゴリー',
    categoryTypeManagement: 'カテゴリータイプ管理',
    companies: '企業',
    companyId: '企業ID',
    companyName: '企業名',
    companyType: '企業タイプ',
    companyTypeManagement: '企業タイプ管理',
    compose: '作成',
    composeMail: 'メールの作成',
    composeSubMail: 'サブメールを作成する',
    createCompanyError: '企業の追加にエーラーが発生しました',
    createCompanySuccess: 'イベントの追加に成功しました',
    createdAt: '追加された時間',
    createdDate: '作成された日付',
    createdOrUpdatedBy: '作成者',
    createMailGroupError: 'メールグループの追加にエーラーが発生しました',
    createMailGroupSuccess: 'メールグループの追加に成功しました!',
    createMailSuccess: 'テストメールを送信しました！',
    createSuccessfully: '追加に成功しました',
    dateFrom: '開始日付',
    dateTo: '終了日付',
    days: '日々',
    delete: '削除',
    deleteCompanyError: '企業の削除にエーラーが発生しました',
    deleteCompanySuccess: 'イベントの削除に成功しました!',
    deleteMailGroupError: 'メールグループの削除にエーラーが発生しました',
    deleteMailGroupSuccess: 'メールグループの削除に成功しました!',
    deleteMailSuccess: 'メールの削除に成功しました!',
    deleteSuccess: '削除に成功しました!',
    department: '部門',
    description: '説明',
    edit: '編集',
    editCompany: '企業の編集',
    editEvent: 'イベントの編集',
    editMail: 'メールの編集',
    editMailGroup: 'メールグループの編集',
    editUser: 'ユーザーの編集',
    email: 'Eメール',
    eventManagement: 'イベント管理',
    eventName: 'イベント名',
    eventOver: 'イベントはもう終了しました!!!',
    eventType: 'イベントタイプ',
    eventTypeId: 'イベントタイプID',
    excludeUsers: 'ユーザー',
    extendedTo: '延長',
    fax: 'ファクス',
    forward: 'メールを転送する',
    groupName: 'グループ名',
    hours: '時間',
    includeUsers: 'ユーザーを含む',
    info: '情報とお知らせ',
    invalidDate: 'データは有効ではありません',
    invitationsReplied: '返信の招待状',
    invitationsSent: '送信済招待状',
    inviteEvent: 'このイベントに参加しますか ?',
    jobTitle: '職業',
    join: '参加します',
    joinSuccess: '参加が成功しました。',
    listCompanies: '企業リスト',
    livestreamFlag: 'オンラインで参加する',
    logo: '北海道イベント',
    logoLogin: 'ログイン',
    logOut: 'ログアウト',
    mailContent: '内容',
    mailGroup: 'メールグループ',
    mailGroups: 'メールグループ',
    mails: 'メール',
    mailSendThankToJoin: '参加者へのお礼メール',
    mailSendToJoin: '参加者への送信済メール',
    mailSendToNotJoin: '不参加者へのメール',
    mailSent: '送信済メール',
    mailStatus: 'メール状態',
    // mailTitle: 'メールタイトル',
    mailTitle: '件名',
    masterManagement: 'マスタ管理',
    minutes: '分',
    miraiExhibitorManagement: 'みらいイベント管理システム',
    mobile: '携帯電話',
    mustSelectItemsToDelete: '少なくとも１つのアイテムを選択してください',
    name: 'ユーザー名',
    notification: 'お知らせ',
    order: 'ID',
    password: 'パスワード',
    place: '場所名',
    placeAddress: '住所',
    placeholderEmail: 'Example@mail.com',
    placeholderPassword: 'パスワード',
    placeholderSearch: 'キーワードを入力してください',
    placeName: '場所名',
    postcode: '郵便番号',
    registerFrom: '登録日時',
    registerTo: '登録終了日時',
    postOfficeFlag: 'メールで資料を受信する',
    refresh: '更新',
    replyRate: '返信率',
    role: '役割',
    ruleCheckbox: 'ご選択ください!',
    ruleInputAddress1: '住所を入力してください',
    ruleInputArea: '場所を一つ選択してください!',
    ruleInputCategory: 'カテゴリーを選択してください!',
    ruleInputCompanyName: '企業名を入力してください!',
    ruleInputCompanyType: '企業タイプを入力してください!',
    ruleInputEmail: 'Eメールを入力してください!',
    ruleInputPassword: 'パスワードを入力してください!',
    ruleInputPostcode: '郵便番号を入力してください!',
    ruleInputTel: '電話番号を入力してください!',
    ruleNotEmail: 'Eメールは有効ではありません!',
    ruleRequired: 'これは必須な項目です',
    saveDate: '保存日',
    savedMail: '保存メール一覧',
    saveMail: 'メール保存',
    secondaryMail: '二次電子メール',
    seconds: '秒',
    send: '送信',
    sendThankToJoin: '参加者にお礼メールを送信する',
    sendTo: 'To',
    sendToJoin: ' 参加者に送信する',
    sendToJoinOnline: ' オンラインで参加者に送信する',
    sendToNotJoin: 'リンク見た人に送信する',
    sendToPostOffice: ' 郵便で資料をもらう人に送信する',
    sentDate: '開始日付',
    sentMail: '送信済みメール',
    signIn: 'サインイン',
    signinSucess: 'ログインに成功しました!',
    signoutSucess: 'ログアウトに成功しました!',
    startDate: '開始日',
    subEvents: 'サブイベントを含む',
    sureToCancel: 'キャンセルします。よろしいですか?',
    sureToDeleteMultipleItem: '選択されたアイテムを削除します。よろしいですか。',
    sureToDeleteSingleItem: '選択されたアイテムを削除します。よろしいですか。',
    tel: '電話番号',
    time: '時間',
    timeFrom: '開催時間',
    timeTo: '終了時間',
    typeSubmail: 'タイプ',
    updateAreaSuccess: '場所のアップデートに成功しました!',
    updateCategory: 'カテゴリーのアップデートに成功しました!',
    updateCompanyError: '企業のアップデートにエーラーが発生しました',
    updateCompanySuccess: 'イベントの更新に成功しました!',
    updateCompanyType: '企業タイプのアップデートに成功しました!',
    updatedAt: '更新時間',
    updateMailGroupError: 'メールグループのアップデートにエーラーが発生しました',
    updateMailGroupSuccess: 'メールグループのアップデートに成功しました!',
    updateMailSuccess: 'メールのアップデートに成功しました!',
    updateUserSuccess: 'ユーザーのアップデートに成功しました!',
    updateUserTypeSuccess: 'ユーザータイプのアップデートに成功しました!',
    users: 'ユーザー',
    userType: 'ユーザータイプ',
    userTypeId: 'ユーザータイプ Id',
    userTypeManagement: 'ユーザータイプ 管理',
    view: '表示',
    warning: 'お知らせ',
    createdBy: '作成者',
    usedLastestBy: '最後の使用者',
    updatedBy: '更新者',
    sentBy: '送信者',
    sendAt: 'に送信',
    mailTest: 'テストメール',
    test: 'テスト',
    scheduleSend: '送信日時',
    notificationLogout: 'ログアウトします。よろしいですか。',
    notificationChangeForm: '更新情報を保存しません。よろしいですか？',
    mergeTitle: '併合',
    addUserType: 'ユーザータイプの追加',
    addArea: '場所の追加',
    addCategory: 'カテゴリーの追加',
    createAreaSuccess: '場所の追加に成功しました',
    createCategorySucucess: 'カテゴリーの追加に成功しました',
    saveAndSentLaterMail: '保存して後で送信する',
    createUserTypeSuccess: 'ユーザータイプの作成に成功しました',
    textMailTests: 'メール送信のテストをしますか?',
    placeholderSendAt: '送信する日時を設定してください',
    uploadTooltip: 'エクセルファイルのアップロード',
    uploadTitle: 'アップロード',
    uploadSuccess: 'ファイルアップロードに成功しました',
    uploadfile: 'ファイルをアップロードしました',
    addUserEvent: 'エクセルファイルでユーザーの追加',
};
const language = {
    jp: Object.assign({}, langEN, langJP),
    en: langEN,
};

// const arrayEN = Object.keys(langEN);
// const arrayJP = Object.keys(langJP);
// arrayEN.some((currentENValue) => {
//     if (
//         !arrayJP.some((currentJPValue) => {
//             return currentENValue === currentJPValue;
//         })
//     )
//         console.warn('EN have but JP nope: ' + currentENValue);
// });

// arrayJP.some((currentENValue) => {
//     if (
//         !arrayEN.some((currentJPValue) => {
//             return currentENValue === currentJPValue;
//         })
//     )
//         console.warn('JP have but EN nope: ' + currentENValue);
// });

export const _word = language[process.env.NEXT_PUBLIC_LANGUAGE === 'en' ? 'en' : 'jp'];
