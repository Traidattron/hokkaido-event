import { _word } from 'data/language';
import moment from 'moment';
import { dateFormatJP, fullFormatJP } from 'constants/format';
import { ColumnTitle, DetailEvent, CreateDate } from 'style/pages.style';
import colors from 'theme/colors';
import { Button, Popover, Tooltip } from 'antd';
import React from 'react';
import { NotificationOutlined } from '@ant-design/icons';
import PushNotificationForm from 'components/PushNotification/PushNotification';

export const columnsUserAddEvent = [
    {
        title: () => <ColumnTitle>{_word.name}</ColumnTitle>,
        dataIndex: 'nameKanji',
        key: 'nameKanji',
    },
    {
        title: () => <ColumnTitle>{_word.jobTitle}</ColumnTitle>,
        dataIndex: 'jobTitle',
        key: 'jobTitle',
    },
    {
        title: () => <ColumnTitle>{_word.department}</ColumnTitle>,
        dataIndex: 'department',
        key: 'department',
    },
    {
        title: () => <ColumnTitle>{_word.companyName}</ColumnTitle>,
        dataIndex: 'companyName',
        key: 'companyName',
    },
    {
        title: () => <ColumnTitle>{_word.email}</ColumnTitle>,
        dataIndex: 'email',
        key: 'email',
    },
    {
        title: () => <ColumnTitle>{_word.tel}</ColumnTitle>,
        dataIndex: 'tel',
        key: 'tel',
    },
];
export const columnsUser = [
    {
        title: (text, record) => <ColumnTitle>{_word.name}</ColumnTitle>,
        dataIndex: 'name',
        key: 'name',
        fixed: 'left',
    },
    { title: () => <ColumnTitle>{_word.email}</ColumnTitle>, dataIndex: 'email', key: 'email' },
    {
        title: () => <ColumnTitle>{_word.companies}</ColumnTitle>,
        dataIndex: 'company',
        key: 'company',
        render: (value, record) => <span>{record?.company?.name}</span>,
    },
    { title: () => <ColumnTitle>{_word.department}</ColumnTitle>, dataIndex: 'department', key: 'department' },
    { title: () => <ColumnTitle>{_word.jobTitle}</ColumnTitle>, dataIndex: 'jobTitle', key: 'jobTitle' },
    { title: () => <ColumnTitle>{_word.mobile}</ColumnTitle>, dataIndex: 'mobile', key: 'mobile' },
    { title: () => <ColumnTitle>{_word.address1}</ColumnTitle>, dataIndex: 'address1', key: 'address1' },
    { title: () => <ColumnTitle>{_word.address2}</ColumnTitle>, dataIndex: 'address2', key: 'address2' },
    { title: () => <ColumnTitle>{_word.fax}</ColumnTitle>, dataIndex: 'fax' },
];
export const columnsEvent = [
    // {
    //     title: () => <ColumnTitle>{_word.notification}</ColumnTitle>,
    //     key: 'notification',
    //     fixed: true,
    //     width: '5%',
    //     render: (value, record) => <span>{record.id}</span>,
    // },
    {
        title: () => <ColumnTitle>{_word.eventName}</ColumnTitle>,
        dataIndex: 'name',
        key: 'name',
        fixed: true,
        render: (value, record) => {
            const tooltipCreate = `${moment(record.createdAt).format(fullFormatJP)}`;
            const tooltipUpdate = `${moment(record.updatedAt).format(fullFormatJP)}`;
            return (
                <DetailEvent style={{ padding: 0 }}>
                    <b style={{ color: colors.primaryText, fontSize: '1rem' }}>{value}</b>
                    {record.createdAt != record.updatedAt ? (
                        <Tooltip placement='bottomLeft' title={tooltipUpdate}>
                            <CreateDate style={{ color: '#349739' }}>
                                {moment(record.updatedAt).fromNow()}に更新
                            </CreateDate>
                        </Tooltip>
                    ) : (
                        <Tooltip placement='bottomLeft' title={tooltipCreate}>
                            <CreateDate>{moment(record.createdAt).fromNow()}に追加</CreateDate>
                        </Tooltip>
                    )}
                </DetailEvent>
            );
        },
    },
    {
        title: () => <ColumnTitle>{_word.placeName}</ColumnTitle>,
        dataIndex: 'placeName',
        key: 'placeName',
    },
    {
        title: () => <ColumnTitle>{_word.placeAddress}</ColumnTitle>,
        dataIndex: 'placeAddress',
        key: 'placeAddress',
    },
    {
        title: () => <ColumnTitle>{_word.dateFrom}</ColumnTitle>,
        dataIndex: 'dateFrom',
        key: 'dateFrom',
        render: (value, record) => {
            if (value === record.dateTo) {
                return <span>{moment(value).format(dateFormatJP)}</span>;
            } else {
                return (
                    <div style={{ display: 'flex', flexDirection: 'column' }}>
                        <span>{moment(value).format(dateFormatJP)}</span>
                        <span>{moment(record.dateTo).format(dateFormatJP)}</span>
                    </div>
                );
            }
        },
    },
    {
        title: () => <ColumnTitle>{_word.timeFrom}</ColumnTitle>,
        dataIndex: 'timeFrom',
        key: 'timeFrom',
        render: (value, record) => (
            <>
                <span>{value}</span> ~ <span>{record.timeTo}</span>
            </>
        ),
    },
    {
        title: () => <ColumnTitle>{_word.description}</ColumnTitle>,
        dataIndex: 'description',
        key: 'description',
        ellipsis: true,
    },
    {
        title: () => <ColumnTitle>{_word.questionaire}</ColumnTitle>,
        dataIndex: 'formAddress',
        key: 'formAddress',
        ellipsis: true,
        render: (value, record) => (
            <a href={value} target='_blank'>
                {value}
            </a>
        ),
    },
];
