export const areasData = [
    {
        id: 1,
        name: '全社共通',
    },
    {
        id: 2,
        name: '東京',
    },
    {
        id: 3,
        name: '中部',
    },
    {
        id: 4,
        name: '関西',
    },
    {
        id: 5,
        name: '北海道',
    },
];

export const userTypesData = [
    {
        id: 1,
        type: 'ALL',
    },
    {
        id: 2,
        type: 'みらい市',
    },
    {
        id: 3,
        type: 'みらい総会',
    },
    {
        id: 4,
        type: '方針説明会',
    },
    {
        id: 5,
        type: '賀詞交歓会',
    },
];

export const companyTypesData = [
    {
        id: 1,
        type: 'メーカー',
    },
    {
        id: 2,
        type: '販売店',
    },
    {
        id: 3,
        type: 'その他',
    },
    {
        id: 4,
        type: '工事店',
    },
];

export const categoriesData = [
    {
        id: 1,
        name: '管・継手',
    },
    {
        id: 2,
        name: '産機・土木',
    },
    {
        id: 3,
        name: '給水システム',
    },
];
