let data = [
    {
        key: '1',
        eventName: 'Test Event',
        startDate: '2021/3/1',
        time: '10:00-12:00',
        place: 'Hotel trivago',
        invitationsSent: 200,
        invitationsReplied: 50,
    },
    {
        key: '2',
        eventName: 'Tokyo Mirai Ichi',
        startDate: '2021/3/1',
        time: '10:00-12:00',
        place: 'Tokyo',
        invitationsSent: 200,
        invitationsReplied: 50,
    },
    {
        key: '3',
        eventName: 'Comic event',
        startDate: '2021/3/1',
        time: '10:00-12:00',
        place: 'Makuhari Messe',
        invitationsSent: 200,
        invitationsReplied: 50,
    },
];

for (let i = 0; i < 10; i++) {
    let tmpData = {
        key: `${i + 4}`,
        eventName: `Comic event ${i + 4}`,
        startDate: '2021/3/1',
        time: '10:00-12:00',
        place: 'Makuhari Messe',
        invitationsSent: 200,
        invitationsReplied: Math.floor(Math.random() * 201),
    };
    data = data.concat(tmpData);
}

export { data };
