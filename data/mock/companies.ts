export const dataSource = [
    {
        id: 1,
        name: '日本製鉄㈱',
        postCode: '100-0005',
        address1: '東京都千代田区丸の内2-6-1',
        address2: null,
        tel: '03-6867-5773',
        fax: '03-6867-3570',
        category: {
            name: '給水システム',
        },
        companyType: {
            type: 'メーカー',
        },
        area: {
            name: '全社共通',
        },
    },
    {
        id: 2,
        name: 'レッキス工業㈱',
        postCode: '177-0032',
        address1: '東京都練馬区谷原5-13-30',
        address2: null,
        tel: '03-5393-6011',
        fax: '03-6686-0445',
        category: {
            name: '管・継手',
        },
        companyType: {
            type: 'メーカー',
        },
        area: {
            name: '東京',
        },
    },
];
