import { Button, Drawer, Space, Modal } from 'antd';
import { _word } from 'data/language';
import useApp from 'hooks/useApp';
import React, { cloneElement, useState } from 'react';

const PrimaryDrawer = () => {
    const { secondaryDrawer, CLOSE_SECONDARY_DRAWER } = useApp();
    const [mutationLoading, setMutationLoading] = useState<boolean>(false);

    const setLoading = (flag) => {
        setMutationLoading(flag);
    };
    return (
        <>
            <Drawer
                width='520'
                placement='right'
                title={secondaryDrawer.title}
                visible={secondaryDrawer.display}
                onClose={CLOSE_SECONDARY_DRAWER}
                destroyOnClose
                footer={
                    <Space size='middle'>
                        <Button onClick={CLOSE_SECONDARY_DRAWER} style={{ width: 100 }}>
                            {_word.cancel}
                        </Button>
                        <Button
                            style={{ width: 100 }}
                            type='primary'
                            form='form-in-modal-secondary'
                            htmlType='submit'
                            loading={mutationLoading}
                        >
                            {_word.ok}
                        </Button>
                    </Space>
                }
                footerStyle={{ display: 'flex', justifyContent: 'flex-end' }}
            >
                {cloneElement(secondaryDrawer.child, {
                    formName: 'form-in-modal-secondary',
                    setLoading,
                })}
            </Drawer>
            <Modal title={_word.warning}></Modal>
        </>
    );
};

export default PrimaryDrawer;
