import React, { ReactNode, useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import { Form, Collapse, Layout, Modal, Input } from 'antd';
import { SearchOutlined } from '@ant-design/icons';
import { LayoutWrapper, Logo, DivLogo, HeaderContentWrapper, BoxMail } from './AppLayout.style';
import { ROUTER } from 'constants/navigation';
import { _word } from 'data/language';
import InfoUser from 'containers/InfoUser/InfoUser';
import colors from 'theme/colors';
import { useAuth } from 'hooks/useAuth';
import useApp from 'hooks/useApp';
import { useHasMounted } from 'hooks/useHasMounted';
import PrimaryDrawer from 'containers/PrimaryDrawer/PrimaryDrawer';
import moment from 'moment';
import DetailEventContainer from 'containers/DetailEvent/DetailEvent';

moment.locale('ja');
const { Search } = Input;
const { Sider, Content } = Layout;
const { Panel } = Collapse;
const { confirm } = Modal;
type AppLayoutProps = {
    children: ReactNode;
};
export default function AppLayout({ children }: AppLayoutProps) {
    const hasMounted = useHasMounted();
    const router = useRouter();
    const { eventID } = router.query;
    const [formKey, setFormKey] = useState(0);
    const { pathname } = router;
    const { isAuthenticated } = useAuth();
    const { mailBox, SEARCH_TEXT } = useApp();

    const onSearch = (e: any) => {
        const { value } = e.target;
        if (value === '') {
            SEARCH_TEXT({ text: undefined });
        } else {
            SEARCH_TEXT({ text: value });
        }
    };

    useEffect(() => {
        if (!isAuthenticated && pathname !== ROUTER.EVENT_REGISTRATION) router.replace(ROUTER.LOGIN);
    }, [isAuthenticated]);

    useEffect(() => {
        //Thay đổi router thì reset lại searchText và input Search
        SEARCH_TEXT({ text: undefined });
        setFormKey((formKey || 0) + 1);
    }, [pathname]);

    if (!hasMounted) {
        return null;
    }
    /**
     * FIXME: window gọi ở đây có đúng không? Hay là gọi ở useEffect?
     */
    window.onbeforeunload = function () {
        if (mailBox.type !== 'none') {
            return 'Sure?';
        }
    };
    return (
        <>
            <LayoutWrapper>
                <Layout style={{ minHeight: '100vh' }}>
                    <Layout>
                        <Content
                            style={{
                                background: '#fafafa',
                                height: 'calc(100vh)',
                                overflowY: 'auto',
                                padding: '0 16px',
                                paddingTop: 64,
                            }}
                        >
                            <HeaderContentWrapper style={{ width: eventID ? '80vw' : '100vw' }}>
                                <div>
                                    <Form className='search-layout' key={formKey}>
                                        <Form.Item name='input-search' noStyle>
                                            <Search
                                                id='input-search-layout'
                                                bordered={false}
                                                style={{ width: 400 }}
                                                placeholder={_word.placeholderSearch}
                                                prefix={
                                                    <SearchOutlined
                                                        style={{ color: colors.darkGrey, fontSize: '1.3em' }}
                                                    />
                                                }
                                                allowClear
                                                size='large'
                                                onChange={onSearch}
                                            />
                                        </Form.Item>
                                    </Form>
                                </div>
                                <InfoUser />
                            </HeaderContentWrapper>
                            <PrimaryDrawer />
                            {children}
                        </Content>
                        {eventID && (
                            <Sider style={{ flex: '0 0 300px', maxWidth: '300px', minWidth: '300px', width: '300px' }}>
                                <DetailEventContainer />
                            </Sider>
                        )}
                    </Layout>
                </Layout>
            </LayoutWrapper>
        </>
    );
}
