import styled from 'styled-components';
import colors from 'theme/colors';

export const LayoutWrapper = styled.div`
    .ant-layout-sider {
        flex: 0 0 20vw !important;
        max-width: 20vw !important;
        min-width: 20vw !important;
        width: 20vw !important;
        background: none;
    }
    .ant-layout-sider-children {
        display: flex;
        color: red;
        flex-direction: column;
    }
    .ant-layout-header {
        padding: 0px 30px;
    }
    .ant-menu {
        font-weight: 500;
    }
    .btn-composemail {
        position: fixed;
        bottom: 70px;
        right: 140px;
        border-radius: 100px;
        height: 70px;
        line-height: 5px;
        font-size: 1.3rem;
        font-weight: 500;
        width: 150px;
        margin: 0 auto;
        color: #fff;
        background: #001529;
        box-shadow: 0 1px 6px 0 rgba(4, 4, 5, 0.28);
        &:hover {
            border: 1px solid #000;
            box-shadow: rgba(0, 0, 0, 0.15) 1.95px 1.95px 2.6px;
            text-decoration: none;
            text-shadow: -1px -1px 0 #001529;
            transition: all 250ms linear;
        }
    }
`;
export const BoxMail = styled.div`
    overflow: hidden;
    position: fixed;
    border-radius: 10px 10px 0px 0px;
    width: 650px;
    bottom: 0;
    right: 15px;
    z-index: 9;
    .ant-collapse {
        border: 1px solid #40434e;
        box-shadow: 0 1px 6px 0 rgba(32, 33, 36, 0.28);
        .ant-collapse-header {
            cursor: default;
            background-color: #40434e;
        }
        .anticon-close-circle,
        .anticon-line,
        .anticon-arrows-alt,
        .anticon-shrink {
            transition: all 0.5s;
            padding: 3px;
            &:hover {
                border-radius: 2px;
                background-color: #3f3f3f;
                color: ${colors.primary};
            }
        }
        .ant-collapse-item > .ant-collapse-header {
            color: #fff;
            font-weight: 600;
            font-size: 1rem;
        }
        .ant-collapse-content.ant-collapse-content-active.ant-motion-collapse-enter.ant-motion-collapse-enter-active.ant-motion-collapse {
            height: auto !important; //Fix bug height when collapse with fullscreen
        }
    }
    .ant-picker-content:last-child {
        overflow: hidden;
    }
    .item-option {
        animation-duration: 0.5s;
        animation-name: slidein;
        @keyframes slidein {
            from {
                margin-left: 100%;
                width: 300%;
            }
            to {
                margin-left: 0%;
                width: 100%;
            }
        }
    }
    .tag-compose-mail {
        display: inline-flex;
        margin: auto auto auto 8px;
        padding: 1px 7px;
        font-size: 0.9rem;
        max-width: 40%;
        span {
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
        }
    }
    .ant-motion-collapse {
        transition: unset !important;
    }
    &.fullscreen {
        position: absolute;
        width: 90vw;
        height: fit-content;
        top: 6vh;
        left: 5vw;
        .ck-content {
            height: 35vh !important;
        }
    }
    &.minimise {
        position: fixed;
        width: 350px !important;
        height: auto;
        top: unset;
        left: unset;
        bottom: 0;
        right: 15px;
        .tag-compose-mail {
            display: none;
        }
    }
`;
export const DivLogo = styled.div`
    display: flex;
    align-items: center;
    width: 20vw;
    height: 80px;
    justify-content: center;
    position: fixed;
    top: 0;
    z-index: 10;
    right: 0;
    background-color: #001529;
`;
export const Logo = styled.span`
    /* color: ${colors.primary}; */
    /* color: #fff;
    font-size: 2em;
    font-weight: bold;
    text-transform: uppercase; */
    margin-left: 10px;
    color: #fff;
    font-size: 2.2rem;
    text-transform: uppercase;
    font-weight: 700;
    background: linear-gradient(to right, #25abe8 10%, #212f6f 50%, #25abe8 60%);
    background-size: auto auto;
    background-clip: border-box;
    background-size: 200% auto;
    background-clip: text;
    text-fill-color: transparent;
    -webkit-background-clip: text;
    -webkit-text-fill-color: transparent;
    animation: textclip 2s linear infinite;
    display: inline-block;
    @keyframes textclip {
        to {
            background-position: 200% center;
        }
    }
`;
export const TitleTableWrapper = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
    .ant-select-single.ant-select-sm:not(.ant-select-customize-input) .ant-select-selector {
        padding: 7px;
        border-radius: 5px;
    }
    .ant-select-single.ant-select-sm:not(.ant-select-customize-input) .ant-select-selector {
        height: 36px;
    }
`;
export const SortByWrapper = styled.div`
    display: flex;
    align-items: center;
    margin: 0 10px;
    color: #fff;
    background: #8e8e8e;
    height: 40px;
    padding: 0 5px;
    border-radius: 10px;
    box-shadow: rgb(0 0 0 / 24%) 0px 3px 8px;
    p {
        font-weight: 500;
        margin: 0 10px;
    }
`;
export const ButtonWrapper = styled.div`
    display: flex;
    flex-direction: row;
`;
export const HeaderContentWrapper = styled.div`
    padding: 0 16px;
    background: #fff;
    box-shadow: 0 1px 6px 0 rgba(32, 33, 36, 0.28);
    position: fixed;
    top: 0;
    left: 0;
    z-index: 2;
    height: 64px;
    width: 80vw;
    display: flex;
    align-items: center;
    justify-content: space-between;
    div {
        display: flex;
        align-items: center;
    }
    h3 {
        color: #000;
        margin: 0;
        margin-left: 10px;
    }
    .ant-input-group-addon {
        visibility: hidden;
    }
    .search-layout {
        .ant-input-affix-wrapper {
            border-radius: 5px !important;
            box-shadow: rgb(50 50 93 / 25%) 0px 2px 5px -1px, rgb(0 0 0 / 30%) 0px 1px 3px -1px;
            background: #f0f0f0;
        }
        .ant-input-affix-wrapper:focus,
        .ant-input-affix-wrapper-focused {
            background: #fff !important;
        }
    }
`;
