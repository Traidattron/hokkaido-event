import React from 'react';
import { _word } from 'data/language';
import { NotificationWrapper } from './Notification.style';
import { NotificationOutlined } from '@ant-design/icons';
import { List, Avatar } from 'antd';
const data = [
    {
        title: 'Title Notification 1',
    },
    {
        title: 'Title Notification 2',
    },
    {
        title: 'Title Notification 3',
    },
    {
        title: 'Title Notification 4',
    },
];
const NotificationDrawer = () => {
    return (
        <NotificationWrapper>
            <h2>{_word.notification}</h2>
            <div>
                <List
                    itemLayout='horizontal'
                    dataSource={data}
                    renderItem={(item) => (
                        <List.Item>
                            <List.Item.Meta
                                avatar={
                                    <Avatar style={{ backgroundColor: '#5367b5' }} icon={<NotificationOutlined />} />
                                }
                                title={<a href='https://ant.design'>{item.title}</a>}
                                description='This is a demo. It will be updated soon!'
                            />
                        </List.Item>
                    )}
                />
            </div>
        </NotificationWrapper>
    );
};
export default NotificationDrawer;
