import styled from 'styled-components';
import colors from 'theme/colors';

export const NotificationWrapper = styled.div`
    .site-page-header {
        margin: 10px 0;
    }
    .fade {
        animation: fadeIn 1s ease-in-out 1;
        @keyframes fadeIn {
            from {
                opacity: 0;
            }
            to {
                opacity: 1;
            }
        }
    }
    .ant-page-header-ghost {
        background-color: #f0f0f0;
    }
`;
