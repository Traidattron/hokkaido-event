import { Button, Drawer, Space, Modal } from 'antd';
import SecondaryDrawer from 'containers/SecondaryDrawer/SecondaryDrawer';
import { _word } from 'data/language';
import useApp from 'hooks/useApp';
import React, { cloneElement, useState } from 'react';

const PrimaryDrawer = () => {
    const { primaryDrawer, CLOSE_PRIMARY_DRAWER } = useApp();
    const [mutationLoading, setMutationLoading] = useState<boolean>(false);

    const setLoading = (flag) => {
        setMutationLoading(flag);
    };
    return (
        <>
            <Drawer
                width='620'
                placement='right'
                title={primaryDrawer.title}
                visible={primaryDrawer.display}
                onClose={CLOSE_PRIMARY_DRAWER}
                destroyOnClose
                footer={
                    <Space size='middle'>
                        <Button onClick={CLOSE_PRIMARY_DRAWER} style={{ width: 100 }}>
                            {_word.cancel}
                        </Button>
                        <Button
                            style={{ width: 100 }}
                            type='primary'
                            form='form-in-modal'
                            htmlType='submit'
                            loading={mutationLoading}
                        >
                            {_word.ok}
                        </Button>
                    </Space>
                }
                footerStyle={{ display: 'flex', justifyContent: 'flex-end' }}
            >
                {cloneElement(primaryDrawer.child, {
                    formName: 'form-in-modal',
                    setLoading,
                })}
                <SecondaryDrawer />
            </Drawer>
            <Modal title={_word.warning}></Modal>
        </>
    );
};

export default PrimaryDrawer;
