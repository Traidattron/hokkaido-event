import { Form, Input, Select, DatePicker, TimePicker, Button } from 'antd';
import { _word } from 'data/language';
import { dateFormat, timeFormat } from 'constants/format';
import moment from 'moment';
import { openNotification, typeNotification } from 'components/Notification/Notification';
import useApp from 'hooks/useApp';
import React, { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import { EventsDocument, useCreateEventMutation, useUpdateEventMutation } from 'graphql/gen-types';
const { Option } = Select;
type props = {
    initialValues: any;
    formName?: string;
    mutationType?: 'create' | 'update';
    setLoading?: any;
    onMutationSuccess: any;
};

export default function EventForm({ initialValues, formName, mutationType, setLoading, onMutationSuccess }: props) {
    const thisTime = moment();
    const remainder = 15 - (thisTime.minute() % 15);
    const URL = process.env.NEXT_PUBLIC_FORM;
    const router = useRouter();
    const { CLOSE_DRAWER } = useApp();

    function disabledDate(current) {
        return current && current < moment().startOf('day');
    }
    const [createEvent, { loading: createLoading }] = useCreateEventMutation({
        onCompleted: () => {
            openNotification(typeNotification.success, _word.createCompanySuccess);
            onMutationSuccess(mutationType);
            CLOSE_DRAWER();
        },
        refetchQueries: [
            {
                query: EventsDocument,
                variables: {
                    limit: Number(router.query.limit) || 20,
                    offset: 0,
                },
            },
        ],
        onError: (error) => {
            openNotification(typeNotification.error, error.message);
        },
    });

    const [updateEvent, { loading: updateLoading }] = useUpdateEventMutation({
        onCompleted: () => {
            onMutationSuccess(mutationType);
            openNotification(typeNotification.success, _word.updateCompanySuccess);
            CLOSE_DRAWER();
        },
        onError: (error) => {
            openNotification(typeNotification.error, error.message);
        },
    });

    useEffect(() => {
        setLoading(createLoading || updateLoading);
    }, [createLoading, updateLoading]);
    const formatEvent = (values) => {
        const { name, description, placeName, placeAddress, date, time, formAddress } = values;
        let tmp = {
            name,
            description,
            dateFrom: date[0].format(dateFormat),
            dateTo: date[1].format(dateFormat),
            timeFrom: time[0].format('HH:mm'),
            timeTo: time[1].format('HH:mm'),
            placeName,
            placeAddress,
            formAddress,
        };
        return tmp;
    };

    const handleCallMutation = (values) => {
        if (mutationType === 'create') {
            createEvent({ variables: { event: formatEvent(values) as any } });
        }
        mutationType === 'update' &&
            updateEvent({
                variables: {
                    id: initialValues.id,
                    event: formatEvent(values),
                },
            });
    };
    const layout = {
        labelCol: {
            span: 8,
        },
        wrapperCol: {
            span: 16,
        },
    };
    return (
        <Form
            onFinish={handleCallMutation}
            name={formName}
            initialValues={initialValues}
            {...layout}
            labelAlign='right'
        >
            <Form.Item
                label={_word.eventName}
                name='name'
                rules={[
                    {
                        required: true,
                        message: _word.ruleRequired,
                    },
                ]}
            >
                <Input autoFocus />
            </Form.Item>
            <Form.Item
                label={_word.placeName}
                name='placeName'
                rules={[
                    {
                        required: true,
                        message: _word.ruleRequired,
                    },
                ]}
            >
                <Input />
            </Form.Item>
            <Form.Item
                label={_word.placeAddress}
                name='placeAddress'
                rules={[
                    {
                        required: true,
                        message: _word.ruleRequired,
                    },
                ]}
            >
                <Input />
            </Form.Item>
            <Form.Item
                label={_word.dateFrom}
                name='date'
                rules={[
                    {
                        required: true,
                        message: _word.ruleRequired,
                    },
                ]}
            >
                <DatePicker.RangePicker
                    ranges={{
                        今日: [moment(), moment()],
                        今月: [moment().startOf('month'), moment().endOf('month')],
                    }}
                    disabledDate={disabledDate}
                    format={dateFormat}
                    className='input-datetimepicker'
                />
            </Form.Item>
            <Form.Item
                label={_word.timeFrom}
                name='time'
                rules={[
                    {
                        required: true,
                        message: _word.ruleRequired,
                    },
                ]}
                initialValue={[moment().add(remainder, 'minutes'), moment().add(remainder, 'minutes').add(2, 'hours')]}
            >
                <TimePicker.RangePicker format={timeFormat} minuteStep={15} className='input-datetimepicker' />
            </Form.Item>
            <Form.Item label={_word.description} name='description'>
                <Input.TextArea rows={3} />
            </Form.Item>
            <Form.Item label={_word.questionaire}>
                <Form.Item name='formAddress' noStyle>
                    <Input></Input>
                </Form.Item>
                <a href={URL} target='_blank'>
                    {_word.createQuestion}
                </a>
            </Form.Item>
            {/* {mutationType === 'create' && (
                <>
                    <h2 style={{ fontWeight: 600 }}>{_word.pushNoti}</h2>
                    <Form.Item label='To' name='type'>
                        <Select defaultValue='lucy' style={{ width: '100%' }}>
                            <Option value='jack'>Jack</Option>
                            <Option value='lucy'>Lucy</Option>
                            <Option value='Yiminghe'>yiminghe</Option>
                        </Select>
                    </Form.Item>
                    <Form.Item label={_word.message} name='message'>
                        <Input.TextArea rows={3} />
                    </Form.Item>
                </>
            )} */}
        </Form>
    );
}
