import { Button, Modal, Drawer, Divider, Tooltip } from 'antd';
import React, { useState, useEffect } from 'react';
import { _word } from 'data/language';
import Cookies from 'universal-cookie';
import Image from 'next/image';
import { useAuth } from 'hooks/useAuth';
import { useRouter } from 'next/router';
import { useHasMounted } from 'hooks/useHasMounted';
import { InfoUserWrapper } from './InfoUser.style';
import { openNotification } from 'components/Notification/Notification';
import { createFromIconfontCN } from '@ant-design/icons';
const IconFont = createFromIconfontCN({
    scriptUrl: '//at.alicdn.com/t/font_8d5l8fzk5b87iudi.js',
});
const { confirm } = Modal;

const InfoUser = () => {
    const router = useRouter();
    const { editmail, subMail, updateSubmail, createMail } = router.query;
    const hasMounted = useHasMounted();
    const cookie = new Cookies();
    const [visible, setVisible] = useState(false);
    const { isAuthenticated, SIGNOUT } = useAuth();
    const showDrawer = () => {
        setVisible(true);
    };
    const onClose = () => {
        setVisible(false);
    };
    function showConfirm() {
        confirm({
            title: _word.notification,
            content: editmail || subMail || createMail ? _word.notificationChangeForm : _word.notificationLogout,
            onOk() {
                SIGNOUT();
                openNotification('success', _word.signoutSucess);
            },
            onCancel() {},
        });
    }
    return (
        <>
            {hasMounted && (
                <>
                    <InfoUserWrapper>
                        <Image src='/user.svg' alt='Logo' width={30} height={30} />
                        <h3>{cookie.get('user_name')}</h3>
                        <Tooltip title={_word.logOut}>
                            <Button
                                style={{
                                    color: 'rgb(241, 33, 33)',
                                }}
                                icon={<IconFont type='icon-tuichu' style={{ fontSize: '1.3rem' }} />}
                                type='text'
                                size='large'
                                danger
                                onClick={() => {
                                    showConfirm();
                                }}
                            ></Button>
                        </Tooltip>
                    </InfoUserWrapper>
                </>
            )}
        </>
    );
};
export default InfoUser;
