import styled from 'styled-components';
import colors from 'theme/colors';

export const InfoUserWrapper = styled.div`
    h3 {
        font-weight: 600;
    }
    button:hover {
        background: transparent !important;
    }
`;
export const InfoContent = styled.div`
    display: flex;
    flex-direction: column;
`;
export const InfoUserContent = styled.div`
    display: flex;
    width: 100%;
    flex-direction: column;
    /* border-bottom: 1px solid ${colors.greyBorder}; */
`;
