import { Form, Input, Select, Checkbox, Spin, Result } from 'antd';
import { _word } from 'data/language';
import React, { useEffect, useState } from 'react';
import { EyeInvisibleOutlined, EyeTwoTone } from '@ant-design/icons';
import { enforceFormat, formatToFax, formatToPhone, patternFax, patternPhone } from 'constants/format';
import { ForwardForm, SubmitEvent } from 'style/pages.style';
import { useTrackingEventAttendeesMutation } from 'graphql/gen-types';
import { openNotification, typeNotification } from 'components/Notification/Notification';
import { useRouter } from 'next/router';
type props = {
    onFinishForm?: any;
    initialValues?: any;
    formName?: string;
};
const options = [
    { label: _word.livestreamFlag, value: 'livestreamFlag' },
    { label: _word.attendFlag, value: 'attendFlag' },
    { label: _word.postOfficeFlag, value: 'postOfficeFlag' },
];
export default function UserForm({ onFinishForm, initialValues, formName }: props) {
    const router = useRouter();
    const [completed, setCompleted] = useState(false);
    const [listCheck, setListCheck] = useState<any>([]);
    const [trackingEventAttendees, { loading: loadingTrackingEvent }] = useTrackingEventAttendeesMutation();
    const [form] = Form.useForm();
    useEffect(() => {
        form.resetFields();
    }, [initialValues]);
    function onChange(checkedValues) {
        setListCheck(checkedValues);
    }
    const onFinish = (values) => {
        if (listCheck.length === 0) {
            openNotification(typeNotification.warning, _word.ruleCheckbox);
            return;
        }
        trackingEventAttendees({
            variables: {
                forwardFromId: +router.query.forwardFromId,
                forwardUser: values,
                tracking: {
                    viewedLink: true,
                    attendFlag: listCheck.indexOf('attendFlag') !== -1 ? true : false,
                    livestreamFlag: listCheck.indexOf('livestreamFlag') !== -1 ? true : false,
                    postOfficeFlag: listCheck.indexOf('postOfficeFlag') !== -1 ? true : false,
                },
                mailUserId: router.query.mail_user_id.toString(),
            },
        })
            .then(() => {
                openNotification(typeNotification.success, _word.joinSuccess);
                setCompleted(true);
            })
            .catch((err) => {
                openNotification(typeNotification.error, err.message);
            });
    };

    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };
    return (
        <ForwardForm>
            <h1>{_word.registrationForm}</h1>
            {completed ? (
                <div
                    style={{
                        display: 'flex',
                        flexDirection: 'column',
                        justifyContent: 'center',
                        alignItems: 'center',
                        minHeight: '560px',
                    }}
                >
                    <Result
                        status='success'
                        title={_word.joinSuccess}
                    // subTitle=""
                    />
                </div>
            ) : loadingTrackingEvent ? (
                <div
                    style={{
                        display: 'flex',
                        flexDirection: 'column',
                        justifyContent: 'center',
                        alignItems: 'center',
                        minHeight: '560px',
                    }}
                >
                    <h1 style={{ color: '#fff' }}>{_word.sending}...</h1>
                    <Spin size='large' />
                </div>
            ) : (
                        <Form
                            className='form-forward'
                            name={formName}
                            initialValues={initialValues}
                            form={form}
                            layout='vertical'
                            onFinish={onFinish}
                            onFinishFailed={onFinishFailed}
                            labelAlign='right'
                        >
                            <Form.Item
                                label={_word.email}
                                name='email'
                                rules={[
                                    {
                                        type: 'email',
                                        message: _word.ruleNotEmail,
                                    },
                                    { required: true, message: _word.ruleRequired },
                                ]}
                            >
                                <Input autoFocus autoComplete='off' />
                            </Form.Item>
                            <Form.Item
                                label={_word.password}
                                name='password'
                                // rules={[
                                //     {
                                //         required: true,
                                //         message: _word.ruleRequired,
                                //     },
                                // ]}
                                hidden
                            >
                                <Input.Password
                                    iconRender={(visible) => (visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />)}
                                />
                            </Form.Item>

                            <Form.Item
                                label={_word.name}
                                name='name'
                                rules={[
                                    {
                                        required: true,
                                        message: _word.ruleRequired,
                                    },
                                ]}
                            >
                                <Input autoComplete='off' />
                            </Form.Item>
                            <Form.Item
                                label={_word.department}
                                name='department'
                                rules={[
                                    {
                                        required: true,
                                        message: _word.ruleRequired,
                                    },
                                ]}
                            >
                                <Input />
                            </Form.Item>

                            <Form.Item
                                label={_word.mobile}
                                name='mobile'
                                rules={[
                                    {
                                        required: true,
                                        message: _word.ruleRequired,
                                    },
                                ]}
                            >
                                <Input
                                    pattern={patternPhone}
                                    maxLength={13}
                                    placeholder='(Ex) 123-4567-8901'
                                    onKeyUp={formatToPhone}
                                    onKeyDown={enforceFormat}
                                />
                            </Form.Item>

                            <Form.Item
                                label={_word.companyName}
                                name='companyId'
                                rules={[
                                    {
                                        required: true,
                                        message: _word.ruleRequired,
                                    },
                                ]}
                                hidden
                            >
                                <Select
                                    // options={isSearch ? companyListAfterSearch : companyIds}
                                    // options={companyIds}
                                    showSearch
                                    // filterOption={false}
                                    filterOption={(input, option: any) => {
                                        return option.label.toLowerCase().indexOf(input.toLowerCase()) >= 0;
                                    }}
                                />
                            </Form.Item>
                            <Form.Item
                                label={_word.userType}
                                name='userTypeId'
                                rules={[
                                    {
                                        required: true,
                                        message: _word.ruleRequired,
                                    },
                                ]}
                                hidden
                            >
                                <Select
                                    // options={userTypeList}
                                    showSearch
                                    filterOption={(input, option: any) => {
                                        return option.label.toLowerCase().indexOf(input.toLowerCase()) >= 0;
                                    }}
                                />
                            </Form.Item>
                            <Form.Item
                                label={_word.jobTitle}
                                name='jobTitle'
                                rules={[
                                    {
                                        required: true,
                                        message: _word.ruleRequired,
                                    },
                                ]}
                                hidden
                            >
                                <Input />
                            </Form.Item>
                            <Form.Item
                                label={_word.address1}
                                name='address1'
                                rules={[
                                    {
                                        required: true,
                                        message: _word.ruleRequired,
                                    },
                                ]}
                                hidden
                            >
                                <Input />
                            </Form.Item>
                            <Form.Item label={_word.address2} name='address2' hidden>
                                <Input />
                            </Form.Item>
                            <Form.Item label={_word.fax} name='fax' hidden>
                                <Input
                                    pattern={patternFax}
                                    maxLength={15}
                                    placeholder='(Ex) 123-4567-890123'
                                    onKeyUp={formatToFax}
                                    onKeyDown={enforceFormat}
                                />
                            </Form.Item>
                            <Form.Item
                                label={_word.lableTypeJoin}
                                rules={[
                                    {
                                        required: true,
                                        message: _word.ruleRequired,
                                    },
                                ]}
                            >
                                <Checkbox.Group
                                    options={options}
                                    onChange={onChange}
                                    style={{ textAlign: 'left', marginTop: 0 }}
                                />
                            </Form.Item>
                            <Form.Item style={{ textAlign: 'center' }}>
                                {/* <Button block type='primary' htmlType='submit' onClick={() => {}}>
                        {_word.send}
                    </Button> */}
                                <SubmitEvent className='btn-forward'>
                                    <span>{_word.send}</span>
                                </SubmitEvent>
                            </Form.Item>
                        </Form>
                    )}
        </ForwardForm>
    );
}
