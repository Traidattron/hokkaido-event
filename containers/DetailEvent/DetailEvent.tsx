import { useRouter } from 'next/router';
import React, { useEffect, useState } from 'react';
import QRCode from 'qrcode.react';
import { Button, Row, Col, Tooltip, Popover, Space } from 'antd';
import { DownloadOutlined, CloseOutlined, NotificationOutlined } from '@ant-design/icons';
import { useEventQuery, useStatisticEventUserQuery } from 'graphql/gen-types';
import { _word } from 'data/language';
import {
    DetailEventContent,
    DetailEventInfo,
    DetailEventName,
    DetailEventWrapper,
    DetailEvent,
    QRDetailEventContent,
    NotificationDataEvent,
    ErrorText,
    HeaderDetailEvent,
} from 'style/pages.style';
import moment from 'moment';
import { dateFormatJP } from 'constants/format';
import Head from 'next/head';
import { Bar } from 'react-chartjs-2';
import LoadingSmall from 'components/Loading/LoadingSmaill';
import PushNotificationForm from 'components/PushNotification/PushNotification';

const DetailEventContainer = () => {
    const router = useRouter();
    const eventId = router.query.eventID;
    const [linkResponse, setLinkResponse] = useState<String>();
    const { data, loading } = useEventQuery({
        variables: {
            id: eventId as string,
        },
        onCompleted: (data) => {
            const URL = data.event.formAddress?.replace('viewform', 'responses');
            setLinkResponse(URL);
        },
    });
    const { data: dataUserEvent, loading: loadingStatistic } = useStatisticEventUserQuery({
        variables: {
            id: eventId as string,
        },
    });
    const handleDownload = () => {
        window.open(`https://hokkaido-stag.mirai-chi.com:4002/event/${data?.event.id}`);
    };
    const dataC = {
        labels: ['登録', '出席する', 'アンケート'],
        datasets: [
            {
                label: 'ユーザー',
                data: [
                    dataUserEvent?.statisticEventUser.total,
                    dataUserEvent?.statisticEventUser.attendCount,
                    dataUserEvent?.statisticEventUser.submitFormCount,
                ],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    // 'rgba(75, 192, 192, 0.2)',
                    // 'rgba(153, 102, 255, 0.2)',
                    // 'rgba(255, 159, 64, 0.2)',
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    // 'rgba(75, 192, 192, 1)',
                    // 'rgba(153, 102, 255, 1)',
                    // 'rgba(255, 159, 64, 1)',
                ],
                borderWidth: 2,
            },
        ],
    };
    const options = {
        scales: {
            yAxes: [
                {
                    ticks: {
                        beginAtZero: true,
                    },
                },
            ],
        },
    };
    return (
        <>
            <Head>
                <title>{data?.event.name}</title>
            </Head>
            <DetailEventWrapper>
                <HeaderDetailEvent>
                    <DetailEventName>{_word.detailEvent}</DetailEventName>
                    <Button
                        size='small'
                        style={{
                            position: 'absolute',
                            top: 0,
                            right: 0,
                            borderRadius: '50%',
                            background: '#8e8e8e',
                            color: '#fff',
                        }}
                        icon={<CloseOutlined />}
                        type='primary'
                        onClick={() => {
                            router.replace({ query: { eventID: null } });
                        }}
                    ></Button>
                </HeaderDetailEvent>
                {data ? (
                    <DetailEventContent className='fade'>
                        <QRDetailEventContent>
                            <QRCode
                                id='qr-gen'
                                value={data.event.id.toString()}
                                size={180}
                                level={'H'}
                                includeMargin={true}
                            />
                            <div
                                className='overlayQRCode'
                                onClick={() => {
                                    const canvas = document.getElementById('qr-gen') as HTMLCanvasElement;
                                    const pngUrl = canvas
                                        .toDataURL('image/png')
                                        .replace('image/png', 'image/octet-stream');
                                    let downloadLink = document.createElement('a');
                                    downloadLink.href = pngUrl;
                                    downloadLink.download = `${data.event.name}QRCode.png`;
                                    document.body.appendChild(downloadLink);
                                    downloadLink.click();
                                    document.body.removeChild(downloadLink);
                                }}
                            >
                                <h1>{_word.downloadQR}</h1>
                            </div>
                        </QRDetailEventContent>
                        <DetailEvent>
                            <DetailEventName style={{ color: '#0069b1' }}>{data.event.name}</DetailEventName>
                            <Row gutter={[16, 8]}>
                                <Col span={24}>
                                    <DetailEventInfo>
                                        <b>{_word.placeName}:</b> {data.event.placeName}
                                    </DetailEventInfo>
                                </Col>
                            </Row>
                            <Row gutter={[16, 8]}>
                                <Col span={24}>
                                    <DetailEventInfo>
                                        <b>{_word.placeAddress}:</b> {data.event.placeAddress}
                                    </DetailEventInfo>
                                </Col>
                            </Row>
                            <Row gutter={[16, 8]}>
                                <Col span={24}>
                                    <DetailEventInfo>
                                        <b>{_word.dateFrom}:</b>{' '}
                                        {data.event.dateFrom === data.event.dateTo ? (
                                            moment(data.event.dateFrom).format(dateFormatJP)
                                        ) : (
                                            <>
                                                {moment(data.event.dateFrom).format(dateFormatJP)} -{' '}
                                                {moment(data.event.dateTo).format(dateFormatJP)}
                                            </>
                                        )}
                                    </DetailEventInfo>
                                </Col>
                            </Row>
                            <Row gutter={[16, 8]}>
                                <Col span={24}>
                                    <DetailEventInfo>
                                        <b>{_word.timeFrom}:</b> {data.event.timeFrom} - {data.event.timeTo}
                                    </DetailEventInfo>
                                </Col>
                            </Row>
                            <DetailEventInfo>
                                <b>{_word.description}:</b> {data.event.description}
                            </DetailEventInfo>
                            <Row gutter={[16, 8]}>
                                <Col span={24}>
                                    <DetailEventInfo>
                                        <b>{_word.questionaire}:</b>{' '}
                                        {data.event.formAddress ? (
                                            <a
                                                style={{ color: '#3e95cd', fontWeight: 600, wordBreak: 'break-word' }}
                                                href={String(data.event.formAddress)}
                                                target='_blank'
                                            >
                                                {data.event.formAddress}
                                            </a>
                                        ) : (
                                            <ErrorText>{_word.dontHave}</ErrorText>
                                        )}
                                    </DetailEventInfo>
                                    {data.event.formAddress && (
                                        <a
                                            style={{ color: '#3e95cd', fontWeight: 600, wordBreak: 'break-word' }}
                                            href={String(linkResponse)}
                                            target='_blank'
                                        >
                                            {_word.responseForm}
                                        </a>
                                    )}
                                </Col>
                            </Row>
                            <DetailEventName>※{_word.eventStatistic}</DetailEventName>
                            <DetailEventInfo style={{ textAlign: 'center' }}>
                                <span style={{ marginRight: 10 }}>
                                    <b>{_word.totalUsers}:</b> {dataUserEvent?.statisticEventUser.total}
                                </span>
                                <Space>
                                    {/* <Popover
                                        placement='right'
                                        content={<PushNotificationForm eventID={eventId as string} />}
                                        title={_word.pushNoti}
                                        overlayClassName='popover-pushnoti'
                                        trigger='click'
                                    >
                                        <Button
                                            style={{
                                                borderRadius: 10,
                                            }}
                                            size='small'
                                            type='primary'
                                            icon={<NotificationOutlined />}
                                        />
                                    </Popover> */}
                                    {dataUserEvent && dataUserEvent?.statisticEventUser.total != 0 && (
                                        <Tooltip title={_word.downloadUsers}>
                                            <Button
                                                style={{
                                                    borderRadius: 10,
                                                    background: '#8e8e8e',
                                                    color: '#fff',
                                                }}
                                                size='small'
                                                icon={<DownloadOutlined />}
                                                type='default'
                                                onClick={handleDownload}
                                            ></Button>{' '}
                                        </Tooltip>
                                    )}
                                </Space>
                            </DetailEventInfo>
                            {loadingStatistic && <LoadingSmall />}
                            {!loadingStatistic ? (
                                dataUserEvent && dataUserEvent?.statisticEventUser.total != 0 ? (
                                    <Bar height={300} data={dataC} options={options} />
                                ) : (
                                    <ErrorText style={{ textAlign: 'center' }}>
                                        現在、参加しているユーザーがいません。
                                    </ErrorText>
                                )
                            ) : (
                                <></>
                            )}
                            <DetailEventInfo style={{ textAlign: 'center', fontWeight: 600, marginTop: 16 }}>
                                ※{_word.pushNoti}
                            </DetailEventInfo>
                            <PushNotificationForm eventID={eventId as string} />
                        </DetailEvent>
                    </DetailEventContent>
                ) : (
                    <QRDetailEventContent style={{ height: '100%', width: '100%' }}>
                        <NotificationDataEvent>
                            {/* <FileUnknownOutlined style={{ fontSize: '3rem', margin: '10px 0' }} /> */}
                            <div className='boxes'>
                                <div className='box'>
                                    <div></div>
                                    <div></div>
                                    <div></div>
                                    <div></div>
                                </div>
                                <div className='box'>
                                    <div></div>
                                    <div></div>
                                    <div></div>
                                    <div></div>
                                </div>
                                <div className='box'>
                                    <div></div>
                                    <div></div>
                                    <div></div>
                                    <div></div>
                                </div>
                                <div className='box'>
                                    <div></div>
                                    <div></div>
                                    <div></div>
                                    <div></div>
                                </div>
                            </div>
                            {loading ? <span>{_word.loading}</span> : <span>{_word.selectEvent}</span>}
                        </NotificationDataEvent>
                    </QRDetailEventContent>
                )}
            </DetailEventWrapper>
        </>
    );
};
export default DetailEventContainer;
